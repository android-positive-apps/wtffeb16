package com.wtf.serverTask;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;

public class SearchBuisnessTask extends AsyncTask<Void, Void, JSONObject>{
	
	
	private double longitude;
	private double latitude;
	private CallBack<JSONObject> callBack;
	private Context context;
	private String URL;
	private ProgressDialog mDialog;
	private String keyWord;
	
	
	public SearchBuisnessTask(Context context,String keyWord , CallBack<JSONObject> callBack){
		
		longitude = Double.parseDouble(Preferences.getInstans(context).getLongitude());
		latitude = Double.parseDouble(Preferences.getInstans(context).getLatitude());
		this.context = context;
		this.keyWord = keyWord;
		this.callBack = callBack;
		
		URL = "http://www.mypushserver.com/dev/bmm/api/searchBusiness";		
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mDialog = ProgressDialog.show(context, "", context.getString(R.string.working_for_you));
	}

	@Override
	protected JSONObject doInBackground(Void... params) {
		
		JSONObject jsonObject = null;
		
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
		
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(URL);
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("Name", keyWord));
		nameValuePairs.add(new BasicNameValuePair("NumResults", "20"));
		nameValuePairs.add(new BasicNameValuePair("Lat", latitude + ""));
		nameValuePairs.add(new BasicNameValuePair("Lng", longitude + ""));
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String s =EntityUtils.toString(entity);
			Log.e("response", s);
			jsonObject = new JSONObject(s);
			
		} catch (Exception e) {
			e.printStackTrace();
		}catch (Error r) {
			// TODO: handle exception
		}
		
		return jsonObject;
	}
	
	@Override
	protected void onPostExecute(JSONObject result) {
		super.onPostExecute(result);
		
		mDialog.dismiss();
		
		if(result == null || isCancelled()){
			Toast.makeText(context, context.getString(R.string.result_not_found), Toast.LENGTH_LONG).show();
			return;
		}
		
		Log.e("searchLog", result.toString());
		
		callBack.callBack(result);
	}
	
	

}
