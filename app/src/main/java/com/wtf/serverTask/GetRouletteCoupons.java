package com.wtf.serverTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wtf.dataBase.Preferences;
import com.wtf.objects.Coupon;

public class GetRouletteCoupons extends AsyncTask<Void, Void, List<Coupon>> {

	private Context mContext;
	
	private CallBack<List<Coupon>> mCallBack;

	private List<Coupon> mList;
	
	
	
	public GetRouletteCoupons(Context mContext, CallBack<List<Coupon>> mCallBack) {
		this.mContext = mContext;
		this.mCallBack = mCallBack;
		mList = new ArrayList<Coupon>();
	}

	@Override
	protected List<Coupon> doInBackground(Void... paramss) {

		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, 5000);
		HttpConnectionParams.setSoTimeout(params, 5000);

		String userId = Preferences.getInstans(mContext).getUserId();
		
		String url = "http://www.mypushserver.com/dev/bmm/api/getRouletteCoupons?AppuserID=" + userId + "&Amount=6";
		
		HttpClient client = new DefaultHttpClient(params);
		HttpGet get = new HttpGet(url);
		
		try {
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			JSONObject jsonObject = new JSONObject(string);
			
			Log.e("network get rulet", jsonObject.toString());
			jsonObject = jsonObject.getJSONObject("data");
			JSONArray array = jsonObject.getJSONArray("Coupons");
			
			for (int i = 0; i < array.length(); i++) {
				mList.add(new Coupon(array.getJSONObject(i)));
			}
			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return mList;
	}
	
	
	@Override
	protected void onPostExecute(List<Coupon> result) {
		super.onPostExecute(result);
		
		if(result == null){
			return;
		}
		
		mCallBack.callBack(result);
	}

}

	
	
	

