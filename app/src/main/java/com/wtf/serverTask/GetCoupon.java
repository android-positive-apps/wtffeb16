package com.wtf.serverTask;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.wtf.dataBase.Preferences;
import com.wtf.objects.Coupon;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class GetCoupon extends AsyncTask<Void, Void, Coupon> {

	private Coupon mCoupon;

	private Context mContext;
	
	private CallBack<Coupon> mCallBack;
	

	

	public GetCoupon(Coupon mKoupon, Context mContext,
			CallBack<Coupon> mCallBack) {
		this.mCoupon = mKoupon;
		this.mContext = mContext;
		this.mCallBack = mCallBack;
	}


	@Override
	protected Coupon doInBackground(Void... paramss) {

		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, 5000);
		HttpConnectionParams.setSoTimeout(params, 5000);

		String userId = Preferences.getInstans(mContext).getUserId();
		String couponId = mCoupon.getId();
		String url = "http://www.mypushserver.com/dev/bmm/api/getCoupon?AppuserID=" + userId + "&CouponID=" + couponId;
		
		HttpClient client = new DefaultHttpClient(params);
		HttpGet get = new HttpGet(url);
		
		try {
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			
			JSONObject jsonObject = new JSONObject(string);
			Log.e("network - get coupon", jsonObject.toString());
			JSONObject data = jsonObject.getJSONObject("data");
			
			mCoupon = new Coupon(data.getJSONObject("Coupon"));
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return mCoupon;
	}
	
	
	@Override
	protected void onPostExecute(Coupon result) {
		super.onPostExecute(result);
		
		if(result == null){
			return;
		}
		
		mCallBack.callBack(result);
	}

}
