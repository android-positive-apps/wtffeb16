package com.wtf.serverTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.util.Log;

import com.wtf.dataBase.Preferences;

public class UpdateGcm extends Thread{
	
	
	private Context context;

	public UpdateGcm(Context context) {
		super();
		this.context = context;
	}

	@Override
	public void run() {
		super.run();
		
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://www.mypushserver.com/dev/bmm/api/updateAppuserGCM");
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("ID", Preferences.getInstans(context).getUserId()));
		nameValuePairs.add(new BasicNameValuePair("GCM", Preferences.getInstans(context).getGCM()));
		
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			Log.d("gcm update", string);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
