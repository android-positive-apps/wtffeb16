/**
 * 
 */
package com.wtf.serverTask;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;


/**
 * @author natiapplications
 *
 */
public class UpdateUserEnabledLocation extends AsyncTask<Void, Void, JSONObject> {

	
	private Context mContext;
	

	

	public UpdateUserEnabledLocation(Context mContext) {
		
		this.mContext = mContext;
		
	}

	
	
	
	@Override
	protected JSONObject doInBackground(Void... params) {
		
		JSONObject returnedJsonObject = null;
		
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://www.mypushserver.com/dev/bmm/api/updateAppuser");
		
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		try {
			
			String enableLocation = "0";
			if (Preferences.getInstans(BMMApplication.getAppContext()).isShowLocation()){
				enableLocation = "1";
			}
			parameters.add(new BasicNameValuePair("ID", Preferences.getInstans(mContext).getUserId()));
			parameters.add(new BasicNameValuePair("EnableLocation",enableLocation));
			
			post.setEntity(new UrlEncodedFormEntity(parameters));
			
			HttpResponse response = client.execute(post);
			
			HttpEntity entity = response.getEntity();
			
			returnedJsonObject = new JSONObject(EntityUtils.toString(entity));
			Log.e("network - create new user",  "response: " + returnedJsonObject.toString());
			
		} catch (Exception e) {
			Log.e("network - create new user",  "request ex - " + e.getMessage());
			e.printStackTrace();
		}
		
		return returnedJsonObject;
	}



}