package com.wtf.serverTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;

public class GetBezeqTask extends AsyncTask<Void, Void, JSONObject>{
	
	public static final int TypeFood = 1;
	public static final int TypeSport = 2;
	public static final int TypeFation = 3;
	public static final int TypeNight = 4;
	public static final int TypeFreeTime= 5;
	
	
	private double longitude;
	private double latitude;
	private String appUserId;
	private int radius;
	private CallBack<JSONObject> callBack;
	private Context context;
	private String URL;
	private ProgressDialog mDialog;
	private String categoryType;
	
	
	public GetBezeqTask(Context context , CallBack<JSONObject> callBack , int type){
		longitude = Double.parseDouble(Preferences.getInstans(context).getLongitude());
		latitude = Double.parseDouble(Preferences.getInstans(context).getLatitude());
		appUserId = Preferences.getInstans(context).getUserId();
		radius = Preferences.getInstans(context).getRadius();
		this.context = context;
		this.callBack = callBack;
		
		if(longitude == 0.0 || latitude == 0.0){
			Toast.makeText(context, "Your Gps is off" , Toast.LENGTH_LONG).show();
		}
		URL = "http://www.mypushserver.com/dev/bmm/api/getBusinessesByCategory";
		
		/*switch (type) {
		case TypeFood:
			URL = "http://www.mypushserver.com/dev/bmm/api/getBezeqFood";
			break;
		case TypeFation:
			URL = "http://www.mypushserver.com/dev/bmm/api/getBezeqFashion";
			break;
		case TypeFreeTime:
			URL = "http://www.mypushserver.com/dev/bmm/api/getBezeqFreeTime";
			break;
		case TypeNight:
			URL = "http://www.mypushserver.com/dev/bmm/api/getBezeqNight";
			break;
		case TypeSport:
			URL = "http://www.mypushserver.com/dev/bmm/api/getBezeqSport";
			break;

		
		}*/
		
		switch (type) {
		case TypeFood:
			categoryType = "אוכל";
			break;
		case TypeFation:
			categoryType = "אופנה";
			break;
		case TypeFreeTime:
			categoryType = "פנאי";
			break;
		case TypeNight:
			categoryType = "לילה";
			break;
		case TypeSport:
			categoryType = "ספורט";
			break;

		
		}
		
		
		
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mDialog = ProgressDialog.show(context, "", context.getString(R.string.loading));
	}

	@Override
	protected JSONObject doInBackground(Void... params) {
		
		JSONObject jsonObject = null;
		
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
		
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(URL);
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("Category", categoryType));
		nameValuePairs.add(new BasicNameValuePair("AppuserID", appUserId));
		nameValuePairs.add(new BasicNameValuePair("Lat", latitude + ""));
		nameValuePairs.add(new BasicNameValuePair("Lng", longitude + ""));
		nameValuePairs.add(new BasicNameValuePair("Radius", radius + ""));
		nameValuePairs.add(new BasicNameValuePair("NumResults", 5 + ""));
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String s =EntityUtils.toString(entity);
			Log.e("response", s);
			jsonObject = new JSONObject(s);
			
		} catch (Exception e) {
			e.printStackTrace();
		}catch (Error r) {
			// TODO: handle exception
		}
		
		return jsonObject;
	}
	
	@Override
	protected void onPostExecute(JSONObject result) {
		super.onPostExecute(result);
		
		mDialog.dismiss();
		
		if(result == null || isCancelled()){
			Toast.makeText(context, context.getString(R.string.result_not_found), Toast.LENGTH_LONG).show();
			return;
		}
		Log.e("network", result.toString());
		callBack.callBack(result);
	}
	
	private String readIt(InputStream stream) throws IOException,
			UnsupportedEncodingException {

		StringBuilder result = new StringBuilder();
		;
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(stream));
		String line;
		while ((line = reader.readLine()) != null) {
			result.append(line);
		}
		return result.toString();
	}

}
