/**
 * 
 */
package com.wtf.serverTask;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.Response;

/**
 * @author Nati Gabay
 *
 */


public class TransferGiftsBetweenUsersTask extends AsyncTask<Void, Void, Response> {

	private static final String URL = "http://www.mypushserver.com/dev/bmm/api/transferGift";

	private Context context;
	private String appUserId;
	private String toUserID;
	private String giftID;

	public TransferGiftsBetweenUsersTask(Context context, String toUserID,
			String giftID) {
		this.context = context;
		this.toUserID = toUserID;
		this.giftID = giftID;
		Preferences preferences = Preferences.getInstans(context);
		appUserId = preferences.getUserId();
	}

	@Override
	protected Response doInBackground(Void... params) {

		JSONObject jsonObject = null;

		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(URL);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("FromAppuserID", appUserId));
		nameValuePairs.add(new BasicNameValuePair("ToAppuserID", toUserID));
		nameValuePairs.add(new BasicNameValuePair("GiftID", giftID));

		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			jsonObject = new JSONObject(EntityUtils.toString(entity));
			
			Log.e("network  - tranfer gifts", jsonObject.toString());

		} catch (Exception e) {
			e.printStackTrace();
		} 

		return new Response(jsonObject);
	}

	@Override
	protected void onPostExecute(Response response) {
		super.onPostExecute(response);

		if (response == null) {
			Toast.makeText(context, context.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
			return;
		}
		Toast.makeText(context, context.getString(R.string.trancfer_gift_success), Toast.LENGTH_SHORT).show();
	}
}