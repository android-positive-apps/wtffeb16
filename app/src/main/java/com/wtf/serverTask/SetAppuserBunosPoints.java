/**
 * 
 */
package com.wtf.serverTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.wtf.dataBase.Preferences;
import com.wtf.objects.Response;
import com.wtf.utils.GiftUtils;

/**
 * @author natiapplications
 *
 */


public class SetAppuserBunosPoints extends AsyncTask<Void, Void, Response> {

	private static final String URL = "http://www.mypushserver.com/dev/bmm/api/setBonusPoints";

	private Context context;
	private String appUserId;
	private String actionName;
	private int points;

	public SetAppuserBunosPoints(Context context, int points,
			String actionName) {
		this.context = context;
		this.points = points;
		this.actionName = actionName;
		Preferences preferences = Preferences.getInstans(context);
		appUserId = preferences.getUserId();
	}

	@Override
	protected Response doInBackground(Void... params) {

		JSONObject jsonObject = null;

		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(URL);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("AppuserID", appUserId));
		nameValuePairs.add(new BasicNameValuePair("Points",Integer.toString(points)));
		nameValuePairs.add(new BasicNameValuePair("ActionName", actionName));

		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			jsonObject = new JSONObject(EntityUtils.toString(entity));
			
			Log.e("network  - set points", jsonObject.toString());

		} catch (Exception e) {
			e.printStackTrace();
		} 

		return new Response(jsonObject);
	}

	@Override
	protected void onPostExecute(Response response) {
		super.onPostExecute(response);

		if (response == null) {
			return;
		}
		Activity activity = (Activity)context;
		GiftUtils.showResultToast(activity, points+"");
	}
}
