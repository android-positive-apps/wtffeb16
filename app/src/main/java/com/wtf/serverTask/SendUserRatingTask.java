/**
 * 
 */
package com.wtf.serverTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wtf.dataBase.Preferences;
import com.wtf.objects.Response;

/**
 * @author Nati Gabay
 *
 */
public class SendUserRatingTask extends AsyncTask<Void, Void, Response>{

	private static final String URL = "http://www.mypushserver.com/dev/bmm/api/updateRank";
	
	private String appUserId;
	private int q1,q2,q3,q4;
	private String businessID;

	private CallBack<Response> mCallBack;
	

	public SendUserRatingTask(Context context , String businessID ,int q1,int q2, int q3,int q4, CallBack<Response> mCallBack) {
		
		this.mCallBack = mCallBack;
		this.businessID = businessID;
		Preferences preferences = Preferences.getInstans(context);
		appUserId = preferences.getUserId();
		this.q1 = q1;
		this.q2 = q2;
		this.q3 = q3;
		this.q4 = q4;
	}





	@Override
	protected Response doInBackground(Void... params) {
		
		JSONObject jsonObject = null;
		
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
		
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(URL);
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("AppuserID", appUserId));
		nameValuePairs.add(new BasicNameValuePair("BusinessID", businessID));
		nameValuePairs.add(new BasicNameValuePair("Q1", new Integer(q1).toString() ));
		nameValuePairs.add(new BasicNameValuePair("Q2", new Integer(q2).toString()  ));
		nameValuePairs.add(new BasicNameValuePair("Q3", new Integer(q3).toString()  ));
		nameValuePairs.add(new BasicNameValuePair("Q4", new Integer(q4).toString()  ));
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			jsonObject = new JSONObject(EntityUtils.toString(entity));
			
			Log.e ("network", jsonObject.toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return new Response(jsonObject);
	}
	
	
	@Override
	protected void onPostExecute(Response response) {
		super.onPostExecute(response);
		
		if (response == null) {
			return;
		}
		
		mCallBack.callBack(response);
	}
}