/**
 * 
 */
package com.wtf.serverTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wtf.dataBase.Preferences;
import com.wtf.objects.Gift;

/**
 * @author natiapplications
 *
 */
public class GetAppuserId extends AsyncTask<Void, Void, String> {

	private Context mContext;
	
	private CallBack<String> mCallBack;
	
	private String AppuserID = "";
	
	private String facebookID;

	
	

	public GetAppuserId( Context mContext,String facebookID,
			CallBack<String> mCallBack) {
		
		this.mContext = mContext;
		this.facebookID = facebookID;
		this.mCallBack = mCallBack;
	}


	@Override
	protected String doInBackground(Void... paramss) {
		String url = "http://www.mypushserver.com/dev/bmm/api/getAppuserID";
		
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(url);

		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("FacebookID", facebookID));
		
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			
			JSONObject jsonObject = new JSONObject(string);
			JSONObject data = jsonObject.getJSONObject("data");
			
			AppuserID = data.getString("AppuserID");
			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return AppuserID;
	}
	
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		Log.e("network - get id", "id " + result);
		if(result == null){
			return;
		}
		
		mCallBack.callBack(result);
	}

}