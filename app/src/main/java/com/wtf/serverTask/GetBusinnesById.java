/**
 * 
 */
package com.wtf.serverTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.*;

/**
 * @author Nati Gabay
 *
 */

public class GetBusinnesById extends AsyncTask<Void, Void, Business> {

	private Context mContext;
	
	private CallBack<Business> mCallBack;
	
	private Business business ;
	private ProgressDialog mDialog;
	
	private String businessID;
	private String businessExternalID;
	

	
	

	public GetBusinnesById( Context mContext,String businessID,String businessExternalID,
			CallBack<Business> mCallBack) {
		
		this.mContext = mContext;
		this.businessID = businessID;
		this.businessExternalID = businessExternalID;
		this.mCallBack = mCallBack;
	}


	

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mDialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.loading));
	}
	
	@Override
	protected Business doInBackground(Void... paramss) {
		String url = "http://www.mypushserver.com/dev/bmm/api/getBusiness";
		
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(url);

		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("AppuserID", Preferences.getInstans(mContext).getUserId()));
		nameValuePairs.add(new BasicNameValuePair("BusinessID", businessID));
		nameValuePairs.add(new BasicNameValuePair("BusinessExtID", businessExternalID));
		
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			
			JSONObject jsonObject = new JSONObject(string);
			Log.e("network - getBusiness", jsonObject.toString());
			JSONObject data = jsonObject.getJSONObject("data");
			
			business = new Business(data.getJSONObject("Business")) ;
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return business;
	}
	
	
	@Override
	protected void onPostExecute(Business result) {
		super.onPostExecute(result);
		
		mDialog.dismiss();
		if(result == null){
			return;
		}
		
		mCallBack.callBack(result);
	}

}