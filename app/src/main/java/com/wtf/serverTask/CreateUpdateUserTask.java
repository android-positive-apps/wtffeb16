package com.wtf.serverTask;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.UserProfile;
import com.wtf.utils.SimpleFacebookUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

public class CreateUpdateUserTask extends AsyncTask<Void, Void, JSONObject> {

	
	private Context mContext;
	private String firstName;
	private String lastName;
	

	private String facebookId;
	private CallBack<UserProfile> callBack;

	private ProgressDialog dialog;
	

	public CreateUpdateUserTask(Context mContext, String firstName,
			String lastName,String facebookId, CallBack<UserProfile> callBack) {
		
		this.mContext = mContext;
		this.firstName = firstName;
		this.lastName = lastName;
	
		this.facebookId = facebookId;
		this.callBack = callBack;
		
	}

	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		try{
			dialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.loading));
		}catch (Exception e){}

		
	}
	
	
	@Override
	protected JSONObject doInBackground(Void... params) {
		
		JSONObject returnedJsonObject = null;
		
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://www.mypushserver.com/dev/bmm/api/updateAppuser");
		
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		try {
			
			String enableLocation = "0";
			if (Preferences.getInstans(BMMApplication.getAppContext()).isShowLocation()){
				enableLocation = "1";
			}
			parameters.add(new BasicNameValuePair("ID", Preferences.getInstans(mContext).getUserId()));
			parameters.add(new BasicNameValuePair("FirstName", firstName));
			parameters.add(new BasicNameValuePair("LastName", lastName));
			parameters.add(new BasicNameValuePair("FBID", facebookId));
			parameters.add(new BasicNameValuePair("UDID", getUDID()));
			parameters.add(new BasicNameValuePair("EnableLocation",enableLocation));
			
			post.setEntity(new UrlEncodedFormEntity(parameters,HTTP.UTF_8));
			
			HttpResponse response = client.execute(post);
			
			HttpEntity entity = response.getEntity();
			
			returnedJsonObject = new JSONObject(EntityUtils.toString(entity));
			Log.e("network - create new user",  "response: " + returnedJsonObject.toString());
			
		} catch (Exception e) {
			Log.e("network - create new user",  "request ex - " + e.getMessage());
			e.printStackTrace();
		}
		
		return returnedJsonObject;
	}

	@Override
	protected void onPostExecute(JSONObject result) {
		super.onPostExecute(result);
		try{
			dialog.dismiss();
		}catch (Exception e){}

		if(result != null){
			SimpleFacebookUtils.setUserFacebookFriends();
			getAppUserProfile();
		}
	}

	private String getUDID() {

		return Secure.getString(mContext.getContentResolver(),
				Secure.ANDROID_ID);
	}
	
	private void getAppUserProfile() {
		new LoginTask(mContext, callBack).execute();
		new GetUserGifts(mContext, 1, null).execute();
		new GetAppuserReceivedGifts(mContext, 1, null).execute();
	}

}
