/**
 * 
 */
package com.wtf.serverTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.wtf.activity.BMMApplication;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.FacebookFriand;

/**
 * @author Nati Gabay
 *
 */

public class GetBuisnessCheckingsTask extends AsyncTask<Void, Void, ArrayList<String>> {

	

	private Context mContext;
	private String businessID;
	private CallBack<ArrayList<FacebookFriand>> mCallBack;
	
	private ArrayList<String> checkingsFBID = new ArrayList<String>();

	

	public GetBuisnessCheckingsTask( Context mContext,String busineesID,
			CallBack<ArrayList<FacebookFriand>> mCallBack) {
		
		this.mContext = mContext;
		this.mCallBack = mCallBack;
		this.businessID = busineesID;
	}

	

	@Override
	protected ArrayList<String> doInBackground(Void... paramss) {
		try {
			String url = "http://www.mypushserver.com/dev/bmm/api/getBusinessCheckIns";

			String userId = Preferences.getInstans(mContext).getUserId();
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 3000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 5000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			HttpClient client = new DefaultHttpClient(httpParameters);
			HttpPost post = new HttpPost(url);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			ArrayList<String> friendsIDs = new ArrayList<String>();
			for (int i = 0; i < BMMApplication.getUserFacebookFriends().size(); i++) {
				friendsIDs.add(BMMApplication.getUserFacebookFriends().get(i)
						.getFBID());
			}
			if (!Preferences.getInstans(mContext).getFacebookId().equals("0")){
				friendsIDs.add(Preferences.getInstans(mContext).getFacebookId());
			}
			
			JSONArray friendsJsonArray = new JSONArray(friendsIDs);

			nameValuePairs.add(new BasicNameValuePair("FBIDs", friendsJsonArray
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("BusinessID", businessID));

			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);

			JSONObject jsonObject = new JSONObject(string);
			
			Log.e("network", jsonObject.toString());
			JSONObject data = jsonObject.getJSONObject("data");
			JSONArray res = data.getJSONArray("CheckIns");
			
			checkingsFBID = new ArrayList<String>();
			for (int i = 0; i < res.length(); i++) {
				String fbid = res.getJSONObject(i).getString("FBID");
				checkingsFBID.add(fbid);
			}
			

		} catch (Exception e) {
		}

		return checkingsFBID;
	}
	
	
	@Override
	protected void onPostExecute(ArrayList<String> result) { 
		super.onPostExecute(result);
		String userFBID = Preferences.getInstans(mContext).getFacebookId();
		if(result == null){
			return;
		}
		ArrayList<FacebookFriand> picthures = new ArrayList<FacebookFriand>();
		for (int i = 0; i < result.size(); i++) {
			String tempID = result.get(i);
			for (int j = 0; j < BMMApplication.getUserFacebookFriends().size(); j++) {
				String tempFriendID = BMMApplication.getUserFacebookFriends().get(j).getFBID();
				if (tempID.equalsIgnoreCase(tempFriendID)){
					picthures.add(BMMApplication.getUserFacebookFriends().get(j));
				}
			}
			if (tempID.equalsIgnoreCase(userFBID)){
				FacebookFriand user = new FacebookFriand();
				user.setFBID(userFBID);
				user.setImage(Preferences.getInstans(mContext).getFacebookImageProfile());
				picthures.add(user);
			}
		}
		
		Collections.reverse(picthures);
		mCallBack.callBack(picthures);
	}

}