/**
 * 
 */
package com.wtf.serverTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.FriandLocation;
import com.wtf.objects.Gift;

/**
 * @author Nati Application
 *
 */
public class GetAppuserFriandsLocation extends AsyncTask<Void, Void, ArrayList<FriandLocation>> {

	

	private Context mContext;
	
	private CallBack<ArrayList<FriandLocation>> mCallBack;
	
	private ArrayList<FriandLocation> friandLocationArray = new ArrayList<FriandLocation>();

	private ProgressDialog mDialog;

	public GetAppuserFriandsLocation( Context mContext,
			CallBack<ArrayList<FriandLocation>> mCallBack) {
		
		this.mContext = mContext;
		this.mCallBack = mCallBack;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mDialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.loading));
	}

	@Override
	protected ArrayList<FriandLocation> doInBackground(Void... paramss) {
		try {
			String url = "http://www.mypushserver.com/dev/bmm/api/getAppuserFriendsLocation";

			String userId = Preferences.getInstans(mContext).getUserId();
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 3000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 5000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			HttpClient client = new DefaultHttpClient(httpParameters);
			HttpPost post = new HttpPost(url);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			ArrayList<String> friendsIDs = new ArrayList<String>();
			for (int i = 0; i < BMMApplication.getUserFacebookFriends().size(); i++) {
				friendsIDs.add(BMMApplication.getUserFacebookFriends().get(i)
						.getFBID());
			}
			JSONArray friendsJsonArray = new JSONArray(friendsIDs);

			nameValuePairs.add(new BasicNameValuePair("FBIDs", friendsJsonArray
					.toString()));

			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);

			JSONObject jsonObject = new JSONObject(string);
			JSONObject data = jsonObject.getJSONObject("data");
			Log.e("network", data.toString());
			JSONArray res = data.getJSONArray("Friends");
			friandLocationArray = new ArrayList<FriandLocation>();
			for (int i = 0; i < res.length(); i++) {
				FriandLocation friandLocation = new FriandLocation(res.getJSONObject(i));
				if (friandLocation.getLat() >0 && friandLocation.getLon() > 0){
					friandLocationArray.add(friandLocation);
				}
			}
			

		} catch (Exception e) {
		}

		return friandLocationArray;
	}
	
	
	@Override
	protected void onPostExecute(ArrayList<FriandLocation> result) { 
		super.onPostExecute(result);
		mDialog.dismiss();
		if(result == null){
			return;
		}
		
		mCallBack.callBack(result);
	}

}