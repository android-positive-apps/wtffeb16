/**
 * 
 */
package com.wtf.serverTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wtf.dataBase.Preferences;
import com.wtf.objects.Coupon;
import com.wtf.objects.UserProfile;

/**
 * @author natiapplications
 *
 */
public class GetUserProfileTask extends AsyncTask<Void, Void, UserProfile> {

	private UserProfile mUserProfile;

	private Context mContext;
	
	private CallBack<UserProfile> mCallBack;
	

	

	public GetUserProfileTask( Context mContext,
			CallBack<UserProfile> mCallBack) {
		
		this.mContext = mContext;
		this.mCallBack = mCallBack;
	}


	@Override
	protected UserProfile doInBackground(Void... paramss) {

		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, 5000);
		HttpConnectionParams.setSoTimeout(params, 5000);

		String userId = Preferences.getInstans(mContext).getUserId();
		
		String url = "http://www.mypushserver.com/dev/bmm/api/getAppuser?AppuserID=" + userId;
		
		HttpClient client = new DefaultHttpClient(params);
		HttpGet get = new HttpGet(url);
		
		try {
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			
			Log.e("get User Tag", string);
			JSONObject jsonObject = new JSONObject(string);
			JSONObject data = jsonObject.getJSONObject("data");
			
			mUserProfile = new UserProfile(data.getJSONObject("Appuser"));
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return mUserProfile;
	}
	
	
	@Override
	protected void onPostExecute(UserProfile result) {
		super.onPostExecute(result);
		
		if(result == null){
			return;
		}
		mCallBack.callBack(result);
	}

}
