/**
 * 
 */
package com.wtf.serverTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wtf.activity.BMMApplication;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.Coupon;
import com.wtf.objects.Gift;

/**
 * @author natiapplications
 *
 */
public class GetUserGifts extends AsyncTask<Void, Void, ArrayList<Gift>> {

	

	private Context mContext;
	
	private CallBack<ArrayList<Gift>> mCallBack;
	
	private ArrayList<Gift> giftsArray = new ArrayList<Gift>();

	private int used;
	

	public GetUserGifts( Context mContext,int used,
			CallBack<ArrayList<Gift>> mCallBack) {
		
		this.mContext = mContext;
		this.used = used;
		this.mCallBack = mCallBack;
	}


	@Override
	protected ArrayList<Gift> doInBackground(Void... paramss) {
		String url = "http://www.mypushserver.com/dev/bmm/api/getAppuserGifts";
		
		String userId = Preferences.getInstans(mContext).getUserId();
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(url);

		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("AppuserID", userId));
		nameValuePairs.add(new BasicNameValuePair("Unused", used + ""));
	
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			
			JSONObject jsonObject = new JSONObject(string);
			Log.e("network", jsonObject.toString());
			JSONObject data = jsonObject.getJSONObject("data");
			
			JSONArray gifts = data.getJSONArray("Gifts");
			
			for (int i = 0; i < gifts.length(); i++) {
				Gift temp = new Gift(gifts.getJSONObject(i));
				giftsArray.add(temp);
			}
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return giftsArray;
	}
	
	
	@Override
	protected void onPostExecute(ArrayList<Gift> result) {
		super.onPostExecute(result);
		
		if(result == null){
			return;
		}
		
		BMMApplication.setUserGifts(result);
		
		if(mCallBack != null){
			mCallBack.callBack(result);
		}
	}

}
