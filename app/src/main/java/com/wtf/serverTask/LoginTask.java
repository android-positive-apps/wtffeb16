package com.wtf.serverTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.wtf.activity.BMMApplication;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.UserProfile;
import com.wtf.utils.GeneralUtills;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class LoginTask extends AsyncTask<Void, Void, UserProfile>{

	private Context context;
	private String facebookID;
	private String UDID;
	private UserProfile mUserProfile;
	
	private CallBack<UserProfile> mCallBack;

	public LoginTask(Context context,CallBack<UserProfile> callBack) {
		this.context = context;
		this.facebookID = Preferences.getInstans(context).getFacebookId();
		UDID = GeneralUtills.getUDID(context);
		this.mCallBack = callBack;
		this.mUserProfile = BMMApplication.getCurrentUserProfile();
	}

	@Override
	protected UserProfile doInBackground(Void... params) {
		
		HttpClient client = new DefaultHttpClient();
		String URL = "http://www.mypushserver.com/dev/bmm/api/applicationLogin";
		HttpPost post = new HttpPost(URL );
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("FacebookID", facebookID));
		nameValuePairs.add(new BasicNameValuePair("UDID", UDID));
		nameValuePairs.add(new BasicNameValuePair("Lat", Preferences.getInstans(context).getLatitude() + ""));
		nameValuePairs.add(new BasicNameValuePair("Lng", Preferences.getInstans(context).getLongitude() + ""));
		
		Log.e("network", UDID + "\n" + Preferences.getInstans(context).getLatitude() + "\n" + Preferences.getInstans(context).getLongitude());
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			Log.d("Login..", string);
			JSONObject jsonObject = new JSONObject(string);
			JSONObject data = jsonObject.getJSONObject("data");
			mUserProfile = new UserProfile(data);
			
			Log.e("network", jsonObject.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mUserProfile;
	}
	
	@Override
	protected void onPostExecute(UserProfile result) {
		super.onPostExecute(result);
		if (result == null){
			mCallBack.callBack(mUserProfile);
			return;
		}
		mCallBack.callBack(result);
		
	}

}
