package com.wtf.serverTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.wtf.activity.BMMApplication;
import com.wtf.dataBase.Preferences;

public class UpdateAppuserLocationTask extends Thread{

	private double longitude;
	private double latitude;
	private String appUserId;
	
	public UpdateAppuserLocationTask(Context context) {
		longitude = Double.parseDouble(Preferences.getInstans(context).getLongitude());
		latitude = Double.parseDouble(Preferences.getInstans(context).getLatitude());
		appUserId = Preferences.getInstans(context).getUserId();
	}
	
	@Override
	public void run() {
		super.run();
		
		HttpClient client = new DefaultHttpClient();
		String URL = "http://www.mypushserver.com/dev/bmm/api/updateAppuserLocation";
		HttpPost post = new HttpPost(URL );
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("AppuserID", appUserId));
		Log.e("appuseridlog", "userID = " + appUserId);
		if (Preferences.getInstans(BMMApplication.getAppContext()).isShowLocation()){
			nameValuePairs.add(new BasicNameValuePair("Lat", latitude + ""));
			nameValuePairs.add(new BasicNameValuePair("Lng", longitude + ""));
			nameValuePairs.add(new BasicNameValuePair("Version", "V: "  + getApplicationVersion()
					+ " Device: " + Build.MODEL));
		}else{
			nameValuePairs.add(new BasicNameValuePair("Lat","0"));
			nameValuePairs.add(new BasicNameValuePair("Lng","0"));
		}
		
		Log.i("LOC_RECEIVER", "request sent" + latitude+","+longitude);
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			
			Log.d("LOC_RECEIVER","Location update response "+string);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private  String getApplicationVersion() {
		String appVersion = "";
		try {
			PackageManager manager = BMMApplication.getAppContext().getPackageManager();
			PackageInfo info = manager.getPackageInfo(
					BMMApplication.getAppContext().getPackageName(), 0);
			appVersion =  info.versionName;
		} catch (Exception e) {
		}
		return appVersion;
	}
	
}
