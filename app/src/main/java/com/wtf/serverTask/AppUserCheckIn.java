package com.wtf.serverTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.wtf.activity.BMMApplication;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.Response;

import android.content.Context;
import android.os.AsyncTask;

public class AppUserCheckIn extends AsyncTask<Void, Void, Response>{

	private static final String URL = "http://www.mypushserver.com/dev/bmm/api/appuserCheckIn";
	
	private String appUserId;
	private String latitude;
	private String longitude;
	private String businessID;

	private CallBack<Response> mCallBack;
	

	public AppUserCheckIn(Context context , String businessID , CallBack<Response> mCallBack) {
		
		this.mCallBack = mCallBack;
		this.businessID = businessID;
		Preferences preferences = Preferences.getInstans(context);
		appUserId = preferences.getUserId();
		latitude = preferences.getLatitude();
		longitude = preferences.getLongitude();
		
	}

	@Override
	protected Response doInBackground(Void... params) {
		
		JSONObject jsonObject = null;
		
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
		
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(URL);
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("AppuserID", appUserId));
		nameValuePairs.add(new BasicNameValuePair("Lat", latitude ));
		nameValuePairs.add(new BasicNameValuePair("Lng", longitude ));
		nameValuePairs.add(new BasicNameValuePair("BusinessID", businessID));
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			jsonObject = new JSONObject(EntityUtils.toString(entity));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new Response(jsonObject);
	}
	
	
	@Override
	protected void onPostExecute(Response response) {
		super.onPostExecute(response);
		
		if (response == null) {
			return;
		}
		
		mCallBack.callBack(response);
	}
}
