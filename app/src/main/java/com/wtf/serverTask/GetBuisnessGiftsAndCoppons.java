/**
 * 
 */
package com.wtf.serverTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wtf.dataBase.Preferences;
import com.wtf.objects.BuisnessGiftsAndCoupons;
import com.wtf.objects.Coupon;
import com.wtf.objects.Gift;

/**
 * @author natiapplications
 *
 */
public class GetBuisnessGiftsAndCoppons extends AsyncTask<Void, Void,BuisnessGiftsAndCoupons> {

	

	private Context mContext;
	
	private CallBack<BuisnessGiftsAndCoupons> mCallBack;
	
	private BuisnessGiftsAndCoupons buisnessGiftsAndCoupons = new BuisnessGiftsAndCoupons();

	private String buisnessID;
	
	

	public GetBuisnessGiftsAndCoppons( Context mContext,String buisnessID,
			CallBack<BuisnessGiftsAndCoupons> mCallBack) {
		
		this.mContext = mContext;
		this.buisnessID = buisnessID;
		this.mCallBack = mCallBack;
	}


	@Override
	protected BuisnessGiftsAndCoupons doInBackground(Void... paramss) {
		String url = "http://www.mypushserver.com/dev/bmm/api/getBusinessGiftsAndCoupons";
		
		String userId = Preferences.getInstans(mContext).getUserId();
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(url);

		Log.e("network", buisnessID);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("AppuserID", userId));
		nameValuePairs.add(new BasicNameValuePair("BusinessID", buisnessID));
	
		
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			String string = EntityUtils.toString(entity);
			
			JSONObject jsonObject = new JSONObject(string);
			
			
			Log.e("getbc", "response" + jsonObject.toString());
			
			
			
			JSONObject data = jsonObject.getJSONObject("data");
			
			JSONArray gifts = data.getJSONArray("Gifts");
			JSONArray coupons = data.getJSONArray("Coupons");
			
			ArrayList<Gift> giftsArray = new ArrayList<Gift>();
			ArrayList<Coupon> couponsArray = new ArrayList<Coupon>();
			
			for (int i = 0; i < gifts.length(); i++) {
				Gift temp = new Gift(gifts.getJSONObject(i));
				giftsArray.add(temp);
			}
			for (int i = 0; i < coupons.length(); i++) {
				Coupon temp = new Coupon(coupons.getJSONObject(i));
				couponsArray.add(temp);
			}
			
			buisnessGiftsAndCoupons.setCopupons(couponsArray);
			buisnessGiftsAndCoupons.setGifts(giftsArray);
			
		} catch (Exception e) {
			Log.e("getbc", "error" + e.getMessage());
		} 

		return buisnessGiftsAndCoupons;
	}
	
	
	@Override
	protected void onPostExecute(BuisnessGiftsAndCoupons result) {
		super.onPostExecute(result);
		
		if(result == null){
			return;
		}
		
		mCallBack.callBack(result);
	}

}
 


