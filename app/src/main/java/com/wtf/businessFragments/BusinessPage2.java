package com.wtf.businessFragments;

import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.fragments.BusinessFragment;
import com.wtf.objects.Business;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class BusinessPage2 extends Fragment{

	public static final String TAG = "BusinessPage1";
	
	private Business mBusiness;
	
	

	public static BusinessPage2 newInstance(Business business){
		BusinessPage2 instance = new BusinessPage2();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MainActivity.EXTERA_BUSINESS, business);
		instance.setArguments(bundle);
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mBusiness = (Business)getArguments().getSerializable(MainActivity.EXTERA_BUSINESS);
		return inflater.inflate(R.layout.business_page_2, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		if (mBusiness.getOpening() != null && ! mBusiness.getOpening().equals("")){
			( (TextView) view.findViewById(R.id.business2_hours_txt) ).setText(mBusiness.getOpening());
		}

		view.requestFocus();
	}
}
