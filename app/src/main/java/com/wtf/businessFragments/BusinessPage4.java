package com.wtf.businessFragments;

import java.util.ArrayList;


import com.meg7.widget.SvgImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.adapters.BusinessGiftsAndCouponsListAdapter;
import com.wtf.fragments.BusinessFragment;
import com.wtf.objects.BuisnessGiftsAndCoupons;
import com.wtf.objects.BuisnessGiftsAndCoupons.GiftSummery;
import com.wtf.objects.Business;
import com.wtf.objects.Coupon;
import com.wtf.objects.Gift;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetBuisnessGiftsAndCoppons;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BusinessPage4 extends Fragment implements OnItemClickListener{

	public static final String TAG = "BusinessPage1";
	
	
	private Business mBusiness;
	
	private TextView noCouponsAndGifts;
	private ProgressBar progressBar;
	private ListView giftsAndCouponsListView;
	private BusinessGiftsAndCouponsListAdapter adapter;
	private ArrayList<GiftSummery> giftsAndCouponsArray;
	
	
	
	
	public static BusinessPage4 newInstance(Business business){
		BusinessPage4 instance = new BusinessPage4();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MainActivity.EXTERA_BUSINESS, business);
		instance.setArguments(bundle);
		return instance;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mBusiness = (Business)getArguments().getSerializable(MainActivity.EXTERA_BUSINESS);
		return inflater.inflate(R.layout.business_page_4, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		view.requestFocus();
		noCouponsAndGifts = (TextView)view.findViewById(R.id.no_coupons_tv);
		giftsAndCouponsListView = (ListView)view.findViewById(R.id.gift_and_coupon_list);
		giftsAndCouponsListView.setVisibility(View.INVISIBLE);
		progressBar = (ProgressBar)view.findViewById(R.id.get_gifts_pb);
		

		getBusinessGiftsAndCoupons();
		
	}
	
	private void updateGiftsAndCouponsListView () {
		giftsAndCouponsArray = BMMApplication.getBuisnessGiftsAndCoupons().getAllNames();
		adapter = new BusinessGiftsAndCouponsListAdapter(getActivity(), giftsAndCouponsArray);
		giftsAndCouponsListView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		progressBar.setVisibility(View.INVISIBLE);
		giftsAndCouponsListView.setVisibility(View.VISIBLE);
		giftsAndCouponsListView.setOnItemClickListener(this);
		
		if (giftsAndCouponsArray.size() == 0){
			noCouponsAndGifts.setVisibility(View.VISIBLE);
		}else {
			noCouponsAndGifts.setVisibility(View.GONE);
		}
	}
	
	public void getBusinessGiftsAndCoupons () {
		Log.e("network", mBusiness.getId() + mBusiness.getName());
		GetBuisnessGiftsAndCoppons request = new GetBuisnessGiftsAndCoppons(getActivity(), mBusiness.getId(),
				new CallBack<BuisnessGiftsAndCoupons>() {
					
					@Override
					public void callBack(BuisnessGiftsAndCoupons object) {
						// TODO Auto-generated method stub
						BMMApplication.setBuisnessGiftsAndCoupons(object);
						updateGiftsAndCouponsListView();
						
					}
				});
		request.execute();
	}
	
	public void showCouponDialog (final Coupon coupon){
		
		View view = getActivity().getLayoutInflater().inflate(R.layout.business_giftsand_coupons_dialog, null);
		
		final SvgImageView image = (SvgImageView)view.findViewById(R.id.image);
		TextView title = (TextView)view.findViewById(R.id.title);
		TextView desc = (TextView)view.findViewById(R.id.desc);
		TextView experiant = (TextView)view.findViewById(R.id.experiant);
		
		title.setText(coupon.getTitle());
		desc.setText(coupon.getDescription());
		experiant.setText(coupon.getExpiration());
		
		if (!coupon.getImgUrl().equals("")&&coupon.getImgUrl() != null){
			
			Picasso.with(getActivity())
			.load(coupon.getImgUrl())
			.fit()
			.into(image,new Callback(){
				@Override
				public void onError() {}
				@Override
				public void onSuccess() {
					Picasso.with(getActivity())
					.load(coupon.getImgUrl())
					.fit()
					.into(image);
				}
			});
			
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(view);
		builder.setCancelable(true);
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}
	
	public void showGiftDialog (final Gift gift){
		
		View view = getActivity().getLayoutInflater().inflate(R.layout.business_giftsand_coupons_dialog, null);
		
		final SvgImageView image = (SvgImageView)view.findViewById(R.id.image);
		TextView title = (TextView)view.findViewById(R.id.title);
		TextView desc = (TextView)view.findViewById(R.id.desc);
		TextView experiant = (TextView)view.findViewById(R.id.experiant);
		
		title.setText(gift.getTitle());
		desc.setText(gift.getDescription());
		experiant.setText(gift.getExpriation());
		
		if (gift.getImageURl().equals("")&&gift.getImageURl() != null){
			
			Picasso.with(getActivity())
			.load(gift.getImageURl())
			.fit()
			.into(image,new Callback(){
				@Override
				public void onError() {}
				@Override
				public void onSuccess() {
					Picasso.with(getActivity())
					.load(gift.getImageURl())
					.fit()
					.into(image);
				}
			});
			
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(view);
		builder.setCancelable(true);
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}


	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		GiftSummery pressed = giftsAndCouponsArray.get(position);
		
		if (pressed.type == BuisnessGiftsAndCoupons.TYPE_COUPON){
			Coupon selected = BMMApplication.getBuisnessGiftsAndCoupons().getCopupons().get
					(pressed.indexInParentList);
			showCouponDialog(selected);
		}else if (pressed.type == BuisnessGiftsAndCoupons.TYPE_GIFT){
			Gift selected = BMMApplication.getBuisnessGiftsAndCoupons().getGifts().get
					(pressed.indexInParentList);
			showGiftDialog(selected); 
		}
		
	}
	
	
}
