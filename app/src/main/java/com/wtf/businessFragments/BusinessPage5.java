package com.wtf.businessFragments;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.adapters.GridAdapter;
import com.wtf.dataBase.Preferences;
import com.wtf.fragments.BusinessFragment;
import com.wtf.objects.Business;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.Place;
import com.wtf.objects.Response;
import com.wtf.serverTask.AppUserCheckIn;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetBuisnessCheckingsTask;
import com.wtf.serverTask.SetAppuserBunosPoints;
import com.wtf.utils.GiftUtils;

@SuppressLint("ValidFragment")
public class BusinessPage5 extends Fragment implements OnClickListener,OnItemClickListener{

	public static final String TAG = "BusinessPage5";
	private Business mBusiness;
    private GridView checkInFriendsGridView;
    private GridAdapter adapter;
    
    public static BusinessPage5 newInstance(Business business){
    	BusinessPage5 instance = new BusinessPage5();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MainActivity.EXTERA_BUSINESS, business);
		instance.setArguments(bundle);
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mBusiness = (Business)getArguments().getSerializable(MainActivity.EXTERA_BUSINESS);
		return inflater.inflate(R.layout.business_page_5, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		checkInFriendsGridView = (GridView)view.findViewById(R.id.check_in_friends_gird);
		updateGirdView(new ArrayList<FacebookFriand>());
		getBusinnessCheckings ();
		((Button) view.findViewById(R.id.business5_check_in_btn)).setOnClickListener(this);
		view.requestFocus();
	}
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	private void updateGirdView(ArrayList<FacebookFriand> pictures) {

		adapter = new GridAdapter(getActivity(), pictures);
		checkInFriendsGridView.setAdapter(adapter);
		checkInFriendsGridView.setOnItemClickListener(this);
		adapter.notifyDataSetChanged();
	}

	private void getBusinnessCheckings () {
		GetBuisnessCheckingsTask getBuisnessCheckingsTask = new GetBuisnessCheckingsTask(getActivity(),
				mBusiness.getId(), new CallBack<ArrayList<FacebookFriand>> () {
					@Override
					public void callBack(ArrayList<FacebookFriand> object) {
						updateGirdView(object);
					}
		});
		getBuisnessCheckingsTask.execute();
	}
	
	@Override
	public void onClick(View v) {
		
		if (!Preferences.getInstans(getActivity()).isShowLocation()){
			Toast.makeText(getActivity(), getString(R.string.no_location_foncationality), Toast.LENGTH_LONG).show();
			return;
		}
		if (!checkIfUserCanCheckIn()){
			Toast.makeText(getActivity(),
					"You need to wait minmum 15 mintues from last chek in reporting",
						Toast.LENGTH_LONG).show();
			return;
		}
		
		final ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setMessage(getString(R.string.loading));
		dialog.show();
		AppUserCheckIn appUserCheckIn = new AppUserCheckIn(getActivity(), mBusiness.getId(), new CallBack<Response>() {
			
			@Override
			public void callBack(Response response) {
				
				if (dialog.isShowing()){
					
					dialog.dismiss();
				}
				if(response.isSuccess()){
					Place place = new Place(mBusiness.getName(), mBusiness.getLat(),
							mBusiness.getLng(),mBusiness.getId(),mBusiness.getExternalID());
					place.setCategoryType(BMMApplication.currentCatgoryIcon);
					BMMApplication.setPlaces(getActivity(), place);
					SetAppuserBunosPoints setAppuserBunosPoints = new SetAppuserBunosPoints(getActivity(),
							GiftUtils.CHECK_IN_POINT.value ,
							GiftUtils.CHECK_IN_POINT.actionName);
					setAppuserBunosPoints.execute();
					
					BMMApplication.getCurrentUserProfile().setPoints(BMMApplication.getCurrentUserProfile().getPoints() +
							GiftUtils.CHECK_IN_POINT.value);
				}else{
					Toast.makeText(getActivity(), response.getErrorMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
		
		appUserCheckIn.execute();
		updateDriverLocation();
		getBusinnessCheckings();
	}
	
	private boolean checkIfUserCanCheckIn () {
		try {
			final long queterHour = 15*60*1000;
			long lastCheckIn = Preferences.getInstans(getActivity()).getLastCheckIn();
			if (lastCheckIn > 0){
				long currentTime = System.currentTimeMillis();
				if (lastCheckIn + queterHour < currentTime ){
					Preferences.getInstans(getActivity()).setLastCheckIn(currentTime);
					return true;
				}else{
					return false;
				}
			}else{
				Preferences.getInstans(getActivity()).setLastCheckIn
						(System.currentTimeMillis());
				return true;
			}
		} catch (Exception e) {
			return true;
		}
		
		
	}
	
	private synchronized void updateDriverLocation (){
		Log.e(TAG, "update location on server");
		Intent filterRes = new Intent();
		filterRes.setAction("com.wtf.intent.action.LOCATION");
	    BMMApplication.getAppContext().sendBroadcast(filterRes);
	}
	
	
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		FacebookFriand temp = adapter.getItem(arg2);
		if (temp != null){
			String fbid = adapter.getItem(arg2).getFBID();
			if (fbid != null && !fbid.isEmpty()){
				launchFacebook(fbid);
			}
		}
		
	}
	
	public final void launchFacebook(String id) {
        final String urlFb = "fb://profile/"+id;
        Intent intent = new Intent(Intent.ACTION_VIEW);
       
        final String urlBrowser = "https://www.facebook.com/"+id;
        intent.setData(Uri.parse(urlBrowser));
        startActivity(intent);
    }
}
