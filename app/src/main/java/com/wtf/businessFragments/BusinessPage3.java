package com.wtf.businessFragments;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.fragments.BusinessFragment;
import com.wtf.fragments.SearchFragment;
import com.wtf.objects.Business;
import com.wtf.objects.Place;
import com.wtf.objects.Rating;
import com.wtf.objects.Response;
import com.wtf.serverTask.AppUserCheckIn;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.SendUserRatingTask;
import com.wtf.serverTask.SetAppuserBunosPoints;
import com.wtf.utils.GiftUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class BusinessPage3 extends Fragment implements OnSeekBarChangeListener,OnClickListener{

	public static final String TAG = "BusinessPage1";
	
	private Business mBusiness;
	
	private SeekBar servicePb;
	private SeekBar atmosfarePb;
	private SeekBar recommandFriandsPb;
	private SeekBar howPricePb;
	private Button sendReport;
	
	private int serviceValue;
	private int atmosfareValue;
	private int recommandFriandsValue;
	private int howPriceValue;
	
	
	public static BusinessPage3 newInstance(Business business){
		BusinessPage3 instance = new BusinessPage3();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MainActivity.EXTERA_BUSINESS, business);
		instance.setArguments(bundle);
		return instance;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mBusiness = (Business)getArguments().getSerializable(MainActivity.EXTERA_BUSINESS);
		return inflater.inflate(R.layout.business_page_3, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		servicePb = (SeekBar)view.findViewById(R.id.service_pb);
		atmosfarePb = (SeekBar)view.findViewById(R.id.atmosphere_pb);
		recommandFriandsPb = (SeekBar)view.findViewById(R.id.recommand_friendes_pb);
		howPricePb = (SeekBar)view.findViewById(R.id.how_is_price_pb);
		sendReport = (Button)view.findViewById(R.id.send_rating);
		
		servicePb.setOnSeekBarChangeListener(this);
		atmosfarePb.setOnSeekBarChangeListener(this);
		recommandFriandsPb.setOnSeekBarChangeListener(this);
		howPricePb.setOnSeekBarChangeListener(this);
		sendReport.setOnClickListener(this);
		

		view.requestFocus();
		
		
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		sendUserRatingToTheServer ();
	}
	
	private void sendUserRatingToTheServer () {
		
		
		if (!checkIfUserCanRate()){
			Toast.makeText(getActivity(),
					"You need to wait minmum 15 mintues from last Rate reporting",
						Toast.LENGTH_LONG).show();
			return;
		}
		
		final ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setMessage(getString(R.string.loading));
		dialog.show();
		SendUserRatingTask sendUserRatingTask = new SendUserRatingTask(getActivity(), mBusiness.getId(),
				serviceValue,atmosfareValue,recommandFriandsValue,howPriceValue,
				new CallBack<Response>() {
			
			@Override
			public void callBack(Response response) {
				dialog.dismiss();
				if(response.isSuccess()){
					Rating rating = new Rating(mBusiness.getName(), getAverageRating(),
							BMMApplication.currentCatgoryIcon,mBusiness.getId(),mBusiness.getExternalID());
					rating.setCategoryType(BMMApplication.currentCatgoryIcon);
					BMMApplication.setRatings(getActivity(), rating);
				}else{
					Toast.makeText(getActivity(), response.getErrorMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
		
		sendUserRatingTask.execute();
		updateDriverLocation();
		if (Preferences.getInstans(getActivity()).isShowLocation()){
			AppUserCheckIn appUserCheckIn = new AppUserCheckIn(getActivity(),
					mBusiness.getId(), new CallBack<Response>() {
						public void callBack(Response response) {}
					});
			appUserCheckIn.execute();
		}
		
		SetAppuserBunosPoints setAppuserBunosPoints = new SetAppuserBunosPoints(getActivity(),
				GiftUtils.RANK_POINT.value,
				GiftUtils.RANK_POINT.actionName);
		setAppuserBunosPoints.execute();
		BMMApplication.getCurrentUserProfile().setPoints(BMMApplication.getCurrentUserProfile().getPoints() +
				GiftUtils.RANK_POINT.value);
		
	}

	
	private boolean checkIfUserCanRate () {
		try {
			final long queterHour = 15*60*1000;
			long lastCheckIn = Preferences.getInstans(getActivity()).getLasRate();
			if (lastCheckIn > 0){
				long currentTime = System.currentTimeMillis();
				if (lastCheckIn + queterHour < currentTime ){
					Preferences.getInstans(getActivity()).setLastRate(currentTime);
					return true;
				}else{
					return false;
				}
			}else{
				Preferences.getInstans(getActivity()).setLastRate(System.currentTimeMillis());
				return true;
			}
		} catch (Exception e) {
			return true;
		}
		
		
	}
	
	private float getAverageRating () {
		int sum = atmosfareValue + serviceValue + recommandFriandsValue + howPriceValue;
		if (sum <=  0){
			return 0;
		}
		float average = sum/4 ;
		return average;
	}
	
	
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		switch (seekBar.getId()) {
		case R.id.service_pb:
			this.serviceValue = progress;
			break;

		case R.id.atmosphere_pb:
			this.atmosfareValue = progress;
			break;
		case R.id.recommand_friendes_pb:
			this.recommandFriandsValue = progress;
			break;
		case R.id.how_is_price_pb:
			this.howPriceValue = progress;
			break;
		}
		
	}

	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
	
	private synchronized void updateDriverLocation (){
		Log.e(TAG, "update location on server");
		Intent filterRes = new Intent();
		filterRes.setAction("com.wtf.intent.action.LOCATION");
	    BMMApplication.getAppContext().sendBroadcast(filterRes);
	}
	
}
