package com.wtf.businessFragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Picasso;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.customView.CircleImageView;
import com.wtf.fragments.BusinessFragment;
import com.wtf.objects.Business;

@SuppressLint("ValidFragment")
public class BusinessPage1 extends Fragment implements OnClickListener {

	
	public static final String TAG = "BusinessPage1";

	private Business mBusiness;

	private TextView mTitleTv, mAddressTv, mPhoneTv, mTimeActiveTv;

	private ImageView findUsFacebook, webSite;

	

	public static BusinessPage1 newInstance(Business business){
		BusinessPage1 instance = new BusinessPage1();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MainActivity.EXTERA_BUSINESS, business);
		instance.setArguments(bundle);
		return instance;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mBusiness = (Business)getArguments().getSerializable(MainActivity.EXTERA_BUSINESS);
		return inflater.inflate(R.layout.business_page_1, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mTitleTv = (TextView) view.findViewById(R.id.business1_textView_title);
		mAddressTv = (TextView) view
				.findViewById(R.id.business1_textView_address);
		mPhoneTv = (TextView) view.findViewById(R.id.business1_textView_phone);
		mTimeActiveTv = (TextView) view
				.findViewById(R.id.business1_textView_time_active);
		findUsFacebook = (ImageView) view.findViewById(R.id.facebook);
		webSite = (ImageView) view.findViewById(R.id.web_site);

		if (!mBusiness.getName().equals("")) {
			mTitleTv.setText(mBusiness.getName());
		}
		if (!mBusiness.getPhone().equals("")) {
			mPhoneTv.setText(mBusiness.getPhone());
		}
		if (!mBusiness.getBusinessDesc().equals("")) {
			String descToSet = mBusiness.getBusinessDesc();
			if (descToSet.length() >= 35) {
				descToSet = descToSet.substring(0, 32);
				linkifyReadMore(mTimeActiveTv, descToSet);
			} else {
				mTimeActiveTv.setText(descToSet);
			}
		}
		mTimeActiveTv.setOnClickListener(this);
		mAddressTv
				.setText(mBusiness.getAddress().equals("") ? "Navigate To Business"
						: mBusiness.getAddress());

		if (!mBusiness.getPhone().equals("")) {
			mPhoneTv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent callIntent = new Intent(Intent.ACTION_DIAL);
					callIntent.setData(Uri.parse("tel:" + mBusiness.getPhone()));
					startActivity(callIntent);

				}
			});
		}

		mAddressTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					String locationCordi = mBusiness.getLat() + ","
							+ mBusiness.getLng();
					Uri location = Uri.parse("geo:0,0?q=" + locationCordi);
					Intent intent = new Intent(Intent.ACTION_VIEW, location);
					startActivity(intent);
				} catch (Exception ex) {
				}
			}
		});

		webSite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// lunch web site
				if (mBusiness.getWebsite() != null
						&& !mBusiness.getWebsite().equals("")) {
					String url = mBusiness.getWebsite();
					if (!url.contains("http://")) {
						url = "http://" + mBusiness.getWebsite();
					}
					try {
						Intent intent = new Intent(
								android.content.Intent.ACTION_VIEW, Uri
										.parse(url));
						startActivity(intent);
					} catch (Exception e) {
						Toast.makeText(getActivity(),
								getString(R.string.web_address_not_found),
								Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getActivity(),
							getString(R.string.web_address_not_found),
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		findUsFacebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mBusiness.getFacebookLink() != null
						&& !mBusiness.getFacebookLink().equals("")) {
					startActivity(getOpenFacebookIntent(getActivity(),
							mBusiness.getFacebookLink()));
				} else {
					Toast.makeText(getActivity(),
							getString(R.string.facebook_not_found),
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		view.requestFocus();
	}

	public static Intent getOpenFacebookIntent(Context context, String link) {

		try {
			context.getPackageManager()
					.getPackageInfo("com.facebook.katana", 0);
			return new Intent(Intent.ACTION_VIEW, Uri.parse(link));
		} catch (Exception e) {
			return new Intent(Intent.ACTION_VIEW, Uri.parse(link));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View arg0) {
		if (!mBusiness.getBusinessDesc().equals("")) {
			mTimeActiveTv.setText(mBusiness.getBusinessDesc());
		}
		LinearLayout.LayoutParams txtParams = (LinearLayout.LayoutParams) mTimeActiveTv
				.getLayoutParams();
		txtParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
		mTimeActiveTv.setLayoutParams(txtParams);

	}

	private void linkifyReadMore(TextView toLink, String toSet) {
		/*
		 * Pattern pattern = Pattern.compile("read more");
		 * toLink.setText(toSet); Linkify.addLinks(toLink,pattern, "");
		 */
		String text = "<font color=#000000>" + toSet
				+ "</font> <font color=#00008B>...</font>";
		toLink.setText(Html.fromHtml(text));
	}

}
