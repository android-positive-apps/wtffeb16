package com.wtf.dataBase;

import java.util.Calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.DateFormat;

public class Preferences {

	private static Preferences mInstance = null;
	
	private SharedPreferences mPrefs = null;
	
	public static final String GCM = "gcm";
	
	public static final String LONGITUDE = "longitude";
	
	public static final String LATITUDE = "latitude";
	
	public static final String RADIUS = "radius";
	
	public static final String USER_ID = "user_id";
	
	public static final String FIRST_TIME_USE = "first_time_use";
	
	public static final String DATE_REGISTRATION = "date_registration";
	
	public static final String SHOW_LOCATION = "show_location";
	
	public static final String PROFILE_IMG_PATH = "profile_img_path";
	
	public static final String SCREEN_HEGIHT = "screen_height";
	
	public static final String SCREEN_WIDTH = "screen_width";

	private static final String FACEBOOK_ID = "facebook_id";

	private static final String FACEBOOK_IMAGE = "facebook_image";
	
	private static final String LOCATION_SERVICE_RUNNING = "Servic_running";
	
    private static final String NAME = "name";
    
    private static final String SHOW_NO_COMPPAS_DIALOG = "ShowNoCompasDialog";
    
    private static final String FACEBOOK_ACCESS_TOKEN = "FacebookAccessTokenF";
	
    private static final String LAST_CHECK_IN = "lastCheckIn";
    private static final String LAST_RATE = "lastRate";
    
	private Preferences (Context context){
		
		mPrefs = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
	}
	
	public static Preferences getInstans(Context context){
		if(mInstance == null){
			mInstance = new Preferences(context);
		}
		return mInstance;
	}
	
	// getters
	
	
	public String getFacebookId() {
		return mPrefs.getString(FACEBOOK_ID, "0");
		
	}
	
	public String getGCM() {
		return mPrefs.getString(GCM, "");
	}
	
	public String getLongitude() {
		return mPrefs.getString(LONGITUDE, "0.0");
	}
	
	public String getLatitude() {
		return mPrefs.getString(LATITUDE, "0.0");
	}
	
	public String getUserId() {
		return mPrefs.getString(USER_ID, "0");
	}
	
	public int getScreenWidth() {
		return mPrefs.getInt(SCREEN_WIDTH, 0);
	}
	
	public String getDateRegistration() {
		
		String date = (String) DateFormat.format("dd/MM/yyy", Calendar.getInstance());
		return mPrefs.getString(DATE_REGISTRATION, date);
	}

	public int getScreenHeight() {
		return mPrefs.getInt(SCREEN_HEGIHT, 0);
	}

	public int getRadius() {
		return mPrefs.getInt(RADIUS, 10);
	}

	public boolean isShowLocation() {
		return mPrefs.getBoolean(SHOW_LOCATION, true);
	}
	
	public boolean isFirstTimeUse() {
		return mPrefs.getBoolean(FIRST_TIME_USE, true);
	}
	
	public boolean isShowNoCompasDialog() {
		return mPrefs.getBoolean(SHOW_NO_COMPPAS_DIALOG, false);
	}
	
	public String getProfileImgPath() {
		return mPrefs.getString(PROFILE_IMG_PATH, "");
	}
	
	
	public long getLastCheckIn() {
		return mPrefs.getLong(LAST_CHECK_IN, 0);
	}
	
	public long getLasRate() {
		return mPrefs.getLong(LAST_RATE, 0);
	}
	
	
	// setters
	
	public void setLastCheckIn(long set) {
		mPrefs.edit().putLong(LAST_CHECK_IN, set).commit();
	}
	
	public void setLastRate(long set) {
		mPrefs.edit().putLong(LAST_RATE, set).commit();
	}
	
	
	public void setGCM(String gcm) {
		mPrefs.edit().putString(GCM, gcm).commit();
	}
	
	public void setLongitude(String longitude) {
		mPrefs.edit().putString(LONGITUDE, longitude).commit();
	}
	
	public void setLatitude(String latitude) {
		mPrefs.edit().putString(LATITUDE, latitude).commit();
	}
	
	public void setUserId(String userId) {
		mPrefs.edit().putString(USER_ID, userId).commit();
	}
	
	public void setScreenWidth(int width) {
		mPrefs.edit().putInt(SCREEN_WIDTH, width).commit();
	}
	
	public void setDateRegistration(String date) {
		mPrefs.edit().putString(DATE_REGISTRATION, date).commit();
	}
	
	public void setScreenHeight(int height) {
		mPrefs.edit().putInt(SCREEN_HEGIHT, height).commit();
	}

	public void setRadius(int radius) {
		mPrefs.edit().putInt(RADIUS, radius).commit();
	}

	public void setShowLocation(boolean bool) {
		mPrefs.edit().putBoolean(SHOW_LOCATION, bool).commit();
	}
	public void setShowNoCompassDialog(boolean bool) {
		mPrefs.edit().putBoolean(SHOW_NO_COMPPAS_DIALOG, bool).commit();
	}
	public void setFirstTimeUse(boolean bool) {
		mPrefs.edit().putBoolean(FIRST_TIME_USE, bool).commit();
	}
	
	public void setProfileImgPath(String path) {
		mPrefs.edit().putString(PROFILE_IMG_PATH, path).commit();
	}

	public void setFacebookId(String id) {
		mPrefs.edit().putString(FACEBOOK_ID, id).commit();
		
	}
	
	public void setFacebookImageProfile (String imageUrl) {
		mPrefs.edit().putString(FACEBOOK_IMAGE, imageUrl).commit();
	}
	
	
	public void setFacebookAccessTocke (String tocken) {
		mPrefs.edit().putString(FACEBOOK_ACCESS_TOKEN, tocken).commit();
	}
	public String getFacebookImageProfile () {
		
		return mPrefs.getString(FACEBOOK_IMAGE, "0");
		
	}
	
	public void setServiceRunning (boolean isRun) {
		mPrefs.edit().putBoolean(LOCATION_SERVICE_RUNNING, isRun).commit();
	}
	
	public boolean isLocationServiceRunning () {
		
		return mPrefs.getBoolean(LOCATION_SERVICE_RUNNING, false);
		
	}
	
	public void setName (String isRun) {
		mPrefs.edit().putString(NAME, isRun).commit();
	}
	
	public String getName () {
		return mPrefs.getString(NAME, "");
	}
	
	public String getFacebookAccessTocken () {
		return mPrefs.getString(FACEBOOK_ACCESS_TOKEN, "");
	}
	
	
	
	
	
}
