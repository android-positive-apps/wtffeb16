/**
 * 
 */
package com.wtf.dataBase;

import java.util.ArrayList;

import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.InitiateMatchResult;
import com.wtf.activity.BMMApplication;
import com.wtf.objects.FacebookFriand;
import com.wtf.storage.StorageListener;

/**
 * @author natiapplications
 *
 */
public class UserFriendsManager {
	
	
	private ArrayList<FacebookFriand> friands;
	
	private static UserFriendsManager instance;
	
	private UserFriendsManager () {
		read(null);
	}
	
	public static UserFriendsManager getInstance () {
		if (instance == null){
			instance = new UserFriendsManager();
		}
		return instance;
	}
	
	
	public void read (final FriendsCallbac callbac) {
		
		FacebookFriand.readFriendsFromStorage(BMMApplication.getAppContext(), new StorageListener() {
			
			@Override
			public void onStorageDataReceived(Object data) {
				if (data != null){
					friands = (ArrayList<FacebookFriand>)data;
					
				}else{
					friands = new ArrayList<FacebookFriand>();
				}
				if (callbac != null){
					callbac.onFriendsArrived(friands);
				}
				
			}
		});
	}
	
	public void get (final FriendsCallbac callbac) {
		if (friands != null){
			if (callbac != null){
				callbac.onFriendsArrived(friands);
			}
		}else{
			read(callbac);
		}
	}
	
	
	public ArrayList<FacebookFriand> get () {
		if (friands != null){
			return friands;
		}
		return new ArrayList<FacebookFriand>();
	}
	
	public void addFriends (final ArrayList<FacebookFriand> toAdd) {
		if (friands == null){
			read(new FriendsCallbac() {
				@Override
				public void onFriendsArrived(ArrayList<FacebookFriand> result) {
					if (result != null){
						friands = result;
						for (int i = 0; i < toAdd.size(); i++) {
							addFriend(toAdd.get(i));
						}
					}else{
						friands = toAdd;
					}
					saveToStorage();
				}
			});
		}else{
			for (int i = 0; i < toAdd.size(); i++) {
				addFriend(toAdd.get(i));
			}
			saveToStorage();
		}
		
	}
	
	
	public void addFriend (FacebookFriand toAdd){
		if (!friands.contains(toAdd)){
			friands.add(toAdd);
		}
	}
	
	
	public void saveToStorage () {
		FacebookFriand.saveFriendsToStorage(BMMApplication.getAppContext(), friands);
	}
	
	public interface FriendsCallbac {
		public void onFriendsArrived (ArrayList<FacebookFriand> result);
	}

}
