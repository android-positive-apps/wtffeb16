/**
 * 
 */
package com.wtf.location;

import java.lang.reflect.WildcardType;

import com.wtf.activity.BMMApplication;
import com.wtf.dataBase.Preferences;
import com.wtf.location.LocationTransmitterThread;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

/**
 * @author natiapplications
 *
 */
public class LocationService extends Service {

	
	private LocationManager locationManager;
	private LocationListener locationListener;
	private LocationTransmitterThread locationTransmitterThread;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.d("LOC_RECEIVER", "location service start");
		Preferences.getInstans(BMMApplication.getAppContext()).setServiceRunning(true);
		locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		
		addLocationListener();
		startLocationTransmitterThread();
		return super.onStartCommand(intent, flags, startId);
	}
	
	
	/**
	 * 
	 */
	private void startLocationTransmitterThread() {
		try {
			LocationTransmitterThread.getInstance(this).cancel();
		} catch (Exception e) {}
		
		LocationTransmitterThread.getInstance(this).start();
	}

	private void addLocationListener()
	{
	    new Thread(new Runnable(){
	        public void run(){
	            try{
	                Looper.prepare();
	               
	                while (true){
	                	Log.d("LOC_RECEIVER", "trying to get location service!");
	                	Criteria c = new Criteria();
		                c.setAccuracy(Criteria.ACCURACY_FINE);
		                final String PROVIDER = locationManager.getBestProvider(c, false);
	                	boolean isGpsEnabled = locationManager.isProviderEnabled(PROVIDER);
	                	if (isGpsEnabled){
		                	Log.e("LOC_RECEIVER", "location service found and started");
		                	try {
		                		locationManager.removeUpdates(locationListener);
							} catch (Exception e) {}
		                	locationListener = new MyLocationListener();
			                locationManager.requestLocationUpdates(PROVIDER, 200000, 0, locationListener);
			                Location location = locationManager.getLastKnownLocation(PROVIDER);
			                updateLocation(location);
			                break;
		                }
	                	Thread.sleep(30000);
	                }
	                Looper.loop();
	            }catch(Exception ex){
	                ex.printStackTrace();
	            }
	            Log.e("LOC_RECEIVER", "add listenr thread finished");
	        }
	    }, "LocationThread").start();
	    
	}

	public static void updateLocation(Location location)
	{
		if (location == null){
			return;
		}
		
	    Context appCtx = BMMApplication.getAppContext();
	    double latitude, longitude;
	    latitude = location.getLatitude();
	    longitude = location.getLongitude();
	    Log.i("LOC_RECEIVER", "location change" + latitude+","+longitude);
	    Preferences.getInstans(appCtx).setLatitude(latitude+"");
    	Preferences.getInstans(appCtx).setLongitude(longitude+"");
	}


	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		//locationManager.removeUpdates(this);
		Log.e("LOC_RECEIVER", "location service stoped");
		try {
			locationManager.removeUpdates(locationListener);
			LocationTransmitterThread.getInstance(this).cancel();
		} catch (Exception e) {}
	}

	class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			Log.e("LOC_RECEIVER", "location change");
			updateLocation(location);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			Log.i("LOC_RECEIVER", "location status change" + status);
		}

		@Override
		public void onProviderEnabled(String provider) {
			Log.i("LOC_RECEIVER", "location enabled" + provider);
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Log.i("LOC_RECEIVER", "location disabled");
			addLocationListener();
		}

	}

}
