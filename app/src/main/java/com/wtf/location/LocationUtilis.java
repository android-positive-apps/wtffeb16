/**
 * 
 */
package com.wtf.location;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * @author natiapplications
 *
 */
public class LocationUtilis {
	
	
	public static void stopLocationService (Context context) {
		
		try {
			context.stopService(new Intent(context,NewLocationService.class));
			context.stopService(new Intent(context,LocationService.class));
		} catch (Exception e) {}
	}
	
	
	
	public static void startLocationService(Context context)
	{
		if (checkPlayServices(context)){
			Log.e("GoogleApiLocationLog", "new location service");
		    try {
				context.stopService(new Intent(context,NewLocationService.class));
			} catch (Exception e) {}

		    Intent mServiceIntent = new Intent(context, NewLocationService.class);
		    context.startService(mServiceIntent);
		}else{
			Log.e("GoogleApiLocationLog", "regular location service");
		    try {
				context.stopService(new Intent(context,LocationService.class));
			} catch (Exception e) {}

		    Intent mServiceIntent = new Intent(context, LocationService.class);
		    context.startService(mServiceIntent);
		}
		
	}

	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private static boolean checkPlayServices(Context context) {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        return false;
	    }
	    return true;
	}
}
