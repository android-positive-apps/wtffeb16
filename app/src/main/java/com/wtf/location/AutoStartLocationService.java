/**
 * 
 */
package com.wtf.location;

import com.wtf.dataBase.Preferences;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * @author Nati Gabay
 *
 */
public class AutoStartLocationService extends BroadcastReceiver {

	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		 Log.e("LOC_RECEIVER", "AUTO START LOCATION SERVICE");
		if (Preferences.getInstans(context).isShowLocation()){
			try {
				LocationUtilis.startLocationService(context);
			} catch (Exception e) {}
			 
		}
	    

	}

}
