package com.wtf.location;



import com.wtf.activity.BMMApplication;

import android.content.Context;
import android.content.Intent;


public class LocationTransmitterThread extends Thread {
	
	public static final String TAG = "location transmitter";
	
	private Context context;
	private static LocationTransmitterThread instance;
	private  boolean run;
	
	private LocationTransmitterThread (Context context){
	    this.context = context;	
	    run = true;
	}
	
	public static LocationTransmitterThread getInstance (Context context){
		if (instance == null){
			instance = new LocationTransmitterThread(context);
		}
		return instance;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		int miliseconde = 30*1000;
		
		while (run){
			try {
				if (BMMApplication.getCurrentUserProfile() != null){
					if (BMMApplication.getCurrentUserProfile().getUpdateInterval() > 0){
						miliseconde = BMMApplication.getCurrentUserProfile().getUpdateInterval() * 60000;
					}
				}
				updateDriverLocation();
				sleep(miliseconde);
			} catch (InterruptedException e) {}
		}
		
	}
	
	private synchronized void updateDriverLocation (){
		Intent filterRes = new Intent();
		filterRes.setAction("com.wtf.intent.action.LOCATION");
	    BMMApplication.getAppContext().sendBroadcast(filterRes);
	}
	
	public synchronized  void cancel (){
		
		run = false;
		instance = null;
	}
	
	
	

}
