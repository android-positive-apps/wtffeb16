/**
 * 
 */
package com.wtf.location;

import com.wtf.dataBase.Preferences;
import com.wtf.serverTask.UpdateAppuserLocationTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * @author natiapplications
 *
 */
public class LocationReceiver extends BroadcastReceiver {

    double latitude, longitude;

    @Override
    public void onReceive(final Context context, final Intent calledIntent)
    {
        Log.d("LOC_RECEIVER", "Location RECEIVED!");

        updateDriverLocation(context);

    }

    
    private synchronized void updateDriverLocation (Context context){
    	if (Preferences.getInstans(context).getUserId().equals("0")){
    		return;
    	}
		UpdateAppuserLocationTask appuserLocationTask = new UpdateAppuserLocationTask(context);
		appuserLocationTask.start();
	}
}
