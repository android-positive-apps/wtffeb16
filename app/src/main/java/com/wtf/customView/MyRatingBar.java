/**
 * 
 */
package com.wtf.customView;

import com.wtf.activity.R;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

/**
 * @author Nati Gabay
 *
 */
public class MyRatingBar {
	
	
	public static final int ORIANTATION_TOP = 1;
	public static final int ORIANTATION_BOTTOM = 2;
	
	private View rateingBarView;
	private Activity activity;
	private int progress;
	private int oriantation;
	
	private int fillResourse;
	private int emptyResourse;
	private int halfResourse;
	
	private ImageView item1;
	private ImageView item2;
	private ImageView item3;
	private ImageView item4;
	private ImageView item5;
	
	private ImageView[] items = new ImageView[5];
	
	public MyRatingBar (Activity activity,int oriantation,int fillRes, int emptyRes, int halfRes){
		this.activity = activity;
		this.oriantation = oriantation;
		this.fillResourse = fillRes;
		this.emptyResourse = emptyRes;
		this.halfResourse = halfRes;
		if (oriantation == ORIANTATION_TOP){
			rateingBarView = activity.getLayoutInflater().inflate(R.layout.my_rating_bar_top, null);
		}else if (oriantation == ORIANTATION_BOTTOM){
			rateingBarView = activity.getLayoutInflater().inflate(R.layout.my_rating_bar_bottom, null);
		}
		bulidview();
	}

	/**
	 * 
	 */
	private void bulidview() {
		
		item1 = (ImageView)rateingBarView.findViewById(R.id.item_1);
		item2 = (ImageView)rateingBarView.findViewById(R.id.item_2);
		item3 = (ImageView)rateingBarView.findViewById(R.id.item_3);
		item4 = (ImageView)rateingBarView.findViewById(R.id.item_4);
		item5 = (ImageView)rateingBarView.findViewById(R.id.item_5);
		
		items[0] = item1;
		items[1] = item2;
		items[2] = item3;
		items[3] = item4;
		items[4] = item5;
		
		setProgress(this.progress);
	}


	/**
	 * @param progress2
	 */
	public void setProgress(int progress) {

		this.progress = progress;
		if(this.progress == 0){
			setFilProgress
			(emptyResourse,emptyResourse,emptyResourse,emptyResourse,emptyResourse);
		}else if (this.progress > 0 && this.progress <= 20) {
			if (this.progress < 10) {
				setFilProgress
				(halfResourse,emptyResourse,emptyResourse,emptyResourse,emptyResourse);
			} else {
				setFilProgress
				(fillResourse,emptyResourse,emptyResourse,emptyResourse,emptyResourse);
			}
		} else if (this.progress > 20 && this.progress <= 40) {
			if (this.progress <= 30) {
				setFilProgress
				(fillResourse,halfResourse,emptyResourse,emptyResourse,emptyResourse);
			} else {
				setFilProgress
				(fillResourse,fillResourse,emptyResourse,emptyResourse,emptyResourse);
			}
		} else if (this.progress > 40 && this.progress <= 60) {
			if (this.progress <= 50) {
				setFilProgress
				(fillResourse,fillResourse,halfResourse,emptyResourse,emptyResourse);
			} else {
				setFilProgress
				(fillResourse,fillResourse,fillResourse,emptyResourse,emptyResourse);
			}
		} else if (this.progress > 60 && this.progress <= 80) {
			if (this.progress <= 70) {
				setFilProgress
				(fillResourse,fillResourse,fillResourse,halfResourse,emptyResourse);
			} else {
				setFilProgress
				(fillResourse,fillResourse,fillResourse,fillResourse,emptyResourse);
			}
		} else if (this.progress > 80 && this.progress <= 100) {
			if (this.progress <= 90) {
				setFilProgress
				(fillResourse,fillResourse,fillResourse,fillResourse,halfResourse);
			} else {
				setFilProgress
				(fillResourse,fillResourse,fillResourse,fillResourse,fillResourse);
			}
		}

	}
	
	private void setFilProgress(int resItem1,int resItem2,int resItem3,int resItem4,int resItem5){
		
		this.item1.setBackgroundResource(resItem1);
		this.item2.setBackgroundResource(resItem2);
		this.item3.setBackgroundResource(resItem3);
		this.item4.setBackgroundResource(resItem4);
		this.item5.setBackgroundResource(resItem5);
	}
	
	public int getProgress (){
		return this.progress;
	}
	
	public View getRatingBarView (){
		return this.rateingBarView;
	}
	
	public void setItemsSize (int size){
		
		
			for (int i = 0; i < items.length; i++) {
				RelativeLayout.LayoutParams item1Params = (RelativeLayout.LayoutParams) items[i]
						.getLayoutParams();
				item1Params.height = size;
				item1Params.width = size;
				items[i].setLayoutParams(item1Params);
				
			}
		
		
	}
	

}
