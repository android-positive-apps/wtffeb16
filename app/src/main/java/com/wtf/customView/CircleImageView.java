
package com.wtf.customView;

import com.wtf.utils.BitmapUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;


public class CircleImageView  extends ImageView{

    private boolean mShowStroke = true;
    private Bitmap mBitmap;
    private int mPendingResource = -1;
    Paint mShaderPaint = new Paint();
    Paint mStrokPaint = new Paint();
    private boolean mPendingBitmap = false;

    private boolean shadowEnabled = false;
    private int strokeColor = Color.parseColor("#afafaf");
    private int strokeWidth = 2;

    public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleImageView(Context context) {
        super(context);
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    @Override
    public void setImageBitmap(Bitmap bm) {

        mBitmap = bm;
        mPendingResource = -1;

        if (mBitmap != null) {

            if (getWidth() != 0 && getHeight() != 0) {
                cropImageFromBitmap();
            }
        } else {
            mPendingBitmap = true;
        }
    }

    @Override
    public void setImageResource(int resId) {
        
        mPendingResource = resId;
        mBitmap = null;

        if (getWidth() != 0 && getHeight() != 0) {

            createImageFromResource();
            invalidate();
        } else {
            mPendingResource = resId;
            mPendingBitmap = false;
        }
    }

    private void createImageFromResource() {
        mBitmap = BitmapUtils.getSampleBitmapFromResource(getContext(), mPendingResource,
                getWidth(), getHeight());

        mPendingResource = -1;
        cropImageFromBitmap();
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        mPendingResource = -1;

        mBitmap = BitmapUtils.drawableToBitmap(drawable);

        if (getWidth() != 0 && getHeight() != 0) {
            cropImageFromBitmap();
            invalidate();
        } else {
            mPendingBitmap = true;
        }
    }

    private void cropImageFromBitmap() {
        cropImage();
        mPendingResource = -1;
        invalidate();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (mPendingBitmap) {
            mPendingBitmap = false;
            cropImageFromBitmap();
        } else if (mPendingResource != -1) {
            createImageFromResource();
            invalidate();
        }
    }

    private void cropImage() {
        if (mBitmap == null) {
            return;
        }

        int dwidth = mBitmap.getWidth();
        int dheight = mBitmap.getHeight();

        int vwidth = getWidth() - getPaddingLeft() - getPaddingRight();
        int vheight = getHeight() - getPaddingTop() - getPaddingBottom();

        BitmapShader shader;
        shader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        Matrix localmatrix = new Matrix();

        /**
         * applying center crop mode
         */
        float scale;
        float dx = 0, dy = 0;

        if (dwidth * vheight > vwidth * dheight) {
            scale = (float) vheight / (float) dheight;
            dx = (vwidth - dwidth * scale) * 0.5f;
        } else {
            scale = (float) vwidth / (float) dwidth;
            dy = (vheight - dheight * scale) * 0.5f;
        }

        localmatrix.setScale(scale, scale);
        localmatrix.postTranslate(dx, dy);
        shader.setLocalMatrix(localmatrix);

        mShaderPaint.setAntiAlias(true);
        mShaderPaint.setShader(shader);
    }

    private Paint mShadowOvelayPaint = new Paint();

    @Override
    protected void onDraw(Canvas canvas) {

        int circleCenter = getWidth() / 2;

        mStrokPaint.setStyle(Style.STROKE);
        mStrokPaint.setStrokeWidth(strokeWidth);
        mStrokPaint.setColor(strokeColor);
        mStrokPaint.setAntiAlias(true);

        mShadowOvelayPaint.setStyle(Style.FILL);
        mShadowOvelayPaint.setColor(Color.parseColor("#66333333"));

        if (mShowStroke) {
            canvas.drawCircle(circleCenter, circleCenter, (getHeight() / 2) - strokeWidth, mStrokPaint);
        }

        int strokPadding = mShowStroke ? strokeWidth : 2;

        if (mBitmap != null && mShaderPaint != null) {
            canvas.drawCircle(circleCenter, circleCenter, getHeight() / 2 - strokPadding, mShaderPaint);
        }
        if (shadowEnabled) {
            canvas.drawCircle(circleCenter, circleCenter, getHeight() / 2 - strokPadding, mShadowOvelayPaint);
        }
    }

    public boolean isShowStrokeEnabled() {
        return mShowStroke;
    }

    public void setStrokeEnabled(boolean enabled) {
        this.mShowStroke = enabled;
        invalidate();
    }

    public boolean isShadowEnabled() {
        return shadowEnabled;
    }

    public void setShadowEnabled(boolean enabled) {
        shadowEnabled = enabled;
        invalidate();
    }

    public int getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
    }
}
