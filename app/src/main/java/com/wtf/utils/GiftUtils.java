/**
 * 
 */
package com.wtf.utils;

import com.wtf.activity.R;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;




/**
 * @author natiapplications
 *
 */
public class GiftUtils {
	
	public static Point CHECK_IN_POINT = new Point(1, "Chek in");
	public static Point RANK_POINT = new Point(2, "Rank");
	public static Point SHARE_POINT = new Point(1, "Shere");
	
	public static final int GIFT_1 = 10;
	public static final int GIFT_2 = 20;
	public static final int GIFT_3 = 30;
	public static final int GIFT_4 = 40;
	
	public static final int GIFT_TYPE_1 = 1;
	public static final int GIFT_TYPE_2 = 2;
	public static final int GIFT_TYPE_3 = 3;
	public static final int GIFT_TYPE_4 = 4;
	
	public static int getUserGiftsByUserPoints (int userPoints) {
		
		if (userPoints >= GIFT_4){
			return 4;
		}else if (userPoints >= GIFT_3){
			return 3;
		}else if (userPoints >= GIFT_2){
			return 2;
		}else if (userPoints >= GIFT_1){
			return 1;
		}
		return 0;
	}
	
	public static class Point {
		public int value;
		public String actionName;
		
		
		public Point(int value, String name) {
			this.value = value;
			this.actionName = name;
		}
	}
	
    public static void showResultToast (Activity activity,String message) {
		
		LayoutInflater inflater = activity.getLayoutInflater();
		View layout = inflater.inflate(R.layout.my_custom_toast_dialog, null);
		TextView textResult = (TextView)layout.findViewById(R.id.toast_message);
		textResult.setText(message);

		Toast toast = new Toast(activity);
		toast.setGravity(Gravity.BOTTOM|Gravity.LEFT, 0, 0);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();
		
	}

}


