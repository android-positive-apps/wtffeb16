package com.wtf.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class DrawableUtills {

	public static Drawable[] getDrawableFromData(Context conntext ,int[] data) {
		Drawable[] ret = new Drawable[data.length];
		for (int i = 0; i < data.length; i++) {
			ret[i] = conntext.getResources().getDrawable(data[i]);
		}
		return ret;
	}
}
