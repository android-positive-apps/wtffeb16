/**
 * 
 */
package com.wtf.utils;

import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.util.Log;




import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;
import com.wtf.activity.BMMApplication;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.CreateUpdateUserTask;

/**
 * @author Nati Gabay
 * 
 */
public class SimpleFacebookUtils {

	public static void facebookLogin(Activity activity,
			CallBack<UserProfile> callBack) {
		BMMApplication.mSimpleFacebook.login(new onSimpleFacebookLoginListener(
				activity, callBack));
	}


	
	public static void setUserFacebookFriends () {
		PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
		pictureAttributes.setHeight(250);
		pictureAttributes.setWidth(250);
		pictureAttributes.setType(PictureType.SQUARE);
		
    	Profile.Properties properties = new Profile.Properties.Builder()
	    .add(Properties.ID)
	    .add(Properties.FIRST_NAME)
	    .add(Properties.LAST_NAME)
	    .add(Properties.EMAIL)
	    .add(Properties.PICTURE,pictureAttributes)
	    .build();
		BMMApplication.mSimpleFacebook.getFriends(properties, new OnFriendsListener() {

			public void onComplete(java.util.List<Profile> response) {

				Log.e("FacebookLog", "get friends: " + response.size());

				ArrayList<FacebookFriand> friends = new ArrayList<FacebookFriand>();
				for (int i = 0; i < response.size(); i++) {
					FacebookFriand temp = new FacebookFriand(response.get(i));
					friends.add(temp);
				}
				BMMApplication.setUserFacebookFriends(friends);
			}

			;

		});
	}

	public static void setUserFacebookFriends (OnFriendsListener listener) {
		PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
		pictureAttributes.setHeight(250);
		pictureAttributes.setWidth(250);
		pictureAttributes.setType(PictureType.SQUARE);

		Profile.Properties properties = new Profile.Properties.Builder()
				.add(Properties.ID)
				.add(Properties.FIRST_NAME)
				.add(Properties.LAST_NAME)
				.add(Properties.EMAIL)
				.add(Properties.PICTURE,pictureAttributes)
				.build();
		BMMApplication.mSimpleFacebook.getFriends(properties,listener);
	}

}

/*
 * 
 * listen to login state. when the user login success the onLogin method
 * implement call the getProfile method from the facebook manager for get the
 * user data
 */
class onSimpleFacebookLoginListener implements OnLoginListener {

	private Activity activity;
	private CallBack<UserProfile> callback;

	public onSimpleFacebookLoginListener(Activity activity,
			CallBack<UserProfile> callBack) {
		this.activity = activity;
		this.callback = callBack;
	}



	@Override
	public void onException(Throwable throwable) {
		throwable.printStackTrace();
		Log.e("facebookLog", "on exception: " + throwable.getMessage());
	}

	@Override
	public void onFail(String reason) {
		Log.e("facebookLog", "on fail: " + reason);
	}

	@Override
	public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
		Log.e("facebookLog", "on login:");

		PictureAttributes pictureAttributes = Attributes
				.createPictureAttributes();
		pictureAttributes.setHeight(250);
		pictureAttributes.setWidth(250);
		pictureAttributes.setType(PictureType.SQUARE);


		Profile.Properties properties = new Profile.Properties.Builder()
				.add(Properties.ID).add(Properties.FIRST_NAME)
				.add(Properties.LAST_NAME).add(Properties.EMAIL)
				.add(Properties.PICTURE, pictureAttributes).build();
		BMMApplication.mSimpleFacebook.getProfile(properties,
				new onSimpleFacebookProfileListener(activity, callback));

		Preferences.getInstans(activity).setFacebookAccessTocke(accessToken);
		Log.e("facebookTocken", "tocken" + Preferences.getInstans(activity).getFacebookAccessTocken());
	}

	@Override
	public void onCancel() {
		Log.e("facebookLog", "on cancel:");
	}
}

class onSimpleFacebookProfileListener extends OnProfileListener {

	private Activity activity;
	private CallBack<UserProfile> callback;

	public onSimpleFacebookProfileListener(Activity activity,
			CallBack<UserProfile> callBack) {
		this.activity = activity;
		this.callback = callBack;
	}

	@Override
	public void onComplete(Profile profile) {

		if (profile != null) {
			// get user data
			String id = profile.getId();
			String firstName = profile.getFirstName();
			String lastName = profile.getLastName();
			String image = profile.getPicture();
			Preferences.getInstans(activity).setName(firstName+ " " + lastName);
			Preferences.getInstans(activity).setFacebookId(id);
			Preferences.getInstans(activity).setFacebookImageProfile(image);
			sendUpdateAppUser(activity, callback, firstName, lastName, id);

		}
	}

	private void sendUpdateAppUser(Activity activity,
			CallBack<UserProfile> callbac, String firstName, String lastName,
			String facebookid) {

		Preferences.getInstans(activity).setName(firstName + " " + lastName);
		new CreateUpdateUserTask(activity, firstName, lastName,
				facebookid, callbac).execute();
	}
}
