package com.wtf.utils;

import java.net.URL;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.Business;
import com.wtf.objects.FacebookFriand;

public class Const {

	public static final String BASE_ADDRESS = "http://www.mypushserver.com/dev/bmm/";
	
}
/*
  private void addMarkersForAllSelectedCategorys (final ArrayList<FacebookFriand> friends) {
    	try {
			
		
    	clearAllMarkers();
    	new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				
				for (int j = 0; j < allCategorys.size(); j++) {
					for (int k = 0; k < allCategorys.get(j).size(); k++) {
						
						final Business temp = allCategorys.get(j).get(k);
						try {
							URL url = new URL(temp.getImage());
							final Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
						
						
						if (bmp != null){
							runOnUiThread( new Runnable() {
								public void run() {
									Marker marker = googleMap.addMarker(new MarkerOptions()
									.position(new LatLng(temp.getLat(), temp.getLng()))
									.title(temp.getName().trim()).
									icon(BitmapDescriptorFactory
											.fromBitmap(bmp)));
									marker.setSnippet(temp.getName() + 
											"||" + temp.getAddress() + 
											"||" + temp.getLat() + 
											"||" + temp.getLng()+ 
											"||" + temp.getMarkerRecourse() + 
											"||" + temp.getId()+ 
											"||" + temp.getExternalID() + 
											"||" + temp.getMarkerRecourseSelected()+ 
											"||" + temp.getImage() + 
											"||" + "*"
					 						);
								}
							});
						}
						
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						
					}
					
				}
				
				if (friends != null){
					for (int i = 0; i < friends.size(); i++) {
						final FacebookFriand temp = friends.get(i);
						try {
							
						
						URL url = new URL(temp.getImage());
						final Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
						
						if (bmp != null){
							runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									Marker marker = googleMap.addMarker(new MarkerOptions()
									.position(new LatLng(temp.getLat(), temp.getLon()))
									.title(temp.getFirstName().trim() +" "+ temp.getLastName().trim()).
									icon(BitmapDescriptorFactory
											.fromBitmap(bmp)));
									
									
									marker.setSnippet(temp.getFirstName() + temp.getLastName() + 
											"||"+ "" + 
											"||" + temp.getLat() +
											"||" + temp.getLon()+ 
											"||" + R.drawable.man_location_icon + 
											"||" + "*" + 
											"||" + "*" + 
											"||" + R.drawable.man_location_icon + 
											"||" + temp.getImage() + 
											"||" + temp.getFBID());
									
								}
							});
						}
						
						} catch (Exception e) {
							// TODO: handle exception
						}
						
					}
				}
				
			}
		}).start();
		
			LatLng currentLocation = new LatLng(Double.parseDouble(Preferences.getInstans(this).getLatitude()),
					Double.parseDouble(Preferences.getInstans(this).getLongitude()));
			Marker marker  = googleMap.addMarker(new MarkerOptions()
			.position(currentLocation)
			.title(Preferences.getInstans(this).getName())
			.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.man_location_icon)));
			marker.setSnippet(Preferences.getInstans(this).getName());
			
		} catch (Exception e) {
			Log.e("ext", "ex - " + e.getMessage());
		}
	}
 */