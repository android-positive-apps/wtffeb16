package com.wtf.utils;


import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.wtf.activity.BMMApplication;
import com.wtf.location.LocationReceiver;


public class ServerTaskUtills {

	public static void jsonGetRequest(Context context, String url  ,  Listener<JSONObject> listener , ErrorListener errorListener) {
		RequestQueue requestQueue = Volley.newRequestQueue(context);

		JsonObjectRequest request = new JsonObjectRequest(Method.GET,
				url, null, listener , errorListener);

		requestQueue.add(request);
		requestQueue.start();
	}

	
	public static void startLocationTransmiterService () {
		AlarmManager alarmMgr = (AlarmManager)BMMApplication.getAppContext().
				getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(BMMApplication.getAppContext(),LocationReceiver.class);
		PendingIntent alarmIntent = PendingIntent.getBroadcast(BMMApplication.getAppContext(), 0, intent, 0);
		alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
		        30000,
		        AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntent);
	}
}
