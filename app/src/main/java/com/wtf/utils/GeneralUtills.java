package com.wtf.utils;

import java.util.regex.Pattern;



import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Patterns;

public class GeneralUtills {

	
	public static String getUserEmail(Context context ){
		String possibleEmail = "";
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
		Account[] accounts = AccountManager.get(context).getAccounts();
		for (Account account : accounts) {
		    if (emailPattern.matcher(account.name).matches()) {
		    	possibleEmail = account.name;
		    }
		}
		return possibleEmail;
	}
	
	public static String getUDID(Context mContext) {

		return Secure.getString(mContext.getContentResolver(),
				Secure.ANDROID_ID);
	}
}
