
package com.wtf.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

public class BitmapUtils {
    private final static String TAG = BitmapUtils.class.getName();

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Bitmap createImageWithSpecificRatio(Bitmap originalBitmap, ImageView targetImageView, float widthToHeightRatio) {

        int widthOfOriginalBitmap = originalBitmap.getWidth();
        int heightOfOriginalBitmap = originalBitmap.getHeight();

        if (widthOfOriginalBitmap >= heightOfOriginalBitmap) {
            if (widthOfOriginalBitmap / widthToHeightRatio <= heightOfOriginalBitmap) {
                Bitmap bitmap = Bitmap.createBitmap(originalBitmap, 0, 0, widthOfOriginalBitmap, (int) (widthOfOriginalBitmap / widthToHeightRatio));
                return Bitmap.createScaledBitmap(bitmap, targetImageView.getWidth(), targetImageView.getHeight(), false);
            } else {
                Bitmap bitmap = Bitmap
                        .createBitmap(originalBitmap, 0, 0, (int) (heightOfOriginalBitmap * widthToHeightRatio), heightOfOriginalBitmap);
                return Bitmap.createScaledBitmap(bitmap, targetImageView.getWidth(), targetImageView.getHeight(), false);
            }
        }

        else if (widthOfOriginalBitmap < heightOfOriginalBitmap) {
            if ((widthOfOriginalBitmap / widthToHeightRatio) < heightOfOriginalBitmap) {
                Bitmap bitmap = Bitmap.createBitmap(originalBitmap, 0, 0, widthOfOriginalBitmap, (int) (widthOfOriginalBitmap / widthToHeightRatio));
                return Bitmap.createScaledBitmap(bitmap, targetImageView.getWidth(), targetImageView.getHeight(), false);
            } else {
                Bitmap bitmap = Bitmap
                        .createBitmap(originalBitmap, 0, 0, heightOfOriginalBitmap, (int) (heightOfOriginalBitmap / widthToHeightRatio));
                return Bitmap.createScaledBitmap(bitmap, targetImageView.getWidth(), targetImageView.getHeight(), false);
            }
        }

        return originalBitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static Bitmap createScaledBitmapFromUri(Context context, Uri uri, int reqWidth, int reqHeight) {
        try {

            // calculating image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            InputStream inputStramForOptions = context.getContentResolver().openInputStream(uri);
            BitmapFactory.decodeStream(inputStramForOptions, null, options);

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);
            inputStramForOptions.close();

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            InputStream stream = context.getContentResolver().openInputStream(uri);

            Bitmap bmp = BitmapFactory.decodeStream(stream, null, o2);

            stream.close();

            return bmp;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            oom.printStackTrace();
        }

        return null;
    }

    public static Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        try {
            int sourceWidth = source.getWidth();
            int sourceHeight = source.getHeight();

            // Compute the scaling factors to fit the new height and width,
            // respectively.
            // To cover the final image, the final scaling will be the bigger
            // of these two.
            float xScale = (float) newWidth / sourceWidth;
            float yScale = (float) newHeight / sourceHeight;
            float scale = Math.max(xScale, yScale);

            // Now get the size of the source bitmap when scaled
            float scaledWidth = scale * sourceWidth;
            float scaledHeight = scale * sourceHeight;

            // Let's find out the upper left coordinates if the scaled bitmap
            // should be centered in the new size give by the parameters
            float left = (newWidth - scaledWidth) / 2;
            float top = (newHeight - scaledHeight) / 2;

            // The target rectangle for the new, scaled version of the source
            // bitmap
            // will now
            // be
            RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

            // Finally, we create a new bitmap of the specified size and draw
            // our
            // new,
            // scaled bitmap onto it.
            Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
            Canvas canvas = new Canvas(dest);
            canvas.drawBitmap(source, null, targetRect, null);

            return dest;
        } catch (Exception e) {
            Log.e("dsd", "neuea aaa " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getSampleBitmapFromResource(Context context, int resourceId, int reqWidth, int reqHeight) {
        try {

            InputStream in = context.getResources().openRawResource(resourceId);

            // calculating image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);

            in.reset();

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            Bitmap bmp = BitmapFactory.decodeStream(in, null, o2);
            Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight());

            return correctBmp;
        } catch (OutOfMemoryError oom) {
            oom.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return null;
    }

    public static String createScaledImageFile(String bitmapFilePath, int reqWidth, int reqHeight) {
        try {
            File f = new File(bitmapFilePath);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);

            // calculating image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, options);

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);

            // if image already in required maximum size -> do nothing... return
            // original image
            if (scale <= 1) {
                return bitmapFilePath;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

            String newFileName = bitmapFilePath.substring(0, bitmapFilePath.lastIndexOf(".")) + "_scaled.png";

            FileOutputStream out = new FileOutputStream(newFileName);
            correctBmp.compress(Bitmap.CompressFormat.PNG, 75, out);

            return newFileName;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            oom.printStackTrace();
        }

        return null;
    }

    public static String createScaledJpgFile(Context context, String bitmapFilePath, int reqWidth, int reqHeight) {
        try {
            File f = new File(bitmapFilePath);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);

            // calculating image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, options);

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);

            // if image already in required maximum size -> do nothing... return
            // original image
            if (scale <= 1) {
                return bitmapFilePath;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

            String newFileName = context.getFilesDir().getAbsolutePath() + "/" + String.valueOf(System.currentTimeMillis()) + ".jpg";

            FileOutputStream out = new FileOutputStream(newFileName);
            correctBmp.compress(Bitmap.CompressFormat.JPEG, 75, out);

            return newFileName;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            oom.printStackTrace();
        }

        return null;
    }

    public static Bitmap createScaledBitmapFromStream(Context context, Uri uri, int reqWidth, int reqHeight) {
        try {

            // calculating image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            InputStream inputStramForOptions = context.getContentResolver().openInputStream(uri);
            BitmapFactory.decodeStream(inputStramForOptions, null, options);

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);
            inputStramForOptions.close();

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            InputStream stream = context.getContentResolver().openInputStream(uri);

            Bitmap bmp = BitmapFactory.decodeStream(stream, null, o2);

            stream.close();

            return bmp;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            oom.printStackTrace();
        }

        return null;
    }

    public static String createScaledJpgFileFromUri(Context context, Uri uri, int reqWidth, int reqHeight) {
        try {

            // calculating image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            InputStream inputStramForOptions = context.getContentResolver().openInputStream(uri);
            BitmapFactory.decodeStream(inputStramForOptions, null, options);

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);
            inputStramForOptions.close();

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            InputStream stream = context.getContentResolver().openInputStream(uri);

            Bitmap bmp = BitmapFactory.decodeStream(stream, null, o2);

            String newFileName = context.getFilesDir().getAbsolutePath() + "/" + String.valueOf(System.currentTimeMillis()) + ".jpg";

            FileOutputStream out = new FileOutputStream(newFileName);
            bmp.compress(Bitmap.CompressFormat.JPEG, 75, out);

            stream.close();

            return newFileName;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            oom.printStackTrace();
        }

        return null;
    }

    public static String createScaledJpgFile(String bitmapFilePath, String newBitmapFilePath, int reqWidth, int reqHeight) {
        try {
            File f = new File(bitmapFilePath);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);

            // calculating image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, options);

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);

            // if image already in required maximum size -> do nothing... return
            // original image
            if (scale <= 1) {
                return bitmapFilePath;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

            // String newFileName = bitmapFilePath.substring(0,
            // bitmapFilePath.lastIndexOf(".")) + "_scaled.jpg";
            String newFileName = newBitmapFilePath;
            FileOutputStream out = new FileOutputStream(newFileName);
            correctBmp.compress(Bitmap.CompressFormat.JPEG, 75, out);

            return newFileName;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            oom.printStackTrace();
        }

        return null;
    }

    public static Bitmap getSampleBitmapFromFile(String bitmapFilePath, int reqWidth, int reqHeight) {
        try {
            File f = new File(bitmapFilePath);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);

            // calculating image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, options);

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

            return correctBmp;
        } catch (IOException e) {
            Log.e(TAG, "cant butmaputils.getSampleBitmapFromFile failed with IO exception: ", e);
            if (e != null)
                e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            Log.e(TAG, "cant butmaputils.getSampleBitmapFromFile failed with OOM error: ");
            if (oom != null)
                oom.printStackTrace();
        } catch (Exception e) {
            Log.e(TAG, "cant butmaputils.getSampleBitmapFromFile failed with exception: ", e);
        }
        Log.e(TAG, "butmaputils.getSampleBitmapFromFilereturn null for file: " + bitmapFilePath);
        return null;
    }

    public static int[] getDimensionsForBitmap(int width, int height, int targetWidth) {
        int[] retVal = {
                targetWidth, height
        };

        if (width != retVal[0]) {
            float xRatio = ((float) retVal[0]) / width;
            retVal[1] = (int) (retVal[1] * xRatio);
        }

        return retVal;
    }

    public static String toBase64(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); // bm is the
                                                               // bitmap object
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.DEFAULT);

    }

    public static Bitmap overlay(Bitmap bottomImage, Bitmap topImage) {
        Bitmap bmOverlay = Bitmap.createBitmap(bottomImage.getWidth(), bottomImage.getHeight(), bottomImage.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bottomImage, new Matrix(), null);
        canvas.drawBitmap(topImage, new Matrix(), null);
        return bmOverlay;
    }
}
