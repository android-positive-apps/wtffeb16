/**
 * 
 */
package com.wtf.storage;

/**
 * @author natiapplications
 *
 */
public interface StorageListener {
	
	public void onStorageDataReceived (Object data);

}
