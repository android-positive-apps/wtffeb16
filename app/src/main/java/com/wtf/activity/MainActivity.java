package com.wtf.activity;

//import com.crashlytics.android.Crashlytics;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Point;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.facebook.AccessToken;

//import com.facebook.Session;
//import com.facebook.SessionState;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.wtf.dataBase.Preferences;
import com.wtf.fragments.LoginFragment;
import com.wtf.fragments.MainMenuFragment;
import com.wtf.fragments.SearchFragment;
import com.wtf.fragments.SettingFragment;
import com.wtf.location.LocationUtilis;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.UpdateGcm;
import com.wtf.utils.SimpleFacebookUtils;

public class MainActivity extends FragmentActivity implements
		android.view.View.OnClickListener {

	
	public static final String EXTERA_BUSINESS = "ExtraBusiness";
	public static final String EXTERA_POSITION = "ExtraPsition";
	
	private static final float LOCATION_REFRESH_DISTANCE = 20;
	public static Button mGps;
	public static Button mSettings;
	public static Button mSearch;
	public static FragmentManager fragmentManager;
	private LocationManager mLocationManager;

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private GoogleCloudMessaging gcm;
	private Context context;
	public static MainActivity mainInstance;

	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	String SENDER_ID = "221047633308";
	private String regid;

	private static final String TAG = "MainActivity";

	//public static List<Business> mBusinesses;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Crashlytics.start(this);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		context = getApplicationContext();
		mainInstance = this;
		BMMApplication.mSimpleFacebook = SimpleFacebook.getInstance(this);
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = Preferences.getInstans(context).getGCM();
			sendRegistrationIdToBackend();

			if (regid.isEmpty()) {
				registerInBackground();
			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}


		showTermsOfUse();

		fragmentManager = getSupportFragmentManager();

		mGps = (Button) findViewById(R.id.main_location_btn);
		mSearch = (Button) findViewById(R.id.main_search_btn);
		mSettings = (Button) findViewById(R.id.main_setting_btn);

		mGps.setOnClickListener(this);
		mSearch.setOnClickListener(this);
		mSettings.setOnClickListener(this);
		
		showTopButtons();
		
		
		
		
		if (savedInstanceState == null) {

			if (Preferences.getInstans(this).getUserId().equals("0")
					|| Preferences.getInstans(this).getFacebookId().equals("0")) {
				// new user
				getSupportFragmentManager()
						.beginTransaction()
						.add(R.id.container, new LoginFragment(),
								MainMenuFragment.TAG).commitAllowingStateLoss();
			} else {

				if (AccessToken.getCurrentAccessToken()  != null){
					SimpleFacebookUtils.setUserFacebookFriends();
					getSupportFragmentManager()
							.beginTransaction()
							.add(R.id.container, new MainMenuFragment(),
									MainMenuFragment.TAG).commitAllowingStateLoss();
				}else{
					SimpleFacebookUtils.facebookLogin(this, new CallBack<UserProfile>() {
						@Override
						public void callBack(UserProfile object) {
							getSupportFragmentManager()
									.beginTransaction()
									.add(R.id.container, new MainMenuFragment(),
											MainMenuFragment.TAG).commitAllowingStateLoss();
						}
					});
				}

			}
		}
		saveScreenDimention();
		
		printKeyHash();
	}
	
	
	
	
	
	
	private void printKeyHash(){
	    // Add code to print out the key hash
	    try {
	        PackageInfo info = getPackageManager().getPackageInfo(
	                "com.UbiKT.activity.mx",
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {
	        Log.d("KeyHash:", e.toString());
	    } catch (NoSuchAlgorithmException e) {
	        Log.d("KeyHash:", e.toString());
	    }
	}
	
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.e("distroyLog", "onDestroy");
		BMMApplication.setCurrentUserProfile(null);
		//LocationTransmitterThread.getInstance(this).cancel();
		if (!Preferences.getInstans(this).isShowLocation()){
			LocationUtilis.stopLocationService(this);
		}
	}

	/***
	 * show a dialog if it is the first time the user uses the APP
	 * 
	 */

	private void showTermsOfUse() {

		if (Preferences.getInstans(this).isFirstTimeUse()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setCancelable(false);

			builder.setMessage(getString(R.string.first_time_terms));
			builder.setPositiveButton(getString(R.string.i_except),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							Preferences.getInstans(MainActivity.this)
									.setFirstTimeUse(false);
						}
					});
			builder.setNegativeButton(getString(R.string.i_dont_except),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
			builder.show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);*/
		BMMApplication.mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * save the dimension width & height in preferences of the screen for later
	 * to adjust some views
	 */

	private void saveScreenDimention() {

		WindowManager w = getWindowManager();
		Display d = w.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		d.getMetrics(metrics);
		// since SDK_INT = 1;
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
		try {
		    widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
		    heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
		} catch (Exception ignored) {
		}
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 17)
		try {
		    Point realSize = new Point();
		    Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
		    widthPixels = realSize.x;
		    heightPixels = realSize.y;
		} catch (Exception ignored) {
		}

		Preferences.getInstans(this).setScreenHeight(heightPixels);
		Preferences.getInstans(this).setScreenWidth(widthPixels);
	}

	@Override
	public void onBackPressed() {

		FragmentManager fm = getSupportFragmentManager();

		if (fm.getBackStackEntryCount() == 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getString(R.string.exit_message))
					.setPositiveButton(getString(R.string.yes), new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					}).setNegativeButton(getString(R.string.no), new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					}).show();
			
		}

		fm.popBackStack();
	}

	
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.main_location_btn:

			Intent callGPSSettingIntent = new Intent(
					android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(callGPSSettingIntent);
			break;
		case R.id.main_search_btn:
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.container, new SearchFragment(),
							SearchFragment.TAG).addToBackStack(null).commit();
			break;
		case R.id.main_setting_btn:
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.container, new SettingFragment(),
							SettingFragment.TAG).addToBackStack(null).commit();
			break;

		}
	}

	/***
	 * hides the top 3 buttons from the Main Activity
	 */

	public static void hideTopButtons() {
		MainActivity.mGps.setVisibility(View.INVISIBLE);
		MainActivity.mSearch.setVisibility(View.INVISIBLE);
		MainActivity.mSettings.setVisibility(View.INVISIBLE);
	}

	/***
	 * shows the top 3 buttons from the Main Activity
	 */

	public static void showTopButtons() {
		MainActivity.mGps.setVisibility(View.VISIBLE);
		MainActivity.mSearch.setVisibility(View.VISIBLE);
		MainActivity.mSettings.setVisibility(View.VISIBLE);
	}
	
	public static void showTopButtonsLocationAndSettings() {
		MainActivity.mGps.setVisibility(View.VISIBLE);
		MainActivity.mSearch.setVisibility(View.GONE);
		MainActivity.mSettings.setVisibility(View.VISIBLE);
	}
	
	public static void showTopButtonsLocationAndSherch() {
		MainActivity.mGps.setVisibility(View.VISIBLE);
		MainActivity.mSearch.setVisibility(View.VISIBLE);
		MainActivity.mSettings.setVisibility(View.GONE);
	}
	
	public static void showTopButtonsLocation() {
		MainActivity.mGps.setVisibility(View.VISIBLE);
		MainActivity.mSearch.setVisibility(View.GONE);
		MainActivity.mSettings.setVisibility(View.GONE);
	}

	public static void cleanAllFragments() {

		for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
			fragmentManager.popBackStack();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		checkPlayServices();
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	private void registerInBackground() {

		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;

					// Persist the regID - no need to register again.
					Preferences.getInstans(context).setGCM(regid);

					sendRegistrationIdToBackend();

				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}
		}.execute();
	}

	private void sendRegistrationIdToBackend() {
		new UpdateGcm(context).start();
	}



}
