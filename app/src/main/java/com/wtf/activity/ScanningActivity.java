/**
 * 
 */
package com.wtf.activity;

import java.io.File;
import java.io.IOException;
import java.sql.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beyondar.android.fragment.BeyondarFragmentSupport;
import com.beyondar.android.util.ImageUtils;
import com.beyondar.android.util.location.BeyondarLocationManager;
import com.beyondar.android.util.math.geom.Point2;
import com.beyondar.android.view.BeyondarViewAdapter;
import com.beyondar.android.view.OnClickBeyondarObjectListener;
import com.beyondar.android.world.BeyondarObject;
import com.beyondar.android.world.BeyondarObjectList;
import com.beyondar.android.world.GeoObject;
import com.beyondar.android.world.World;

import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;
import com.wtf.dataBase.Preferences;
import com.wtf.dataBase.UserFriendsManager.FriendsCallbac;
import com.wtf.fragments.BusinessFragment;
import com.wtf.fragments.ProfileFragment;
import com.wtf.fragments.SearchFragment;
import com.wtf.objects.Business;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.FriandLocation;
import com.wtf.objects.Place;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.CreateUpdateUserTask;
import com.wtf.serverTask.GetAppuserFriandsLocation;
import com.wtf.serverTask.GetBezeqTask;
import com.wtf.serverTask.GetBusinnesById;
import com.wtf.utils.GeneralUtills;
import com.wtf.utils.SimpleFacebookUtils;

/**
 * @author natiapplications
 *
 */
public class ScanningActivity extends FragmentActivity implements OnClickListener {
	
	private static final String TMP_IMAGE_PREFIX = "scanviewimage_";
	public static AtomicInteger sNextGeneratedId = new AtomicInteger(1);
	
	
	public static final String FOOD = "food";
	public static final String FATION = "fation";
	public static final String SPORT = "sport";
	public static final String NIGTH = "nigte";
	public static final String HOBY = "hoby";
	public static final String FRIENDS = "friends";
	
	
	private BeyondarFragmentSupport mBeyondarFragment;
	private World mWorld;
	private boolean isLoggedIn;
	
	private Button syncBtn,deleteBtn,toBuisnessPageBtn;
	private Button foodBtn,fationBtn,sportBtn,nigteBtn,hobyBtn,friendsBtn;
	private TextView nameTv,addressTv,distanceTv;
	private ArrayList<Button> buttons = new ArrayList<Button>();
	
	
	private String currentBuisnessID;
	private String currentBuisnessExternalID;
	private String currentFBID;

	ArrayList<ArrayList<Business>> allCategorys = new ArrayList<ArrayList<Business>>();
	
	ArrayList<Business> fation = new ArrayList<Business>();
	ArrayList<Business> sport = new ArrayList<Business>();
	ArrayList<Business> food = new ArrayList<Business>();
	ArrayList<Business> nigte = new ArrayList<Business>();
	ArrayList<Business> hoby = new ArrayList<Business>();
	
	ArrayList<FacebookFriand> friends = new ArrayList<FacebookFriand>();
	private List<BeyondarObject> showViewOn;

	private int currentCategoryType;
	private ImageView mapBtn;
	
	private ArrayList<FacebookFriand> userFriends;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.scannig_activity);
		mBeyondarFragment = (BeyondarFragmentSupport) getSupportFragmentManager()
	    		.findFragmentById(R.id.beyondarFragment);
		mBeyondarFragment.showFPS(true);
		mBeyondarFragment.setOnClickBeyondarObjectListener(new MarkerClickedListener());
		CustomBeyondarViewAdapter customBeyondarViewAdapter = new CustomBeyondarViewAdapter(this);
		mBeyondarFragment.setBeyondarViewAdapter(customBeyondarViewAdapter);
		nameTv = (TextView)findViewById(R.id.name_tv);
		addressTv = (TextView)findViewById(R.id.address_tv);
		distanceTv = (TextView)findViewById(R.id.distance_tv);
		showViewOn = Collections.synchronizedList(new ArrayList<BeyondarObject>());

		
		initButtons();
		allCategorys.add(food);
		allCategorys.add(fation);
		allCategorys.add(sport);
		allCategorys.add(nigte);
		allCategorys.add(hoby);
	}
	
	
   
	
	private void initButtons () {
		toBuisnessPageBtn = (Button)findViewById(R.id.to_page_btn);
		syncBtn = (Button)findViewById(R.id.sync_scan);
		deleteBtn = (Button)findViewById(R.id.delet_btn);
		foodBtn = (Button)findViewById(R.id.food_btn);
		fationBtn = (Button)findViewById(R.id.fation_btn);
		sportBtn = (Button)findViewById(R.id.sport_btn);
		nigteBtn = (Button)findViewById(R.id.nigte_btn);
		hobyBtn = (Button)findViewById(R.id.hoby_btn);
		friendsBtn = (Button)findViewById(R.id.friends_btn);
		mapBtn = (ImageView)findViewById(R.id.map_btn);
		
		buttons.add(foodBtn);
		buttons.add(fationBtn);
		buttons.add(sportBtn);
		buttons.add(nigteBtn);
		buttons.add(hobyBtn);
		buttons.add(friendsBtn);
		
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).setOnClickListener(this);
		}
		
		friendsBtn.performClick();
		
		
		mapBtn.setOnClickListener(this);
		syncBtn.setOnClickListener(this);
		deleteBtn.setOnClickListener(this);
		toBuisnessPageBtn.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		BeyondarLocationManager.enable();
	}

	@Override
	protected void onPause() {
		super.onPause();
		BeyondarLocationManager.disable();
	}

	
	@Override
	public void onClick(View v) {
		    showViewOn.removeAll(showViewOn);
			switch (v.getId()) {
			case R.id.food_btn:
				currentCategoryType = SearchFragment.CATEGORY_FODD;
				toggleCategorys(v, food, GetBezeqTask.TypeFood);
				break;
			case R.id.fation_btn:
				currentCategoryType = SearchFragment.CATEGORY_FATION;
				toggleCategorys(v, fation, GetBezeqTask.TypeFation);
				break;
			case R.id.sport_btn:
				currentCategoryType = SearchFragment.CATEGORY_SPORT;
				toggleCategorys(v, sport, GetBezeqTask.TypeSport);
				break;
			case R.id.nigte_btn:
				currentCategoryType = SearchFragment.CATEGORY_NIGHT;
				toggleCategorys(v, nigte, GetBezeqTask.TypeNight);
				break;
			case R.id.hoby_btn:
				currentCategoryType = SearchFragment.CATEGORY_TIME;
				toggleCategorys(v, hoby, GetBezeqTask.TypeFreeTime);
				break;
			case R.id.friends_btn:
				if (Preferences.getInstans(this).getFacebookId().equals("0")&&! isLoggedIn){
					showFacebookLoginDialog();
				}else{
					toggleFriandsButton (v);
				}
				break;
			case R.id.sync_scan:
				clearAllMarkers();
				addMarkersForAllSelectedCategorys(friends);
				break;
			case R.id.map_btn:
				finish();
				//startActivity(new Intent(this,MapActivity.class));
				break;
			case R.id.delet_btn:
				clearAllMarkers();
				removeAllData();
				currentBuisnessExternalID ="";
				currentBuisnessID ="";
				break;
			case R.id.to_page_btn:
			case R.id.frame:
				if (currentFBID != null && !currentFBID.isEmpty() && !currentFBID.equals("*")){
					launchFacebook(currentFBID);
				}else{
					goToBusinessPage();
				}
				break;
			}
		
		
		
	}
	
	

	private void toggleCategorys(View v, ArrayList<Business> collection,
			int type) {

		if (v.isSelected()) {
			v.setSelected(false);
			removeBusinesess(collection);
		} else {
			v.setSelected(true);
			getData(type);
		}
	}
	
	private void toggleFriandsButton(View v) {

		if (v.isSelected()) {
			v.setSelected(false);
			clearAllMarkers();
			friends.clear();
			addMarkersForAllSelectedCategorys(null);
		} else {
			v.setSelected(true);
			getUserFriendsLocation();
		}

	}
	
	private void getData(final int type) {
		GetBezeqTask bezeqTask = new GetBezeqTask(this,
				new CallBack<JSONObject>() {
					@Override
					public void callBack(JSONObject object) {
						try {
							JSONArray array = object.getJSONArray("data");
							Business[] businesses = new Business[array.length()];
							for (int i = 0; i < businesses.length; i++) {
								businesses[i] = new Business(array
										.getJSONObject(i));
								businesses[i].setCategoryType(currentCategoryType);
							}
							if (businesses != null) {
								addBusinessesToAproprateCollection(type, businesses);
								addMarkersForAllSelectedCategorys(friends);
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}, type);
		bezeqTask.execute();
	}
	
	private void getUserFriendsLocation () {
		new GetAppuserFriandsLocation(this, new CallBack<ArrayList<FriandLocation>>() {
			@Override
			public void callBack(final ArrayList<FriandLocation> object) {
				if (object.size() == 0){
					Toast.makeText(ScanningActivity.this, getString(R.string.friends_location_not_found), Toast.LENGTH_SHORT).show();
					return;
				}
				//ArrayList<FacebookFriand> friends = BMMApplication.getUserFacebookFriends();
			
				BMMApplication.getUserFacebookFriends(new FriendsCallbac() {
					@Override
					public void onFriendsArrived(ArrayList<FacebookFriand> friends) {
						userFriends = friends;
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								for (int i = 0; i < object.size(); i++) {
									for (int j = 0; j < userFriends.size(); j++) {
										if (userFriends.get(j).getFBID().equalsIgnoreCase(object.get(i).getFBID())){
											userFriends.get(j).setLat(object.get(i).getLat());
											userFriends.get(j).setLon(object.get(i).getLon());
										}
									}
								}
								addMarkersForUserFacebookFriands ();
							}
						});
					}
				});
			}
		}).execute();
	}
	
    
	
	private void clearAllMarkers() {
		try {
			showViewOn.removeAll(showViewOn);
			mWorld.clearWorld();
		} catch (Exception e) {}
	}
    
	private void removeAllData() {
		for (int i = 0; i < allCategorys.size(); i++) {
			allCategorys.get(i).clear();
		}
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).setSelected(false);
		}
		friends.clear();
	}
	
    private void addMarkersForAllSelectedCategorys (ArrayList<FacebookFriand> friends) {
		try {
			
			clearAllMarkers();
			cleanTempFolder();
			mWorld = CustomWorldHelper.generateObjects(this,allCategorys,friends);
			mBeyondarFragment.setWorld(mWorld);
			showViewOn.removeAll(showViewOn);
			
			
			mBeyondarFragment.setPullCloserDistance(30);
			mBeyondarFragment.setPushAwayDistance(5);
			mBeyondarFragment.setMaxDistanceToRender(Preferences.getInstans(this).getRadius() * 1000);
			mBeyondarFragment.setDistanceFactor(2);
			replaceImagesByStaticViews(null,mWorld);
		} catch (Exception e) {
			Log.e("ext", "ex - " + e.getMessage());
			View layout = getLayoutInflater().inflate(R.layout.toast_layout, null);
			TextView textResult = (TextView)layout.findViewById(R.id.toast_message);
			textResult.setText(R.string.no_compess_sensor_message);

			Toast toast = new Toast(this);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			finish();
		}
	}
	
    
	private void replaceImagesByStaticViews(BeyondarObject selected,World world) {
		cleanTempFolder();
		final String path = getTmpPath();
		Log.e("scanninglog" , "size = " + world.getBeyondarObjectLists().size());
		for (BeyondarObjectList beyondarList : world.getBeyondarObjectLists()) {
			for (BeyondarObject beyondarObject : beyondarList) {
				View view = getLayoutInflater().inflate(R.layout.marker_layout, null);
				if (selected != null){
					
					if (selected.getName().equals(beyondarObject.getName())){
						Log.i("scanninglogequeal","selected name = " + selected.getName() );
						Log.d("scanninglogequeal","current name = " +beyondarObject.getName());
						view = getLayoutInflater().inflate(R.layout.marker_layout_selected, null);
					}else{
						Log.e("scanninglogequeal","selected name = " + selected.getName() );
						Log.e("scanninglogequeal","current name = " +beyondarObject.getName());
						view = getLayoutInflater().inflate(R.layout.marker_layout, null);
					}
				}
				
				
				CircleImageView businessIcon = (CircleImageView) view.findViewById(R.id.busi_icon);
				setBusinessIcon(beyondarObject,
						beyondarObject.getName().split(Pattern.quote("||"))[8],
						view,
						businessIcon, path);
			}
		}
	}
	
	
	
	private void setBusinessIcon(final BeyondarObject object,final String url,
			final View view,final ImageView image, final String path) {
		try {
			Picasso.with(ScanningActivity.this).load(url).resize(80, 80)
					.into(image, new Callback() {
						@Override
						public void onError() {
						}

						@Override
						public void onSuccess() {
							Picasso.with(ScanningActivity.this).load(url)
									.resize(80, 80).into(image);
							try {
								String imageName = TMP_IMAGE_PREFIX + object.getName().split(Pattern.quote("||"))[0]+ ".png";
								Log.e("scanninglog" , "image name = " + imageName );
								Log.d("scanninglog" , "image path = " + path + imageName);
								ImageUtils.storeView(view, path, imageName);
								object.setImageUri(path + imageName);
							} catch (IOException e) {
								e.printStackTrace();
								Log.e("scanninglog" , "ex = " + e.getMessage());
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/** Clean all the generated files */
	private void cleanTempFolder() {
		File tmpFolder = new File(getTmpPath());
		if (tmpFolder.isDirectory()) {
			String[] children = tmpFolder.list();
			for (int i = 0; i < children.length; i++) {
				if (children[i].startsWith(TMP_IMAGE_PREFIX)) {
					new File(tmpFolder, children[i]).delete();
				}
			}
		}
	}
	
	private String getTmpPath() {
		return getExternalFilesDir(null).getAbsoluteFile() + "/tmp/";
	}
	
	private void addMarkersForUserFacebookFriands() {
		ArrayList<FacebookFriand> result = userFriends;
		for (int i = 0; i < result.size(); i++) {
			if (result.get(i).getLat() > 0 && result.get(i).getLon() > 0) {
				friends.add(result.get(i));
			}
		}
		addMarkersForAllSelectedCategorys(friends);
	}
	

	private void addBusinessesToAproprateCollection(int type,
			Business[] businesses) {

		switch (type) {
		case GetBezeqTask.TypeFood:
			addBusinesses(businesses, food,R.drawable.food_marker,R.drawable.food_marker_selected);
			break;
		case GetBezeqTask.TypeFation:
			addBusinesses(businesses, fation,R.drawable.fation_marker,R.drawable.fation_marker_selected);
			break;
		case GetBezeqTask.TypeFreeTime:
			addBusinesses(businesses, hoby,R.drawable.hoby_marker,R.drawable.hoby_marker_selected);
			break;
		case GetBezeqTask.TypeNight:
			addBusinesses(businesses, nigte,R.drawable.nigte_marker,R.drawable.nigte_marker_selected);
			break;
		case GetBezeqTask.TypeSport:
			addBusinesses(businesses, sport,R.drawable.sport_marker,R.drawable.sport_marker_selected);
			break;
		}
	}
	
	
	
	
	private void addBusinesses (Business[] businesses,ArrayList<Business> collection,int markerResource
			,int markerResourceSelected){
		collection.clear();
		try {
			Collections.reverse(Arrays.asList(businesses));
		} catch (Exception e) {}
		
		for (int i = 0; i < businesses.length; i++) {
			businesses[i].setMarkerRecourse(markerResource);
			businesses[i].setMarkerRecourseSelected(markerResourceSelected);
			collection.add(businesses[i]);
			if( i == 10){
				break;
			}
		}
		
	}
	
	private void removeBusinesess (ArrayList<Business> collection) {
		
		collection.clear();
		addMarkersForAllSelectedCategorys(friends);
		
	}
	
	
	public class MarkerClickedListener implements OnClickBeyondarObjectListener {

		@Override
		public void onClickBeyondarObject(
				ArrayList<BeyondarObject> beyondarObjects) {
			try {
				BeyondarObject pressed = beyondarObjects.get(0);
				
				showViewOn.removeAll(showViewOn);
				
				showViewOn.add(pressed);
				
				String[] detailse = pressed.getName().split(Pattern.quote("||"));
				Log.e("scanning", pressed.getName() + detailse.length);
				String name = detailse[0];
				
				String address = "אין מידע";
				if (detailse[1]!= null){
					address = detailse[1];
				}
				double lat = Double.parseDouble(detailse[2]);
				double lon = Double.parseDouble(detailse[3]);
				
				currentBuisnessID = detailse[5];
				currentBuisnessExternalID = detailse[6];
				currentFBID = detailse[9];
				currentCategoryType = Integer.parseInt(detailse[10]);
				
				if (currentBuisnessID.equals("*")){
					toBuisnessPageBtn.setVisibility(View.VISIBLE);
					toBuisnessPageBtn.setBackgroundResource(R.drawable.facebook_icon);
					toBuisnessPageBtn.setText("");
				}else{
					toBuisnessPageBtn.setVisibility(View.VISIBLE);
					toBuisnessPageBtn.setBackgroundResource(R.drawable.check_in_selector);
					toBuisnessPageBtn.setText(getString(R.string.to_businnes_page));
					currentFBID = "";
				}
				
				/*int iconSelected = Integer.parseInt(detailse[7]);
				for (BeyondarObjectList beyondarList : mWorld.getBeyondarObjectLists()) {
					for (BeyondarObject beyondarObject : beyondarList) {
						String[] d = beyondarObject.getName().split(Pattern.quote("||"));
						int icon = Integer.parseInt(d[4]);
						beyondarObject.setImageResource(icon);
					}
				}
				pressed.setImageResource(iconSelected);*/
				
				String distance = findDistanceFromUser(lat, lon);
				
				nameTv.setText(name);
				addressTv.setText(address);
				distanceTv.setText(getString(R.string.distanc) + " " + distance + " " + getString(R.string.km));
			} catch (Exception e) {
				//Toast.makeText(ScanningActivity.this, "can not parsing the marker details",
				//Toast.LENGTH_SHORT).show();
			}
		}
		
	
		
		private String findDistanceFromUser(double targetLat, double targetLang)
		{
			double userLat = Double.parseDouble(Preferences.getInstans(ScanningActivity.this).getLatitude());
			double userLon = Double.parseDouble(Preferences.getInstans(ScanningActivity.this).getLongitude());
			
			double earthRadius = 3958.75;
		    double dLat = Math.toRadians(targetLat-userLat);
		    double dLng = Math.toRadians(targetLang-userLon);
		    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		               Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(targetLat)) *
		               Math.sin(dLng/2) * Math.sin(dLng/2);
		    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		    double dist = earthRadius * c;

		    int meterConversion = 1609;
		    double mDistance = (float) (dist * meterConversion);
			mDistance = mDistance/1000;
			String result  = new DecimalFormat("##.##").format(mDistance);
			return result;
		}
		
	}
	
	private void goToBusinessPage () {
		
		if (currentBuisnessID == null || currentBuisnessExternalID == null || 
				currentBuisnessID.equals("")||currentBuisnessExternalID.equals("")|| 
				currentBuisnessID.equals("*")||currentBuisnessExternalID.equals("*")
				){
			return;
		}
		
		GetBusinnesById getBusinnesById = new GetBusinnesById(this,
				currentBuisnessID, currentBuisnessExternalID,
				new CallBack<Business>() {

					@Override
					public void callBack(Business object) {
						if (object != null){
							object.setCategoryType(currentCategoryType);
							Intent i = new Intent(ScanningActivity.this,ContainerActivity.class);
							i.putExtra(ContainerActivity.EXTRA_BUISNESS, object);
							startActivity(i);
						}
					}
				});
		getBusinnesById.execute();
	}
	
	public final void launchFacebook(String id) {
        final String urlFb = "fb://profile/"+id;
        Intent intent = new Intent(Intent.ACTION_VIEW);
       
        final String urlBrowser = "https://www.facebook.com/"+id;
        intent.setData(Uri.parse(urlBrowser));
        startActivity(intent);
    }
	
	private void showFacebookLoginDialog (){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Facebook Login");
		builder.setMessage(getString(R.string.no_facebook_message_dialog));
		builder.setNegativeButton(getString(R.string.cancel), null);
		builder.setPositiveButton(getString(R.string.login), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				isLoggedIn = true;
				loginWithFacebook();
				
			}
		});
		Dialog dialog = builder.create();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}
	
	
	
	
	private void loginWithFacebook() {

		SimpleFacebookUtils.facebookLogin(this, new CallBack<UserProfile>() {
			@Override
			public void callBack(UserProfile object) {
				if (object != null){
					BMMApplication.setCurrentUserProfile(object);
					Preferences.getInstans(ScanningActivity.this).setUserId(
							object.getId());
					finish();
					startActivity(getIntent());
				}
				
			}
		});
	}
	
	private class CustomBeyondarViewAdapter extends BeyondarViewAdapter {

		LayoutInflater inflater;

		public CustomBeyondarViewAdapter(Context context) {
			super(context);
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(BeyondarObject beyondarObject, View recycledView, ViewGroup parent) {
			if (!showViewOn.contains(beyondarObject)) {
				return null;
			}
			if (recycledView == null) {
				recycledView = inflater.inflate(R.layout.marker_selected_view, null);
			}

			TextView textView = (TextView) recycledView.findViewById(R.id.name);
			textView.setText(beyondarObject.getName().split(Pattern.quote("||"))[0]);
			Point2 position = new Point2();
			position.y = beyondarObject.getScreenPositionTopLeft().y - 120;
			position.x = beyondarObject.getScreenPositionTopLeft().x - 110;

			// Once the view is ready we specify the position
			setPosition(position);
			FrameLayout frame = (FrameLayout)recycledView.findViewById(R.id.frame);
			frame.setOnClickListener(ScanningActivity.this);

			return recycledView;
		}

	}
	
	
	
	
	
	
	
	

}
