package com.wtf.activity;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class SMSActivity extends Activity {

	public static final String EXTRA_MESSAGE = "ExtraMessage";
	
	TextView messageTv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sms);
		messageTv = (TextView)findViewById(R.id.sms_message_txt);
		if (getIntent().getExtras() != null){
			String message = getIntent().getExtras().getString(EXTRA_MESSAGE);
			messageTv.setText(message);
		}
	}

	
}
