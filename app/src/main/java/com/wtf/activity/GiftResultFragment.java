package com.wtf.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.meg7.widget.SvgImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wtf.objects.Coupon;
import com.wtf.objects.Gift;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.SetCouponUsed;



public class GiftResultFragment extends Fragment {

	private Coupon mCoupon;
	private Gift mGift;
	private int type;
	
	private TextView businessName;
	private SvgImageView image;
	private TextView title;
	private TextView desc;
	private TextView shortLetters;
	
	private String dialogTitle;
	private String idToSetUsed;
	private String resultMessage;
	private boolean isSuccess;
    public GiftResultFragment () {}
    
	public GiftResultFragment(Coupon coupon) {
		mCoupon = coupon;
	}
	
	public GiftResultFragment(Gift gift) {
		mGift = gift;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater
				.inflate(R.layout.gifts_result_fragment, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		MainActivity.showTopButtons();
		title = ((TextView) view.findViewById(R.id.coupon_res_title));
		desc = ((TextView) view.findViewById(R.id.coupon_res_description));
		shortLetters = ((TextView) view.findViewById(R.id.gift_res_left));
		businessName = (TextView)view.findViewById(R.id.business_name);
		image = (SvgImageView) view.findViewById(R.id.coupon_res_bg_img);
		
		if (mCoupon != null){
			dialogTitle = getString(R.string.set_coupon_code);
			idToSetUsed = mCoupon.getId();
			type = SetCouponUsed.TYPE_COUPON;
			resultMessage = getString(R.string.coupon_used);
			attachDataUsingCoupon();
		}
		if (mGift != null){
			type = SetCouponUsed.TYPE_GIFT;
			dialogTitle = getString(R.string.set_gift_code);
			idToSetUsed = mGift.getId();
			resultMessage = getString(R.string.gift_used);
			attachDataUsingGift();
		}
		
		((Button) view.findViewById(R.id.coupon_res_btn))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showPasswordDialog(dialogTitle,type);
			}

		});
	}
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MainActivity.hideTopButtons();
	}
	
	private void attachDataUsingCoupon (){
		title.setText(mCoupon.getTitle());
		desc.setText(mCoupon.getDescription());
		shortLetters.setText(mCoupon.getSmallLetters());
		businessName.setText(mCoupon.getBusinessName());
		setImage(mCoupon.getImgUrl());
	}
	
	private void attachDataUsingGift () {
		title.setText(mGift.getTitle());
		desc.setText(mGift.getDescription());
		shortLetters.setText(mGift.getSmallLetters());
		businessName.setText(mGift.getBusinessName());
		setImage(mGift.getImageURl());
	}
	
	private void setImage (final String imageUrl) {
		Picasso.with(getActivity())
		   .load(imageUrl).fit() .into(image, new Callback(){
			@Override
			public void onError() {}
			@Override
			public void onSuccess() {
				// TODO Auto-generated method stub
				Picasso.with(getActivity()).load(imageUrl).fit()
				   .into(image);
			}
		   });
	}
	
	
	private void showPasswordDialog(String title,final int type){
		
		final Dialog dialog = new Dialog(getActivity());
		dialog.setTitle(title);
		dialog.setContentView(R.layout.show_password_dialog);
		final EditText passwordET = (EditText) dialog.findViewById(R.id.dialog_password_et);
		Button send = (Button) dialog.findViewById(R.id.dialog_send_btn);
		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setCouponUsed(idToSetUsed,passwordET.getText().toString(),type);
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	private void showResultDialog (String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(message);
		builder.setCancelable(true);
		builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				if (isSuccess){
					MainActivity.cleanAllFragments();
				}
			}
		});
		builder.create().show();
		
	}
	private void setCouponUsed(String id,String password,int type){

		SetCouponUsed couponUsed = new SetCouponUsed(
				getActivity(), id, password,type,
				new CallBack<JSONObject>() {
					@Override
					public void callBack(JSONObject object) {
						try {
							int res = object.getInt("error");
							if (res == 0) {
								isSuccess = true;
								showResultDialog(resultMessage);
							} else {
								isSuccess = false;
								showResultDialog(object.getString("errdesc"));
							}
						} catch (JSONException e) {}
					}
				});
		couponUsed.execute();
	}
	
	private void showToastResult (String message){
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}

}
