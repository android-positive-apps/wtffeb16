package com.wtf.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewAnimator;
import android.widget.ViewFlipper;

import com.android.volley.toolbox.NetworkImageView;
import com.wtf.adapters.FriendsListAdapter;
import com.wtf.dataBase.Preferences;
import com.wtf.dataBase.UserFriendsManager.FriendsCallbac;
import com.wtf.objects.Coupon;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.Gift;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetAppuserId;
import com.wtf.serverTask.GetCoupon;
import com.wtf.serverTask.GetRouletteCoupons;
import com.wtf.serverTask.TransferGiftsBetweenUsersTask;
import com.wtf.utils.Animation3D;
import com.wtf.utils.GiftUtils;
import com.wtf.utils.SimpleFacebookUtils;

public class GiftsFragment extends Fragment implements OnClickListener{

	public static final String TAG = "GiftsFragment";

	
	private FrameLayout noFacebookFrame;
	private Button facebookLogin;
	private boolean isRejisteredUser;
	
	private ImageView[] points = new ImageView[4];
	private ImageView[] userGifts = new ImageView[4];
	private ImageView[] receivedGifts = new ImageView[4];
	
	private TextView pointsTv;
	private ProgressBar userPointProgress;
	
	private ViewFlipper mViewFlipper1;
	private ViewFlipper mViewFlipper2;
	private ViewFlipper mViewFlipper3;

	private List<ImageView> mImageViews1;
	private List<ImageView> mImageViews2;
	private List<ImageView> mImageViews3;

	private int mSpeed;
	private int mCount1 = new Random().nextInt(10) + 10;
	private int mCount2 = new Random().nextInt(10) + 10;
	private int mCount3 = new Random().nextInt(10) + 10;
	private int mFactor = 10;

	private ImageView mSlotHandel;
	private boolean isOn ;

	private Dialog friendDialog;
	
	private ArrayList<FacebookFriand> userFacebookFriends;
	
	public GiftsFragment () {
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.activity_gifts, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		super.onCreate(savedInstanceState);
		noFacebookFrame = (FrameLayout)view.findViewById(R.id.no_facebook_frame);
		facebookLogin = (Button)view.findViewById(R.id.facebook_btn);
		facebookLogin.setOnClickListener(this);
		if (Preferences.getInstans(getActivity()).getFacebookId().equals("0")){
			isRejisteredUser = false;
			noFacebookFrame.setVisibility(View.VISIBLE);
		}else{
			Log.e("appuserid" , "id = " +Preferences.getInstans(getActivity()).getUserId() );
			isRejisteredUser = true;
			noFacebookFrame.setVisibility(View.GONE);
		}
		
		MainActivity.showTopButtons();
		isOn = true;
		
		points[0] =(ImageView)view.findViewById(R.id.user_points_1);
		points[1] =(ImageView)view.findViewById(R.id.user_points_2);
		points[2] =(ImageView)view.findViewById(R.id.user_points_3);
		points[3] =(ImageView)view.findViewById(R.id.user_poitns_4);
		
		userGifts[0] = (ImageView)view.findViewById(R.id.user_gift_coupon);
		userGifts[1] = (ImageView)view.findViewById(R.id.user_gift_coinse);
		userGifts[2] = (ImageView)view.findViewById(R.id.user_gift_pricent);
		userGifts[3] = (ImageView)view.findViewById(R.id.user_gift_king);
		
		receivedGifts[0] = (ImageView)view.findViewById(R.id.received_gift_coupon);
		receivedGifts[1] = (ImageView)view.findViewById(R.id.received_gift_coins);
		receivedGifts[2] = (ImageView)view.findViewById(R.id.received_gift_pricent);
		receivedGifts[3] = (ImageView)view.findViewById(R.id.received_gift_king);
		

		mViewFlipper1 = (ViewFlipper) view.findViewById(R.id.view_flipper1);
		mViewFlipper2 = (ViewFlipper) view.findViewById(R.id.view_flipper2);
		mViewFlipper3 = (ViewFlipper) view.findViewById(R.id.view_flipper3);
		
		pointsTv = (TextView)view.findViewById(R.id.points_tv);
		pointsTv.setText(getString(R.string.my_score) +  BMMApplication.getCurrentUserProfile().getPoints());
		
		userPointProgress = (ProgressBar)view.findViewById(R.id.user_points_pb);
		userPointProgress.setMax(GiftUtils.GIFT_4);
		userPointProgress.setProgress(BMMApplication.getCurrentUserProfile().getPoints());
		
		
		
		fillPointsIcons (BMMApplication.getCurrentUserProfile().getPoints());
		fillUserGiftIcon (BMMApplication.getUserGifts());
		fiilReceivedGiftsIcons (BMMApplication.getReceivedGifts());

		mImageViews1 = new ArrayList<ImageView>();
		mImageViews2 = new ArrayList<ImageView>();
		mImageViews3 = new ArrayList<ImageView>();

		mSlotHandel = (ImageView) view.findViewById(R.id.gifts_slot_handel);

		mSlotHandel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Animation animation = new Animation3D(0, -180, 0,
						v.getHeight() - 14, 0, true);
				animation.setDuration(1000);
				mSlotHandel.startAnimation(animation);
				startSlotAnimation();
			}
		});
		
		
		GetRouletteCoupons coupons = new GetRouletteCoupons(getActivity(), new CallBack<List<Coupon>>() {
			
			@Override
			public void callBack(List<Coupon> list) {
				
				initList(mImageViews1 , list);
				initList(mImageViews2 , list);
				initList(mImageViews3 , list);
				initFlipper(mViewFlipper1, mImageViews1);
				initFlipper(mViewFlipper2, mImageViews2);
				initFlipper(mViewFlipper3, mImageViews3);
			}
		});
		
		coupons.execute();
		
	}
	
	private void showGiftPressedDialog (final Gift gift) {
		String[] items = {getString(R.string.use_in_gift),getString(R.string.send_gift_to_friend)};
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.i_whant_to));
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					getFragmentManager()
					.beginTransaction()
					.replace(R.id.container,new GiftResultFragment(gift))
					.addToBackStack(null).commit();
					break;
				case 1:
					BMMApplication.getUserFacebookFriends(new FriendsCallbac() {
						@Override
						public void onFriendsArrived(ArrayList<FacebookFriand> result) {
							userFacebookFriends = result;
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									showUserFriendsDialog(gift);
								}
							});
						}
					});
					
					break;
				}
			}
		});
		builder.setNegativeButton(getString(R.string.cancel), null);
		Dialog dialog = builder.create();
		dialog.show();
	}
	
	private void showUserFriendsDialog (final Gift gift) {
		View v = getActivity().getLayoutInflater().inflate(R.layout.friends_list_dialog, null);
		ListView friendsListView = (ListView) v.findViewById(R.id.friends_list);
		friendsListView.setAdapter(new FriendsListAdapter(getActivity()));
		friendsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				FacebookFriand selected = userFacebookFriends.get(position);
				
				transferGiftsBetweenUsers(selected.getFBID(),gift.getId());
				if (friendDialog != null){
					friendDialog.dismiss();
				}
			}
		});
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(v);
		builder.setNegativeButton("cancel", null);
		friendDialog = builder.create();
		friendDialog.show();
	}
	
	private void transferGiftsBetweenUsers (String friendFacebookId, final String giftId){
		
		GetAppuserId getAppuserId = new GetAppuserId(getActivity(),
				friendFacebookId, new com.wtf.serverTask.CallBack<String> (){
			@Override
			public void callBack(String object) {
				new TransferGiftsBetweenUsersTask(getActivity(),object,giftId).execute();
			}
		});
		getAppuserId.execute();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		isOn=  false;
		MainActivity.hideTopButtons();
		super.onDestroy();
		
	}

	/**
	 * @param receivedGifts2
	 */
	private void fiilReceivedGiftsIcons(final ArrayList<Gift> receivedGifts2) {
		
		if (receivedGifts2.size() == 0){
			return;
		}
		
		for (int i = 0; i < receivedGifts2.size(); i++) {
			if (receivedGifts2.get(i).getGiftType() >= this.receivedGifts.length){
				continue;
			}
			final Gift tempGift = receivedGifts2.get(i);
			ImageView temp = this.receivedGifts[receivedGifts2.get(i).getGiftType()];
			temp.setSelected(true);
			temp.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showGiftPressedDialog (tempGift);
				}
			});
		}
		
	}
	/**
	 * @param userGifts
	 */
	private void fillUserGiftIcon(ArrayList<Gift> userGifts) {
		
		if (userGifts.size() == 0){
			return;
		}
		
		for (int i = 0; i < userGifts.size(); i++) {
			if (userGifts.get(i).getGiftType() >= this.userGifts.length){
				continue;
			}
			final Gift tempGift = userGifts.get(i);
			ImageView temp = this.userGifts[userGifts.get(i).getGiftType()];
			temp.setSelected(true);
			temp.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showGiftPressedDialog (tempGift);
				}
			});
			
		}
	}
	/**
	 * @param points
	 */
	private void fillPointsIcons(int points) {
		int iter = GiftUtils.getUserGiftsByUserPoints(points);
		for (int i = 0; i < iter; i++) {
			this.points[i].setSelected(true);
		}
	}
	
	
	private void startSlotAnimation() {

		mSpeed = 150;
		Handler h = new Handler();
		h.postDelayed(r, mSpeed);
	}

	
	
	private void initFlipper(ViewFlipper viewFlipper, List<ImageView> imageViews) {

		for (int i = 0; i < imageViews.size(); i++) {
			viewFlipper.addView(imageViews.get(i));
		}
	}
	
	
	
	private void initList(List<ImageView> list, final List<Coupon> coupons) {

		NetworkImageView imageView = null;
		for (int i = 0; i < coupons.size(); i++) {

			try {

				final Coupon currentCoupon = coupons.get(i);

				imageView = new NetworkImageView(getActivity());
				imageView.setLayoutParams(new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				imageView.setScaleType(ScaleType.FIT_XY);

				/*Picasso.with(getActivity()).load(coupons.get(i).getImgUrl())
						.fit().into(imageView);*/
				imageView.setImageUrl(coupons.get(i).getImgUrl());

				imageView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						// get the full coupon and show coupon fragment

						GetCoupon getCoupon = new GetCoupon(currentCoupon,
								getActivity(), new CallBack<Coupon>() {

									@Override
									public void callBack(Coupon coupon) {

										// MainActivity.cleanAllFragments();

										getFragmentManager()
												.beginTransaction()
												.replace(
														R.id.container,
														new GiftResultFragment(
																coupon))
												.addToBackStack(null).commit();
									}
								});

						getCoupon.execute();
					}
				});
				list.add(imageView);
			} catch (Exception e) {

			}
		}

		Collections.shuffle(list);

	}
	


	private Runnable r = new Runnable() {
		
		@Override
		public void run() {
			down(mCount1 , mViewFlipper1);
			mCount1--;
			down(mCount2 , mViewFlipper2);
			mCount2--;
			down(mCount3 , mViewFlipper3);
			mCount3--;
			
			if (mCount1 < 1 && mCount2 < 1 && mCount3 < 1 ) {
				mCount1 = new Random().nextInt(10) + 10;
				mCount2 = new Random().nextInt(10) + 10;
				mCount3 = new Random().nextInt(10) + 10;
				return;
			} else {
				Handler h = new Handler();
				h.postDelayed(r, mSpeed);
			}
		}
	};

	private void down(int count , ViewAnimator fliper) {
		if(count == 0 ){
			return;
		}
		mSpeed += mFactor;
		Animation outToBottom = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 1.0f);
		outToBottom.setInterpolator(new AccelerateInterpolator());
		outToBottom.setDuration(mSpeed);
		Animation inFromTop = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromTop.setInterpolator(new AccelerateInterpolator());
		inFromTop.setDuration(mSpeed);
		fliper.clearAnimation();
		fliper.setInAnimation(inFromTop);
		fliper.setOutAnimation(outToBottom);

		

		if (fliper.getDisplayedChild() == 0) {
			fliper.setDisplayedChild(mImageViews1.size() - 1);
		} else {
			fliper.showPrevious();
		}
		
	}
	
	@Override
	public void onClick(View arg0) {
		
		loginWithFacebook();
		
	}
	
	private void loginWithFacebook() {

		SimpleFacebookUtils.facebookLogin(getActivity(),
				new CallBack<UserProfile>() {
					@Override
					public void callBack(UserProfile object) {
						if (object != null){
							BMMApplication.setCurrentUserProfile(object);
							Preferences.getInstans(getActivity()).setUserId(
									object.getId());
							getFragmentManager().beginTransaction()
									.detach(GiftsFragment.this)
									.attach(GiftsFragment.this).commit();
						}
						
					}
				});
	}
	
	
	
	
	
	
	
	

}
