/*
 * Copyright (C) 2014 BeyondAR
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wtf.activity;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.LocationManager;
import android.util.Log;

import com.beyondar.android.util.location.BeyondarLocationManager;
import com.beyondar.android.world.GeoObject;
import com.beyondar.android.world.World;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.Business;
import com.wtf.objects.FacebookFriand;

@SuppressLint("SdCardPath")
public class CustomWorldHelper {
	public static final int LIST_TYPE_EXAMPLE_1 = 1;

	public static World sharedWorld;
	
	public static World generateObjects(Context context,ArrayList<ArrayList<Business>> businesses,ArrayList<FacebookFriand> friends) {
		
		sharedWorld = new World(context);
		
		sharedWorld.setGeoPosition(Double.parseDouble(Preferences.getInstans(context).getLatitude()),
				Double.parseDouble(Preferences.getInstans(context).getLongitude()), 0);
		
		// The user can set the default bitmap. This is useful if you are
		// loading images form Internet and the connection get lost
		sharedWorld.setDefaultImage(R.drawable.location_icon);
		BeyondarLocationManager
		.setLocationManager((LocationManager) context.getSystemService(Context.LOCATION_SERVICE));
         BeyondarLocationManager.addWorldLocationUpdate(sharedWorld);
	
			
		ArrayList<ArrayList<GeoObject>> objects = new ArrayList<ArrayList<GeoObject>>();
		
		for (int i = 0; i < businesses.size(); i++) {
			ArrayList<GeoObject> tempObjects = new ArrayList<GeoObject>();
			for (int j = 0; j < businesses.get(i).size(); j++) {
				GeoObject temp = new GeoObject();
				temp.setGeoPosition(businesses.get(i).get(j).getLat(), businesses.get(i).get(j).getLng());
				//temp.setImageResource(businesses.get(i).get(j).getMarkerRecourse());
				temp.setImageUri(businesses.get(i).get(j).getImage());
				
				temp.setName(businesses.get(i).get(j).getName() + 
						"||" + businesses.get(i).get(j).getAddress() + 
						"||" + businesses.get(i).get(j).getLat() + 
						"||" + businesses.get(i).get(j).getLng()+ 
						"||" + businesses.get(i).get(j).getMarkerRecourse() + 
						"||" + businesses.get(i).get(j).getId()+ 
						"||" + businesses.get(i).get(j).getExternalID() + 
						"||" + businesses.get(i).get(j).getMarkerRecourseSelected()+ 
						"||" + businesses.get(i).get(j).getImage() + 
						"||" + "*"+
						"||" + businesses.get(i).get(j).getCategoryType()
 						);
				temp.faceToCamera(true);
				temp.setVisible(true);
				tempObjects.add(temp);
			}
			objects.add(tempObjects);
		}
		
		for (int i = 0; i < objects.size(); i++) { 
			for (int j = 0; j < objects.get(i).size(); j++) {
				sharedWorld.addBeyondarObject(objects.get(i).get(j));
			}
		}
		
		if (friends != null){
			ArrayList<GeoObject> friendsObjects = new ArrayList<GeoObject>();
			
			for (int i = 0; i < friends.size(); i++) {

				GeoObject temp = new GeoObject();
				temp.setGeoPosition(friends.get(i).getLat(), friends
						.get(i).getLon());
				//temp.setImageResource(R.drawable.man_location_icon);
				temp.setImageUri(friends.get(i).getImage());
				temp.setName(friends.get(i).getFirstName() + friends.get(i).getLastName() + 
						"||"+ "" + 
						"||" + friends.get(i).getLat() +
						"||" + friends.get(i).getLon()+ 
						"||" + R.drawable.man_location_icon + 
						"||" + "*" + 
						"||" + "*" + 
						"||" + R.drawable.man_location_icon + 
						"||" + friends.get(i).getImage() + 
						"||" + friends.get(i).getFBID()+
						"||" + "10")
				;
				
				temp.faceToCamera(true);
				temp.setVisible(true);
				friendsObjects.add(temp);

			}
			
			for (int i = 0; i < friendsObjects.size(); i++) {
				sharedWorld.addBeyondarObject(friendsObjects.get(i));
			}
		}
        
			
		return sharedWorld;
	}
	
	
	
    

}
