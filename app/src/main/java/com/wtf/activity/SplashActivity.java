package com.wtf.activity;

//import com.crashlytics.android.Crashlytics;
import org.json.JSONObject;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.CreateUpdateUserTask;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class SplashActivity extends ActionBarActivity {

	boolean ison = true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		
		//Crashlytics.start(this);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash);
		TextView appVersion = (TextView)findViewById(R.id.app_version);
		appVersion.setText(getApplicationVersion());
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				if (ison){
					startActivity(new Intent(SplashActivity.this,MainActivity.class));
					finish();
				}
			}
		}, 1000);
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		ison = false;
	}

	private  String getApplicationVersion() {
		String appVersion = "";
		try {
			PackageManager manager = getPackageManager();
			PackageInfo info = manager.getPackageInfo(
					getPackageName(), 0);
			appVersion =  info.versionName;
		} catch (Exception e) {
		}
		return appVersion;
	}

	
	
}
