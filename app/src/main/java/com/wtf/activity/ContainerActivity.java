package com.wtf.activity;

import com.wtf.fragments.BusinessFragment;
import com.wtf.objects.Business;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class ContainerActivity extends FragmentActivity {

	public static final String EXTRA_BUISNESS = "buisness";
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_container);
		
		getSupportFragmentManager().beginTransaction()
		.replace(R.id.container2, BusinessFragment.
				newInstance(0,(Business)getIntent().
						getExtras().getSerializable(EXTRA_BUISNESS),true) ,
				BusinessFragment.TAG).commit();
		
	}

	
}
