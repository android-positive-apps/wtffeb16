/**
 * 
 */
package com.wtf.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.wtf.dataBase.UserFriendsManager;
import com.wtf.dataBase.UserFriendsManager.FriendsCallbac;
import com.wtf.objects.BuisnessGiftsAndCoupons;
import com.wtf.objects.CreditCard;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.Gift;
import com.wtf.objects.Place;
import com.wtf.objects.Rating;
import com.wtf.objects.UserProfile;
import com.wtf.storage.StorageListener;

/**
 * @author Nati Gabay
 *
 */
public class BMMApplication extends Application {
	
	
	
	
	// application name in facebook
	public static final String FACEBOOK_NAME_SPACE = "bmmapplication";
	public static final String FACEBOOK_UBIKT_NAME_SPACE = "ubiktnamespace";

	// the facebook manager ( create from FacebookSimple library - set part for
	// management facebook SDK)
	public static SimpleFacebook mSimpleFacebook;

	// permissions for read special data of user as email from facebook
	Permission[] permissions = new Permission[] { Permission.EMAIL,Permission.USER_FRIENDS,Permission.USER_PHOTOS };
	
	
	
	private static UserProfile currentUserProfile;
	private static Context appContext;
	private static ArrayList<Place> places = new ArrayList<Place>();
	private static ArrayList<Rating> ratings = new ArrayList<Rating>();
	private static ArrayList<CreditCard> cards = new ArrayList<CreditCard>();
	private static ArrayList<Gift> userGifts = new ArrayList<Gift>();
	private static ArrayList<Gift> receivedGifts = new ArrayList<Gift>();
	
	
	private static BuisnessGiftsAndCoupons buisnessGiftsAndCoupons;
	public static int currentCatgoryIcon;
	
	public static UserFriendsManager friendsManager;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
//		Log.e("FlavorLog", " build config is: " + BuildConfig.FLAVOR);
//		if (BuildConfig.FLAVOR.equals("UbiKT")) {
//
//			Locale locale = new Locale("mx");
//			Locale.setDefault(locale);
//			Configuration config = new Configuration();
//			config.locale = locale;
//			getBaseContext().getResources().updateConfiguration(config,
//					getBaseContext().getResources().getDisplayMetrics());
//		}


		appContext = getApplicationContext();
		Place.readPlacesFromStorage(getApplicationContext(), new StorageListener() {
			
			@Override
			public void onStorageDataReceived(Object data) {
				places = (ArrayList<Place>)data;
			}
		});
		Rating.readRatingsFromStorage(getApplicationContext(), new StorageListener() {
			
			@Override
			public void onStorageDataReceived(Object data) {
				// TODO Auto-generated method stub
				ratings = (ArrayList<Rating>)data;
			}
		});
        CreditCard.readCreditCardsFromStorage(getApplicationContext(), new StorageListener() {
			
			@Override
			public void onStorageDataReceived(Object data) {
				// TODO Auto-generated method stub
				cards = (ArrayList<CreditCard>)data;
			}
		});
        friendsManager = UserFriendsManager.getInstance();
       // configure facebook manager  
       facebookManagerCunfiguration();
       
	}
	
	
	
	
	private void facebookManagerCunfiguration ()
	{

//		if (BuildConfig.FLAVOR.equals("UbiKT")) {
//			SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
//					.setAppId(getResources().getString(R.string.fb_app_id))
//					.setNamespace(FACEBOOK_UBIKT_NAME_SPACE)
//					.setPermissions(permissions)
//					.setAskForAllPermissionsAtOnce(true)
//					.build();
//			SimpleFacebook.setConfiguration(configuration);
//		}else{
			SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
					.setAppId(getResources().getString(R.string.app_id))
					.setNamespace(FACEBOOK_NAME_SPACE)
					.setPermissions(permissions)
					.setAskForAllPermissionsAtOnce(true)
					.build();
			SimpleFacebook.setConfiguration(configuration);
//		}

	}
	
	/**
	 * @return the currentUserProfile
	 */
	public static UserProfile getCurrentUserProfile() {
		return currentUserProfile;
	}

	/**
	 * @param currentUserProfile the currentUserProfile to set
	 */
	public static void setCurrentUserProfile(UserProfile currentUserProfile) {
		BMMApplication.currentUserProfile = currentUserProfile;
	}

	/**
	 * @return the places
	 */
	public static ArrayList<Place> getPlaces() {
		
		return places;
	}

	/**
	 * @param places the places to set
	 */
	public static void setPlaces(Context context,Place places) {
		BMMApplication.places.add(places);
		Place.savePlacesToStorage(context, BMMApplication.places);
	}

	/**
	 * @return the ratings
	 */
	public static ArrayList<Rating> getRatings() {
		
		return ratings;
	}


	public static void setRatings(Context context,Rating rating) {
		BMMApplication.ratings.add(rating);
		Rating.saveRatingsToStorage(context, BMMApplication.ratings);
	}

	/**
	 * @return the cards
	 */
	public static ArrayList<CreditCard> getCards() {
		
		return cards;
	}


	public static void setCards(Context context,CreditCard card) {
		BMMApplication.cards.add(card);
		CreditCard.saveCrditCardsToStorage(context, BMMApplication.cards);
	}


	/**
	 * @return the buisnessGiftsAndCoupons
	 */
	public static BuisnessGiftsAndCoupons getBuisnessGiftsAndCoupons() {
		return buisnessGiftsAndCoupons;
	}


	/**
	 * @param buisnessGiftsAndCoupons the buisnessGiftsAndCoupons to set
	 */
	public static void setBuisnessGiftsAndCoupons(
			BuisnessGiftsAndCoupons buisnessGiftsAndCoupons) {
		BMMApplication.buisnessGiftsAndCoupons = buisnessGiftsAndCoupons;
	}


	/**
	 * @return the userFacebookFriends
	 */
	public static void getUserFacebookFriends(FriendsCallbac callbac) {
		friendsManager.read(callbac);
	}


	/**
	 * @return the userFacebookFriends
	 */
	public static ArrayList<FacebookFriand> getUserFacebookFriends() {
		return friendsManager.get();
	}
	
	/**
	 * @param userFacebookFriends the userFacebookFriends to set
	 */
	public static void setUserFacebookFriends(ArrayList<FacebookFriand> userFacebookFriends) {
		friendsManager.addFriends(userFacebookFriends);
	}


	/**
	 * @return the appContext
	 */
	public static Context getAppContext() {
		return appContext;
	}


	/**
	 * @return the userGifts
	 */
	public static ArrayList<Gift> getUserGifts() {
		return userGifts;
	}


	/**
	 * @param userGifts the userGifts to set
	 */
	public static void setUserGifts(ArrayList<Gift> userGifts) {
		BMMApplication.userGifts = userGifts;
	}


	/**
	 * @return the receivedGifts
	 */
	public static ArrayList<Gift> getReceivedGifts() {
		return receivedGifts;
	}


	/**
	 * @param receivedGifts the receivedGifts to set
	 */
	public static void setReceivedGifts(ArrayList<Gift> receivedGifts) {
		BMMApplication.receivedGifts = receivedGifts;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
