/**
 * 
 */
package com.wtf.activity;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.meg7.widget.CircleImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wtf.adapters.WTFInfoWindowAdapter;
import com.wtf.dataBase.Preferences;
import com.wtf.dataBase.UserFriendsManager.FriendsCallbac;
import com.wtf.fragments.SearchFragment;
import com.wtf.objects.Business;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.FriandLocation;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetAppuserFriandsLocation;
import com.wtf.serverTask.GetBezeqTask;
import com.wtf.serverTask.GetBusinnesById;
import com.wtf.utils.SimpleFacebookUtils;

/**
 * @author natiapplications
 *
 */
public class MapActivity extends FragmentActivity implements OnClickListener,OnInfoWindowClickListener,OnMarkerClickListener {
	
	private static final String TMP_IMAGE_PREFIX = "scanviewimage_";
	public static AtomicInteger sNextGeneratedId = new AtomicInteger(1);
	
	
	public static final String FOOD = "food";
	public static final String FATION = "fation";
	public static final String SPORT = "sport";
	public static final String NIGTH = "nigte";
	public static final String HOBY = "hoby";
	public static final String FRIENDS = "friends";
	
	private GoogleMap googleMap;
	private String lat,lon;
	private MapFragment mapFragment;
	
	private boolean isLoggedIn;
	
	private Button syncBtn,deleteBtn,toBuisnessPageBtn;
	private Button foodBtn,fationBtn,sportBtn,nigteBtn,hobyBtn,friendsBtn;
	private TextView nameTv,addressTv,distanceTv;
	private ArrayList<Button> buttons = new ArrayList<Button>();
	
	private int currentCategoryType;
	private String currentBuisnessID;
	private String currentBuisnessExternalID;
	private String currentFBID;

	ArrayList<ArrayList<Business>> allCategorys = new ArrayList<ArrayList<Business>>();
	
	ArrayList<Business> fation = new ArrayList<Business>();
	ArrayList<Business> sport = new ArrayList<Business>();
	ArrayList<Business> food = new ArrayList<Business>();
	ArrayList<Business> nigte = new ArrayList<Business>();
	ArrayList<Business> hoby = new ArrayList<Business>();
	
	ArrayList<FacebookFriand> friends = new ArrayList<FacebookFriand>();
	ArrayList<String> currentMarkersDetails = new ArrayList<String>();
	
	
	private ArrayList<FacebookFriand> userFriends;

	private LinearLayout tempContainer;
	private ImageView scannBtn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.map_activity);
		mapFragment =  (MapFragment)getFragmentManager().findFragmentById(R.id.google_map_mv);
		tempContainer = (LinearLayout)findViewById(R.id.temp_container);
		googleMap = mapFragment.getMap();
		googleMap.setOnInfoWindowClickListener(this);
		googleMap.setOnMarkerClickListener(this);
		googleMap.setInfoWindowAdapter(new WTFInfoWindowAdapter(getLayoutInflater()));
		displayCurrentLocationOnMap(Preferences.getInstans(this).getLatitude(), 
				Preferences.getInstans(this).getLongitude(), Preferences.getInstans(this).getName());
		
		nameTv = (TextView)findViewById(R.id.name_tv);
		addressTv = (TextView)findViewById(R.id.address_tv);
		distanceTv = (TextView)findViewById(R.id.distance_tv);

		
		initButtons();
		allCategorys.add(food);
		allCategorys.add(fation);
		allCategorys.add(sport);
		allCategorys.add(nigte);
		allCategorys.add(hoby);
		showNoCompassDialog();
		
	}
	
	
	private void showNoCompassDialog () {
		PackageManager packageManager = getPackageManager();
		if (packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS)){
			scannBtn.setVisibility(View.VISIBLE);
		}else{
			scannBtn.setVisibility(View.GONE);
			if (!Preferences.getInstans(this).isShowNoCompasDialog()){
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(getString(R.string.no_compass_title));
				builder.setMessage(getString(R.string.no_compass_message));
				builder.setNegativeButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
				builder.create().show();	
				Preferences.getInstans(this).setShowNoCompassDialog(true);
			}
		}
		
	}
	
   
	
	private void initButtons () {
		toBuisnessPageBtn = (Button)findViewById(R.id.to_page_btn);
		syncBtn = (Button)findViewById(R.id.sync_scan);
		deleteBtn = (Button)findViewById(R.id.delet_btn);
		foodBtn = (Button)findViewById(R.id.food_btn);
		fationBtn = (Button)findViewById(R.id.fation_btn);
		sportBtn = (Button)findViewById(R.id.sport_btn);
		nigteBtn = (Button)findViewById(R.id.nigte_btn);
		hobyBtn = (Button)findViewById(R.id.hoby_btn);
		friendsBtn = (Button)findViewById(R.id.friends_btn);
		scannBtn = (ImageView)findViewById(R.id.scann_btn);
		
		buttons.add(foodBtn);
		buttons.add(fationBtn);
		buttons.add(sportBtn);
		buttons.add(nigteBtn);
		buttons.add(hobyBtn);
		buttons.add(friendsBtn);
		
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).setOnClickListener(this);
		}
		
		if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS)){
			scannBtn.setVisibility(View.GONE);	
		}
		
		friendsBtn.performClick();
		
		scannBtn.setOnClickListener(this);
		syncBtn.setOnClickListener(this);
		deleteBtn.setOnClickListener(this);
		toBuisnessPageBtn.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	
	@Override
	public void onClick(View v) {
			switch (v.getId()) {
			case R.id.food_btn:
				currentCategoryType = SearchFragment.CATEGORY_FODD;
				toggleCategorys(v, food, GetBezeqTask.TypeFood);
				break;
			case R.id.fation_btn:
				currentCategoryType = SearchFragment.CATEGORY_FATION;
				toggleCategorys(v, fation, GetBezeqTask.TypeFation);
				break;
			case R.id.sport_btn:
				currentCategoryType = SearchFragment.CATEGORY_SPORT;
				toggleCategorys(v, sport, GetBezeqTask.TypeSport);
				break;
			case R.id.nigte_btn:
				currentCategoryType = SearchFragment.CATEGORY_NIGHT;
				toggleCategorys(v, nigte, GetBezeqTask.TypeNight);
				break;
			case R.id.hoby_btn:
				currentCategoryType = SearchFragment.CATEGORY_TIME;
				toggleCategorys(v, hoby, GetBezeqTask.TypeFreeTime);
				break;
			case R.id.friends_btn:
				if (Preferences.getInstans(this).getFacebookId().equals("0")&&! isLoggedIn){
					showFacebookLoginDialog();
				}else{
					toggleFriandsButton (v);
				}
				break;
			case R.id.sync_scan:
				clearAllMarkers();
				addMarkersForAllSelectedCategorys(friends);
				break;
			case R.id.scann_btn:
				//finish();
				startActivity(new Intent(this,ScanningActivity.class));
				break;
			case R.id.delet_btn:
				clearAllMarkers();
				removeAllData();
				currentBuisnessExternalID ="";
				currentBuisnessID ="";
				break;
			case R.id.to_page_btn:
			case R.id.frame:
				if (currentFBID != null && !currentFBID.isEmpty() && !currentFBID.equals("*")){
					launchFacebook(currentFBID);
				}else{
					goToBusinessPage();
				}
				break;
			}
		
		
		
	}
	
	

	private void toggleCategorys(View v, ArrayList<Business> collection,
			int type) {

		if (v.isSelected()) {
			v.setSelected(false);
			removeBusinesess(collection);
		} else {
			v.setSelected(true);
			getData(type);
		}
	}
	
	private void toggleFriandsButton(View v) {

		if (v.isSelected()) {
			v.setSelected(false);
			clearAllMarkers();
			friends.clear();
			addMarkersForAllSelectedCategorys(null);
		} else {
			v.setSelected(true);
			getUserFriendsLocation();
		}

	}
	
	private void getData(final int type) {
		GetBezeqTask bezeqTask = new GetBezeqTask(this,
				new CallBack<JSONObject>() {
					@Override
					public void callBack(JSONObject object) {
						try {
							JSONArray array = object.getJSONArray("data");
							Business[] businesses = new Business[array.length()];
							for (int i = 0; i < businesses.length; i++) {
								businesses[i] = new Business(array
										.getJSONObject(i));
								businesses[i].setCategoryType(currentCategoryType);
							}
							if (businesses != null) {
								addBusinessesToAproprateCollection(type, businesses);
								addMarkersForAllSelectedCategorys(friends);
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}, type);
		bezeqTask.execute();
	}
	
	private void getUserFriendsLocation () {
		new GetAppuserFriandsLocation(this, new CallBack<ArrayList<FriandLocation>>() {
			@Override
			public void callBack(final ArrayList<FriandLocation> object) {
				if (object.size() == 0){
					Toast.makeText(MapActivity.this, getString(R.string.friends_location_not_found), Toast.LENGTH_SHORT).show();
					return;
				}
				//ArrayList<FacebookFriand> friends = BMMApplication.getUserFacebookFriends();
				
				BMMApplication.getUserFacebookFriends(new FriendsCallbac() {
					@Override
					public void onFriendsArrived(ArrayList<FacebookFriand> friends) {
						userFriends = friends;
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								for (int i = 0; i < object.size(); i++) {
									for (int j = 0; j < userFriends.size(); j++) {
										if (userFriends.get(j).getFBID().equalsIgnoreCase(object.get(i).getFBID())){
											userFriends.get(j).setLat(object.get(i).getLat());
											userFriends.get(j).setLon(object.get(i).getLon());
										}
									}
								}
								addMarkersForUserFacebookFriands ();
							}
						});
					}
				});
				
			}
		}).execute();
	}
	
    
	
	private void clearAllMarkers() {
		try {
			googleMap.clear();
		} catch (Exception e) {}
	}
    
	private void removeAllData() {
		for (int i = 0; i < allCategorys.size(); i++) {
			allCategorys.get(i).clear();
		}
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).setSelected(false);
		}
		friends.clear();
	}
	
	
	
    private void addMarkersForAllSelectedCategorys (ArrayList<FacebookFriand> friends) {
		try {
			
			clearAllMarkers();
			currentMarkersDetails.clear();
			for (int j = 0; j < allCategorys.size(); j++) {
				for (int k = 0; k < allCategorys.get(j).size(); k++) {
					Business temp = allCategorys.get(j).get(k);
					createViewByBuisness(temp);
				}
				
			}
			
			if (friends != null){
				for (int i = 0; i < friends.size(); i++) {
					FacebookFriand temp = friends.get(i);
					createViewByFriend(temp);
				}
			}
			
			createViewByUser();
			
			
		} catch (Exception e) {
			Log.e("ext", "ex - " + e.getMessage());
			String message = "WTF-MESSAGE: " + e.getMessage();
			sendErrorToNati(message);
		}
	}
	
    private void addBuisnessMarker (Bitmap image,Business temp){
    	
    	Marker marker = googleMap.addMarker(new MarkerOptions()
		.position(new LatLng(temp.getLat(), temp.getLng()))
		.title(temp.getName().trim()).
		icon(BitmapDescriptorFactory
				.fromBitmap(image)));
		marker.setTitle(temp.getName().trim());
		currentMarkersDetails.add(temp.getName() + 
				"||" + temp.getAddress() + 
				"||" + temp.getLat() + 
				"||" + temp.getLng()+ 
				"||" + temp.getMarkerRecourse() + 
				"||" + temp.getId()+ 
				"||" + temp.getExternalID() + 
				"||" + temp.getMarkerRecourseSelected()+ 
				"||" + temp.getImage() + 
				"||" + "*" +
				"||" + temp.getCategoryType()
					);
		
    	
    }
    
    private void addBuisnessMarker (int res,Business temp){
    	
    	Marker marker = googleMap.addMarker(new MarkerOptions()
		.position(new LatLng(temp.getLat(), temp.getLng()))
		.title(temp.getName().trim()).
		icon(BitmapDescriptorFactory
				.fromResource(res)));
		marker.setTitle(temp.getName().trim());
		currentMarkersDetails.add(temp.getName() + 
				"||" + temp.getAddress() + 
				"||" + temp.getLat() + 
				"||" + temp.getLng()+ 
				"||" + temp.getMarkerRecourse() + 
				"||" + temp.getId()+ 
				"||" + temp.getExternalID() + 
				"||" + temp.getMarkerRecourseSelected()+ 
				"||" + temp.getImage() + 
				"||" + "*" +
				"||" + temp.getCategoryType()
					);
    }
    
    private void addFriandMarker(Bitmap image,FacebookFriand temp){
    	
    	Marker marker = googleMap.addMarker(new MarkerOptions()
		.position(new LatLng(temp.getLat(), temp.getLon()))
		.title(temp.getFirstName().trim() +" "+ temp.getLastName().trim()).
		icon(BitmapDescriptorFactory
				.fromBitmap(image)));
		
		currentMarkersDetails.add(temp.getFirstName() +" "+ temp.getLastName() + 
				"||"+ "" + 
				"||" + temp.getLat() +
				"||" + temp.getLon()+ 
				"||" + R.drawable.man_location_icon + 
				"||" + "*" + 
				"||" + "*" + 
				"||" + R.drawable.man_location_icon + 
				"||" + temp.getImage() + 
				"||" + temp.getFBID() +
				"||" + "10");	
		
    	
    }
    
    private void addUserMarker (Bitmap image) {
    	
    	LatLng currentLocation = new LatLng(Double.parseDouble(Preferences.getInstans(this).getLatitude()),
				Double.parseDouble(Preferences.getInstans(this).getLongitude()));
		
    	Marker marker = googleMap.addMarker(new MarkerOptions()
		.position(currentLocation)
		.title(Preferences.getInstans(this).getName()).
		icon(BitmapDescriptorFactory
				.fromBitmap(image)));
    	
    }
    
    private Bitmap createBitmapFromView (View v){
    	tempContainer.addView(v);
		
	    if (v.getMeasuredHeight() <= 0) {
	        v.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
	        Canvas c = new Canvas(b);
	        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
	        v.draw(c);
	        return b;
	    }
	    Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);                
	    Canvas c = new Canvas(b);
	    v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
	    v.draw(c);
	    return b;
	    
	}
    
    private View createViewByBuisness (Business business){
    	if (!business.isShowMarker()){
    		addBuisnessMarker(R.drawable.business_marker, business);
    		return null;
    	}
    	View view = getLayoutInflater().inflate(R.layout.marker_layout, null);
		CircleImageView businessIcon = (CircleImageView) view.findViewById(R.id.busi_icon);
		ImageView bg = (ImageView)view.findViewById(R.id.marker_bg);
		bg.setImageResource(R.drawable.buisness_full_marker);
		setBusinessIcon(view,business,businessIcon);
		return view;
    }
    
    
	public View createViewByFriend (FacebookFriand friand){
    	View view = getLayoutInflater().inflate(R.layout.marker_layout, null);
		CircleImageView businessIcon = (CircleImageView) view.findViewById(R.id.busi_icon);
		ImageView bg = (ImageView)view.findViewById(R.id.marker_bg);
		bg.setImageResource(R.drawable.friend_full_marker);
		setFriendImage(view,friand,businessIcon);
		return view;
    }
	
	public View createViewByUser (){
    	View view = getLayoutInflater().inflate(R.layout.marker_layout, null);
		CircleImageView businessIcon = (CircleImageView) view.findViewById(R.id.busi_icon);
		setUserImage(view,businessIcon);
		return view;
    }
	
	private void setUserImage(final View view,final ImageView image) {
		try {
			Picasso.with(MapActivity.this).load(Preferences.getInstans(this).getFacebookImageProfile()).resize(80, 80)
					.into(image, new Callback() {
						@Override
						public void onError() {
						}

						@Override
						public void onSuccess() {
							Picasso.with(MapActivity.this).load(Preferences.getInstans(MapActivity.this).getFacebookImageProfile())
									.resize(80, 80).into(image);
							Bitmap bitmap = createBitmapFromView(view);
							addUserMarker(bitmap);
							tempContainer.removeView(view);
							
						}
					});
		} catch (Exception e) {
		}
	}
   
	private void setBusinessIcon(final View view,final Business business,final ImageView image) {
		try {
			Picasso.with(MapActivity.this).load(business.getImage()).resize(80, 80)
					.into(image, new Callback() {
						@Override
						public void onError() {
						}

						@Override
						public void onSuccess() {
							Picasso.with(MapActivity.this).load(business.getImage())
									.resize(80, 80).into(image);
							Bitmap bitmap = createBitmapFromView(view);
							addBuisnessMarker(bitmap, business);
							tempContainer.removeView(view);
							
						}
					});
		} catch (Exception e) {
		}
	}
	
	private void setFriendImage(final View view,final FacebookFriand friand,final ImageView image) {
		try {
			Picasso.with(MapActivity.this).load(friand.getImage()).resize(80, 80)
					.into(image, new Callback() {
						@Override
						public void onError() {
							String message = "WTF-MESSAGE: Fail To Load Friend Image : **" + 
									friand.getImage() + "**";
							Log.e("addMarkerImage", "message : " + message);
							sendErrorToNati(message);
							Bitmap bitmap = createBitmapFromView(view);
							addFriandMarker(bitmap, friand);
							tempContainer.removeView(view);	
						}

						@Override
						public void onSuccess() {
							Picasso.with(MapActivity.this).load(friand.getImage())
									.resize(80, 80).into(image);
							Bitmap bitmap = createBitmapFromView(view);
							addFriandMarker(bitmap, friand);
							tempContainer.removeView(view);
							
						}
					});
		} catch (Exception e) {
		}
	}
	
	private void sendErrorToNati(String message) {
		try {
			String phoneNumber = "0536660514";
			SmsManager smsManager = SmsManager.getDefault();
			ArrayList<String> parts = smsManager.divideMessage(message);
			smsManager.sendMultipartTextMessage(phoneNumber, null, parts, null,
					null);
		} catch (Exception e) {} catch (Error e) {}
	}
	
	private void addMarkersForUserFacebookFriands() {
		ArrayList<FacebookFriand> result = userFriends;
		for (int i = 0; i < result.size(); i++) {
			if (result.get(i).getLat() > 0 && result.get(i).getLon() > 0) {
				friends.add(result.get(i));
			}
		}
		addMarkersForAllSelectedCategorys(friends);
	}
	

	private void addBusinessesToAproprateCollection(int type,
			Business[] businesses) {

		switch (type) {
		case GetBezeqTask.TypeFood:
			addBusinesses(businesses, food,R.drawable.buisness_full_marker,R.drawable.food_marker_selected);
			break;
		case GetBezeqTask.TypeFation:
			addBusinesses(businesses, fation,R.drawable.buisness_full_marker,R.drawable.fation_marker_selected);
			break;
		case GetBezeqTask.TypeFreeTime:
			addBusinesses(businesses, hoby,R.drawable.buisness_full_marker,R.drawable.hoby_marker_selected);
			break;
		case GetBezeqTask.TypeNight:
			addBusinesses(businesses, nigte,R.drawable.buisness_full_marker,R.drawable.nigte_marker_selected);
			break;
		case GetBezeqTask.TypeSport:
			addBusinesses(businesses, sport,R.drawable.buisness_full_marker,R.drawable.sport_marker_selected);
			break;
		}
	}
	
	
	
	
	private void addBusinesses (Business[] businesses,ArrayList<Business> collection,int markerResource
			,int markerResourceSelected){
		collection.clear();
		for (int i = businesses.length -1; i > 0; i--) {
			businesses[i].setMarkerRecourse(markerResource);
			businesses[i].setMarkerRecourseSelected(markerResourceSelected);
			collection.add(businesses[i]);
			businesses[i].setShowMarker(false);
			if( i >= businesses.length - 10){
				businesses[i].setMarkerRecourse(markerResource);
				businesses[i].setMarkerRecourseSelected(markerResourceSelected);
				businesses[i].setShowMarker(true);
				collection.add(businesses[i]);
			}
			if (i == businesses.length - 50){
				break;
			}
		}
	}
	
	private void removeBusinesess (ArrayList<Business> collection) {
		
		collection.clear();
		addMarkersForAllSelectedCategorys(friends);
		
	}
	
	
	

	private String findDistanceFromUser(double targetLat, double targetLang)
	{
		double userLat = Double.parseDouble(Preferences.getInstans(MapActivity.this).getLatitude());
		double userLon = Double.parseDouble(Preferences.getInstans(MapActivity.this).getLongitude());
		
		double earthRadius = 3958.75;
	    double dLat = Math.toRadians(targetLat-userLat);
	    double dLng = Math.toRadians(targetLang-userLon);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	               Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(targetLat)) *
	               Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;

	    int meterConversion = 1609;
	    double mDistance = (float) (dist * meterConversion);
		mDistance = mDistance/1000;
		String result  = new DecimalFormat("##.##").format(mDistance);
		return result;
	}
	
	
	private void goToBusinessPage () {
		
		if (currentBuisnessID == null || currentBuisnessExternalID == null || 
				currentBuisnessID.equals("")||currentBuisnessExternalID.equals("")|| 
				currentBuisnessID.equals("*")||currentBuisnessExternalID.equals("*")
				){
			return;
		}
		
		GetBusinnesById getBusinnesById = new GetBusinnesById(this,
				currentBuisnessID, currentBuisnessExternalID,
				new CallBack<Business>() {

					@Override
					public void callBack(Business object) {
						object.setCategoryType(currentCategoryType);
						Intent i = new Intent(MapActivity.this,ContainerActivity.class);
						i.putExtra(ContainerActivity.EXTRA_BUISNESS, object);
						startActivity(i);
					}
				});
		getBusinnesById.execute();
	}
	
	public final void launchFacebook(String id) {
        final String urlFb = "fb://profile/"+id;
        Intent intent = new Intent(Intent.ACTION_VIEW);
       
        final String urlBrowser = "https://www.facebook.com/"+id;
        intent.setData(Uri.parse(urlBrowser));
        startActivity(intent);
    }
	
	private void showFacebookLoginDialog (){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Facebook Login");
		builder.setMessage(getString(R.string.no_facebook_message_dialog));
		builder.setNegativeButton(getString(R.string.cancel), null);
		builder.setPositiveButton(getString(R.string.login), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				isLoggedIn = true;
				loginWithFacebook();
				
			}
		});
		Dialog dialog = builder.create();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}
	
	
	
	
	private void loginWithFacebook() {

		SimpleFacebookUtils.facebookLogin(this, new CallBack<UserProfile>() {
			@Override
			public void callBack(UserProfile object) {
				if (object != null){
					BMMApplication.setCurrentUserProfile(object);
					Preferences.getInstans(MapActivity.this).setUserId(
							object.getId());
					finish();
					startActivity(getIntent());
				}
			}
		});
	}
	
	

	
	@Override
	public void onInfoWindowClick(Marker pressed) {
		
		String markerDetails = getMarkerDetailsStringByMarkerTitle(pressed.getTitle());
		if (markerDetails.isEmpty()){
			return ;
		}
		
		if (currentFBID != null && !currentFBID.isEmpty() && !currentFBID.equals("*")){
			launchFacebook(currentFBID);
		}else{
			goToBusinessPage();
		}
		
	}
	
	/**
	 * display specify coordinates on the map fragment whit location marker indicator
	 * 
	 * @param lat - location latitude
	 * @param lon - location longitude
	 */
	private void displayCurrentLocationOnMap(String lat, String lon,String toDisplay) {
		
		LatLng currentLocation = new LatLng(Double.parseDouble(lat),
				Double.parseDouble(lon));

		if (googleMap != null) {
			createViewByUser();
		}

		googleMap.getCameraPosition();
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 10));
		
		
	}




	
	@Override
	public boolean onMarkerClick(Marker markerPressed) {

		try {
			String markerDetails = getMarkerDetailsStringByMarkerTitle(markerPressed.getTitle());
			if (markerDetails.isEmpty()){
				nameTv.setText(markerPressed.getTitle());
				addressTv.setVisibility(View.GONE);
				distanceTv.setVisibility(View.GONE);
				toBuisnessPageBtn.setVisibility(View.GONE);
				return false;
			}
			
			String[] detailse = markerDetails.split(Pattern.quote("||"));
			
			String name = detailse[0];

			String address = "אין מידע";
			if (detailse[1] != null) {
				address = detailse[1];
			}
			double lat = Double.parseDouble(detailse[2]);
			double lon = Double.parseDouble(detailse[3]);

			currentBuisnessID = detailse[5];
			currentBuisnessExternalID = detailse[6];
			currentFBID = detailse[9];
			currentCategoryType = Integer.parseInt(detailse[10]);
			
			
			if (currentBuisnessID.equals("*")){
				toBuisnessPageBtn.setVisibility(View.VISIBLE);
				toBuisnessPageBtn.setBackgroundResource(R.drawable.facebook_icon);
				toBuisnessPageBtn.setText("");
			}else{
				toBuisnessPageBtn.setVisibility(View.VISIBLE);
				toBuisnessPageBtn.setBackgroundResource(R.drawable.check_in_selector);
				toBuisnessPageBtn.setText(getString(R.string.to_businnes_page));
				currentFBID = "";
			}

			String distance = findDistanceFromUser(lat, lon);  

			addressTv.setVisibility(View.VISIBLE);
			distanceTv.setVisibility(View.VISIBLE);
			nameTv.setText(name);
			addressTv.setText(address);
			distanceTv.setText(getString(R.string.distanc) + " " + distance
					+ " " + getString(R.string.km));
		} catch (Exception e) {

		}

		return false;
	}
	
	
	private String getMarkerDetailsStringByMarkerTitle (String title){
		
		for (int i = 0; i < currentMarkersDetails.size(); i++) {
			if (currentMarkersDetails.get(i).contains(title)){
				return currentMarkersDetails.get(i);
			}
		}
		return "";
	}
	
	
	
	
	

}
