/**
 * 
 */
package com.wtf.activity.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.wtf.activity.SMSActivity;

/**
 * @author natiapplications
 * 
 */
public class SMSReceiver extends BroadcastReceiver {

	final SmsManager sms = SmsManager.getDefault();

	@Override
	public void onReceive(Context context, Intent intent) {
		final Bundle bundle = intent.getExtras();

		try {

			if (bundle != null) {

				final Object[] pdusObj = (Object[]) bundle.get("pdus");

				for (int i = 0; i < pdusObj.length; i++) {

					SmsMessage currentMessage = SmsMessage
							.createFromPdu((byte[]) pdusObj[i]);
					String phoneNumber = currentMessage
							.getDisplayOriginatingAddress();

					String senderNum = phoneNumber;
					String message = currentMessage.getDisplayMessageBody();
					Log.e("smslog", "sender num - " + senderNum + " body - " + message) ;
					if (message.contains("WTF-MESSAGE:")){
						try {
							Intent msgIntent = new Intent(context,SMSActivity.class);
							msgIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							msgIntent.putExtra(SMSActivity.EXTRA_MESSAGE, message);
							context.startActivity(msgIntent);
						} catch (Exception e) {
							Log.e("smslog", "error: " + e.getMessage()) ;
						}
					}
					
				}
			}

		} catch (Exception e) {
			Log.e("smslog", "error: " + e.getMessage()) ;
			e.printStackTrace();
		}

	}
}
