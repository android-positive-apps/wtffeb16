package com.wtf.profileFragments;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.adapters.CreditCardsAdapter;
import com.wtf.fragments.ProfileFragment;
import com.wtf.objects.CreditCard;

public class ProfileCreditCardsFragment extends Fragment implements OnClickListener,OnItemClickListener {

	public static final String TAG = "ProfileCreditsFragment";
	
	private TextView noCredits;
	private ImageView addCard;
	private ListView mListView;
	private ArrayList<CreditCard> cards;
	
	public static TypedArray cardsIcons;
	private String[] cardsNames;
	private int[] creditCardsId;
	private Dialog addCrditCardDialog;

	
    public ProfileCreditCardsFragment (){
		
	}
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.profile_credit_cards, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mListView = (ListView) view.findViewById(R.id.profile_credit_cards_list);
		addCard = (ImageView)view.findViewById(R.id.add_card);
		noCredits = (TextView)view.findViewById(R.id.no_credits_tv);
		
		addCard.setOnClickListener(this);
		ProfileFragment.showProfilePicture();
		creditCardsId =getResources().getIntArray(R.array.credit_cards_id);
		cardsIcons = getResources().obtainTypedArray(R.array.credit_cards_icons);
		cardsNames = 
				getActivity().getResources().getStringArray(R.array.credit_cards_names);
		
	}
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updateCreditCardsListView();
	}
	
	private void updateCreditCardsListView () {
		cards = BMMApplication.getCards();
		CreditCardsAdapter adapter= new CreditCardsAdapter(getActivity(),cards);
		mListView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		if (cards.size() == 0){
			noCredits.setVisibility(View.VISIBLE);
		}else {
			noCredits.setVisibility(View.GONE);
		}
		
	}

	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		CreditCard creditCard = new CreditCard(cardsNames[position], position);
		creditCard.setId(creditCardsId[position]);
		
		if (isAllradyExist(creditCardsId[position])){
			Toast.makeText(getActivity(), getString(R.string.credit_card_is_exsit), Toast.LENGTH_SHORT).show();
			return;
		}
		BMMApplication.setCards(getActivity(), creditCard);
		addCrditCardDialog.dismiss();
		updateCreditCardsListView();
	}
	
	//exist
	private boolean isAllradyExist (int toCheck) {
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).getId() == toCheck){
				return true;
			}
		}
		return false;
	}

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		showAddCreditCardDialog ();
	}
	
	private void showAddCreditCardDialog () {
		View view = getActivity().getLayoutInflater().inflate
				(R.layout.add_crdit_card_dialog, null);
		GridView creditCardsGirdView = (GridView)view.findViewById(R.id.credits_girds);
		creditCardsGirdView.setOnItemClickListener(this);
		creditCardsGirdView.setAdapter(new AddCreditCardsAdapter());
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.choose_credit_card));
		builder.setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {}
		});
		builder.setView(view);
		addCrditCardDialog = builder.create();
		addCrditCardDialog.show();
	}
	
	
	class AddCreditCardsAdapter extends BaseAdapter {

		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return cardsNames.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return cardsNames[position];
		}

		
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = getActivity().getLayoutInflater().inflate(R.layout.credit_card_gird_item, null);
			
			TextView companyName = (TextView)convertView.findViewById(R.id.company_name);
			ImageView companyImage = (ImageView)convertView.findViewById(R.id.compani_image);
			
			companyName.setText(cardsNames[position]);
			companyImage.setBackgroundDrawable(cardsIcons.getDrawable(position));
			
			return convertView;
		}
		
	}
	
}
