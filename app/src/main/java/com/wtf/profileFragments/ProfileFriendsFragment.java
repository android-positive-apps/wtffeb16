package com.wtf.profileFragments;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;

//import com.facebook.Session;
//import com.facebook.widget.WebDialog;
import com.facebook.internal.WebDialog;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.sromku.simple.fb.listeners.OnInviteListener;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.adapters.GridAdapter;
import com.wtf.dataBase.Preferences;
import com.wtf.dataBase.UserFriendsManager.FriendsCallbac;
import com.wtf.fragments.ProfileFragment;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.SetAppuserBunosPoints;
import com.wtf.utils.GiftUtils;
import com.wtf.utils.SimpleFacebookUtils;


public class ProfileFriendsFragment extends Fragment implements OnItemClickListener{
	
	public static final String TAG = "ProfileFriendsFragment";
	Button mInviteBtn;
	GridView mGridView;
	private GridAdapter adapter;
	
    public ProfileFriendsFragment (){
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.profile_friend, container , false);
	}
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mInviteBtn = (Button) view.findViewById(R.id.Profile_friends_invite_btn);
		mInviteBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (Preferences.getInstans(getActivity()).getFacebookId().equals("0")){
					showFacebookLoginDialog();
				}else{
					if (AccessToken.getCurrentAccessToken() != null){
						invite();
					}else{
						SimpleFacebookUtils.facebookLogin(getActivity(), new CallBack<UserProfile>() {
							@Override
							public void callBack(UserProfile object) {
								invite();
							}
						});
					}
				}



			}
		});
		ProfileFragment.hideProfilePicture();
		mGridView = (GridView) view.findViewById(R.id.profile_friends_grid);
		updateGirdView();
	}


	private void invite(){
		try {
			BMMApplication.mSimpleFacebook.invite(Preferences.getInstans(getActivity()).getName() + "invite you to use in WTF",
					onInviteListener,"https://play.google.com/store/apps/details?id=com.wtf.activity");
		}catch (Exception e){
			Toast.makeText(getActivity(),"We Sorry, Request canceled.\n" +
					"Please try again",Toast.LENGTH_SHORT).show();
		}
	};

	private void updateGirdView () {
		BMMApplication.getUserFacebookFriends(new FriendsCallbac() {
			
			@Override
			public void onFriendsArrived(final ArrayList<FacebookFriand> result) {
				getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						List<FacebookFriand> pictures = new ArrayList<FacebookFriand>();
						for (int i = 0; i < result.size(); i++) {
							pictures.add(result.get(i));
						}
						adapter = new GridAdapter(getActivity(), pictures);
						mGridView.setAdapter(adapter);
						mGridView.setOnItemClickListener(ProfileFriendsFragment.this);
					}
				});
			}
		});
		
		
		
	}
	
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		FacebookFriand temp = adapter.getItem(arg2);
		if (temp != null){
			String fbid = adapter.getItem(arg2).getFBID();
			if (fbid != null && !fbid.isEmpty()){
				launchFacebook(fbid);
			}
		}
		
	}
	
	public final void launchFacebook(String id) {
        final String urlFb = "fb://profile/"+id;
        Intent intent = new Intent(Intent.ACTION_VIEW);
       
        final String urlBrowser = "https://www.facebook.com/"+id;
        intent.setData(Uri.parse(urlBrowser));
        startActivity(intent);
    }
	
	OnInviteListener onInviteListener = new OnInviteListener() {
	    @Override
	    public void onComplete(List<String> invitedFriends, String requestId) { 
	    	
	    	int points = GiftUtils.SHARE_POINT.value * invitedFriends.size();
	    	SetAppuserBunosPoints setAppuserBunosPoints = new SetAppuserBunosPoints(getActivity(),
					points ,
					GiftUtils.SHARE_POINT.actionName);
			setAppuserBunosPoints.execute();
			BMMApplication.getCurrentUserProfile().setPoints(BMMApplication.getCurrentUserProfile().getPoints() +
					points);
	    }
	    @Override
	    public void onCancel() {
	    	Log.e(TAG, "cancel");
	    }
		@Override
		public void onException(Throwable arg0) {
			arg0.printStackTrace();

			Log.e(TAG, "ex: " + arg0.getMessage());
		}
		@Override
		public void onFail(String arg0) {
			Log.e(TAG, "fail + " + arg0);}
	};
	
	private void showFacebookLoginDialog (){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Error");
		builder.setMessage("You are not logged in with Facebook. Click to login");
		builder.setNegativeButton("cancel", null);
		builder.setPositiveButton("login", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SimpleFacebookUtils.facebookLogin(getActivity(), new CallBack<UserProfile>() {
					@Override
					public void callBack(UserProfile object) {

						SimpleFacebookUtils.setUserFacebookFriends(new OnFriendsListener() {
							@Override
							public void onComplete(List<Profile> response) {
								super.onComplete(response);
								updateGirdView();
								invite();
							}
						});

					}
				});
			}
		});
		Dialog dialog = builder.create();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}
	
	/*
	 * 
	 * listen to login state. when the user login success the onLogin method implement call the
	 * getProfile method from the facebook manager for get the user data
	 * */
	OnLoginListener onLoginListener = new OnLoginListener() {
		@Override
		public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
			PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
			pictureAttributes.setHeight(250);
			pictureAttributes.setWidth(250);
			pictureAttributes.setType(PictureType.SQUARE);

			Profile.Properties properties = new Profile.Properties.Builder()
					.add(Properties.ID)
					.add(Properties.FIRST_NAME)
					.add(Properties.LAST_NAME)
					.add(Properties.EMAIL)
					.add(Properties.PICTURE,pictureAttributes)
					.build();
			BMMApplication.mSimpleFacebook.getProfile(properties,onProfileListener);
			try {

				BMMApplication.mSimpleFacebook.invite(Preferences.getInstans(getActivity()).getName() + "invite you to use in " +
						getString(R.string.app_name),
						onInviteListener,"https://play.google.com/store/apps/details?id=" + getString(R.string.app_package_name));

				//sendRequestDialog();
			} catch (Exception e) {
				Toast.makeText(getActivity(),"We Sorry, Request canceled.\n" +
						"Please try again",Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void onCancel() {}
		@Override
		public void onException(Throwable throwable) {}
		@Override
		public void onFail(String reason) {}

	};
	
	
	
	/*
	 * 
	 * listen to the get profile request. when the request done, the onComplete method handles the
	 * user data that accepted
	 * */
	OnProfileListener onProfileListener = new OnProfileListener() {
		@Override
		public void onComplete(Profile profile) {

			if (profile != null) {
				// get user data
				String id = profile.getId();
				String image = profile.getPicture();
				Preferences.getInstans(getActivity()).setFacebookId(id);
				Preferences.getInstans(getActivity()).setFacebookImageProfile(image);
				ProfileFragment.setPorfilePic(getActivity());
				PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
				pictureAttributes.setHeight(250);
				pictureAttributes.setWidth(250);
				pictureAttributes.setType(PictureType.SQUARE);
				
		    	Profile.Properties properties = new Profile.Properties.Builder()
			    .add(Properties.ID)
			    .add(Properties.FIRST_NAME)
			    .add(Properties.LAST_NAME)
			    .add(Properties.EMAIL)
			    .add(Properties.PICTURE,pictureAttributes)
			    .build();
				BMMApplication.mSimpleFacebook.getFriends(properties,new OnFriendsListener() {
					
					public void onComplete(java.util.List<Profile> response) {
						ArrayList<FacebookFriand> friends = new ArrayList<FacebookFriand>();
		    			for (int i = 0; i < response.size(); i++) {
							FacebookFriand temp = new FacebookFriand(response.get(i));
							friends.add(temp);
						}
		    			BMMApplication.setUserFacebookFriends(friends);
		    			updateGirdView();
					};
					
				});
				
			}
		}

	};
	
	/*private void sendRequestDialog() throws Exception{
	    
		
		
		Bundle parameters = new Bundle();
		parameters.putString("message", Preferences.getInstans(getActivity()).getName() + "invite you to use in WTF");
		parameters.putString("link", "https://play.google.com/store/apps/details?id=com.wtf.activity");
		WebDialog.Builder builder = new WebDialog.Builder(getActivity(), Session.getActiveSession(),
		                                "apprequests", parameters);

		builder.setOnCompleteListener(new WebDialog.OnCompleteListener() {

		    @Override
		    public void onComplete(Bundle values, FacebookException error) {
		        if (error != null){
		            if (error instanceof FacebookOperationCanceledException){
		                Toast.makeText(getActivity(),"Request cancelled",Toast.LENGTH_SHORT).show();
		            }
		            else{
		                Toast.makeText(getActivity(),"Network Error",Toast.LENGTH_SHORT).show();
		            }
		        }
		        else{

		            final String requestId = values.getString("request");
		            if (requestId != null) {
		                Toast.makeText(getActivity(),"Request sent",Toast.LENGTH_SHORT).show();
		            } 
		            else {
		                Toast.makeText(getActivity(),"Request cancelled",Toast.LENGTH_SHORT).show();
		            }
		        }                       
		    }
		});

		WebDialog webDialog = builder.build();
		webDialog.show();
	}*/
	
	
	
	
}
