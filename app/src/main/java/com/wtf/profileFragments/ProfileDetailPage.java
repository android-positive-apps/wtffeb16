package com.wtf.profileFragments;

import java.io.UnsupportedEncodingException;

import org.apache.http.protocol.HTTP;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.customView.CircleImageView;
import com.wtf.dataBase.Preferences;
import com.wtf.fragments.ProfileFragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ProfileDetailPage extends Fragment{
	
	public static final String TAG = "ProfileDetailPage";
	
	private TextView mDateTv;
	private TextView userName;
	private TextView score;
	
    public ProfileDetailPage (){
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.profile_detaile_page, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		ProfileFragment.showProfilePicture();
		userName = (TextView) view.findViewById(R.id.profile_details_name_tv);
		score = (TextView) view.findViewById(R.id.score_tv);
		mDateTv = (TextView) view.findViewById(R.id.profile_details_date_tv);
		
		String date = Preferences.getInstans(getActivity()).getDateRegistration();
		Preferences.getInstans(getActivity()).setDateRegistration(date);
		
		mDateTv.setText(BMMApplication.getCurrentUserProfile().getCreatedAt());
		userName.setText(Preferences.getInstans(getActivity()).getName());
		score.setText(BMMApplication.getCurrentUserProfile().getPoints() + "");
	}

}
