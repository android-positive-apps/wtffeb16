package com.wtf.profileFragments;

import java.util.ArrayList;
import java.util.List;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.adapters.RatingAdapter;
import com.wtf.fragments.ProfileFragment;
import com.wtf.objects.Rating;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class ProfileRatingFragment extends Fragment {

	public static final String TAG = "ProfileRatingFragment";
	private ListView mList;
	private ArrayList<Rating> ratings;
	private TextView noRatings;
	
    public ProfileRatingFragment (){
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.profile_ratings, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		ProfileFragment.showProfilePicture();
		noRatings = (TextView)view.findViewById(R.id.no_ratings_tv);
		mList = (ListView) view.findViewById(R.id.profile_ratings_list);
		
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updateRatingsListView();
	}
	
	private void updateRatingsListView () {
		
		ratings = BMMApplication.getRatings();
		mList.setAdapter(new RatingAdapter(getActivity() , ratings ));
		if (ratings.size() == 0){
			noRatings.setVisibility(View.VISIBLE);
		}else {
			noRatings.setVisibility(View.GONE);
		}
	}
}
