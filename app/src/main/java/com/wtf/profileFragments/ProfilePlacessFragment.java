package com.wtf.profileFragments;

import java.util.ArrayList;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.images.ImageManager.OnImageLoadedListener;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.adapters.PlacesAdapter;
import com.wtf.fragments.BusinessFragment;
import com.wtf.fragments.BusinessPagerFragment;
import com.wtf.fragments.ProfileFragment;
import com.wtf.objects.Business;
import com.wtf.objects.Place;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetBusinnesById;

public class ProfilePlacessFragment extends Fragment implements OnItemClickListener {

	public static final String TAG = "ProfilePlacessFragment";
	
	private ListView mListView;
	private ArrayList<Place> places;
	private PlacesAdapter adapter;
	private TextView noPlaces;

	
    public ProfilePlacessFragment (){
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.profile_placess, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ProfileFragment.showProfilePicture();
		mListView = (ListView) view.findViewById(R.id.profile_places_list);
		noPlaces = (TextView)view.findViewById(R.id.no_places_tv);
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updatePlaceseListView();
	}
	private void updatePlaceseListView (){
		places = BMMApplication.getPlaces();
		adapter = new PlacesAdapter(getActivity(), places );
		mListView.setAdapter(adapter);
		if (places.size() == 0){
			noPlaces.setVisibility(View.VISIBLE);
		}else {
			noPlaces.setVisibility(View.GONE);
		}
		mListView.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		final Place selected = (Place) adapter.getItem(position);

		GetBusinnesById getBusinnesById = new GetBusinnesById(getActivity(),
				selected.getBusinessID(), selected.getBusinessExternalID(),
				new CallBack<Business>() {

					@Override
					public void callBack(Business object) {
						object.setCategoryType(selected.getCategoryType());
						getFragmentManager().beginTransaction()
						.replace(R.id.container, BusinessFragment.newInstance(0, object,true)  ,
								BusinessFragment.TAG).addToBackStack(null).commit();
						
					}
				});
		getBusinnesById.execute();
	}
	
	
}
