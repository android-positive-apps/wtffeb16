/**
 * 
 */
package com.wtf.fragments;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.location.LocationUtilis;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.CreateUpdateUserTask;
import com.wtf.serverTask.GetAppuserReceivedGifts;
import com.wtf.serverTask.GetUserGifts;
import com.wtf.serverTask.LoginTask;
import com.wtf.utils.GeneralUtills;
import com.wtf.utils.SimpleFacebookUtils;

/**
 * @author Nati Gabay
 *
 */
public class LoginFragment extends Fragment implements OnClickListener{

	public static final int NEW_USER = 0;

	private EditText mEmailEt, mFirstNameEt, mAboutEt, mLastName, mPhone;
	private Button mLoginBtn,singUpBtn,mFacebookLogin;


	
	public LoginFragment (){
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.login_fragment, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		MainActivity.hideTopButtons();
		mLoginBtn = (Button) view.findViewById(R.id.login);
		singUpBtn = (Button) view.findViewById(R.id.sing_up);
		mFacebookLogin = (Button) view.findViewById(R.id.facebook_login);
		mLoginBtn.setOnClickListener(this);
		singUpBtn.setOnClickListener(this);
		mFacebookLogin.setOnClickListener(this);
	}
	
	
	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.login:
			if (Preferences.getInstans(getActivity()).getUserId().equals("0")){
				final ProgressDialog dialog = new ProgressDialog(getActivity());
				dialog.setMessage("Loging in..");
				dialog.show();
				getAppUserProfile(dialog);
			}else{
				getFragmentManager().beginTransaction().add(R.id.container,new MainMenuFragment())
				.remove(LoginFragment.this).commit();
			}
			break;
		case R.id.sing_up:
			
			break;
		case R.id.facebook_login:
			loginWithFacebook();
			break;
		}
		
	}
	
	private void getAppUserProfile(final ProgressDialog dialog) {
		new LoginTask(getActivity(), new CallBack<UserProfile>() {

			@Override
			public void callBack(UserProfile object) {
				dialog.dismiss();
				
				if (object == null){
					return;
				}
				BMMApplication.setCurrentUserProfile(object);
				Preferences.getInstans(getActivity()).setUserId(object.getId());
				
				getFragmentManager().beginTransaction().add(R.id.container,new MainMenuFragment())
				.remove(LoginFragment.this).commit();
			}
		}).execute();
		new GetUserGifts(getActivity(), 1, null).execute();
		new GetAppuserReceivedGifts(getActivity(), 1, null).execute();
	}
	
	
	private void loginWithFacebook () {
		
		SimpleFacebookUtils.facebookLogin(getActivity(), new CallBack<UserProfile>() {
					@Override
					public void callBack(UserProfile object) {
						
						if (object == null){
							final ProgressDialog dialog = new ProgressDialog(getActivity());
							dialog.setMessage("Loging in..");
							dialog.show();
							getAppUserProfile(dialog); 
						}else{
							BMMApplication.setCurrentUserProfile(object);
							Preferences.getInstans(getActivity()).setUserId(
									object.getId());
							getFragmentManager().beginTransaction().add(R.id.container,new MainMenuFragment())
										.remove(LoginFragment.this).commit();
						}
						
					}
				} );  
	}

	
	
	
	
	/********************
	 * dialogs
	 ********************/
	
	/*private void showLoginDialog () {
		
		
		View loginView = getActivity().getLayoutInflater().inflate(R.layout.login_dialog, null);
		final EditText email, password;
		email = (EditText) loginView.findViewById(R.id.email);
		password = (EditText) loginView.findViewById(R.id.password);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Login");
		builder.setView(loginView);
		builder.setNegativeButton("cancel", null);
		builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String userEmail = email.getText().toString();
				String userPassword = password.getText().toString();
				
			}
		});
		builder.create().show();
	}*/
	
	/*private void showSignUpDialog () {
		
		View singUpview = getActivity().getLayoutInflater().inflate(R.layout.sing_up_dialog, null);
		final EditText mEmailEt, mFirstNameEt, mAboutEt, mLastName, mPhone;
		mAboutEt = (EditText) singUpview.findViewById(R.id.about_me);
		mEmailEt = (EditText) singUpview.findViewById(R.id.email);
		mFirstNameEt = (EditText) singUpview.findViewById(R.id.first_name);
		mLastName = (EditText) singUpview.findViewById(R.id.last_name);
		mPhone = (EditText) singUpview.findViewById(R.id.phone_number);
		
		mEmailEt.setText(GeneralUtills.getUserEmail(getActivity()));
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Sing Up");
		builder.setView(singUpview);
		builder.setNegativeButton("cancel", null);
		builder.setPositiveButton("Sing Up", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				sendUpdateAppUser(mFirstNameEt.getText()
						.toString(), mLastName.getText().toString(), mPhone
						.getText().toString(), mEmailEt.getText().toString(),
						mAboutEt.getText().toString(), "" ); 
			}
		});
		builder.create().show();
		
	}*/


	
	
	
	
	
	
	

}
