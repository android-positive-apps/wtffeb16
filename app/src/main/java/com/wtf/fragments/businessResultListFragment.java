/**
 * 
 */
package com.wtf.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Picasso;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.adapters.BuisnessView;
import com.wtf.adapters.BuisnessView.OnBuisnessClickListener;
import com.wtf.customView.MyRatingBar;
import com.wtf.objects.Business;

/**
 * @author natiapplications
 *
 */
public class businessResultListFragment extends Fragment implements OnBuisnessClickListener {

	public static final String TAG = "businessResultListFragment";
	
	
	private ImageView mBackgroundImg;
	private LinearLayout mBuisnesessContainer;

	
	private ArrayList<Business> currentBusinesses = new ArrayList<Business>();
	public static TypedArray categoryDeafultIcons;

	
	
	public static businessResultListFragment newInstance
								(ArrayList<Business> businesses){
		businessResultListFragment instance = new businessResultListFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MainActivity.EXTERA_BUSINESS, businesses);
		instance.setArguments(bundle);
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (currentBusinesses != null){
			currentBusinesses.clear();
			currentBusinesses.addAll
			((ArrayList<Business>)getArguments().
					getSerializable(MainActivity.EXTERA_BUSINESS));
		}
		return inflater.inflate(R.layout.business_result_list_fragmetn, container,
				false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mBackgroundImg = (ImageView) view
				.findViewById(R.id.screen_bg);
		mBuisnesessContainer = (LinearLayout)view.findViewById(R.id.businesessContainer);
		
		categoryDeafultIcons = getResources().obtainTypedArray(R.array.deafults_icons_b);
		
		mBackgroundImg.setBackgroundDrawable(MainMenuFragment.currentBG);
		MainActivity.showTopButtonsLocationAndSettings();
		
		initBusinesses();
		
	}
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MainActivity.hideTopButtons();
	}
	
	private void initBusinesses () {
		
		for (int i = 0; i < currentBusinesses.size(); i++) {
			int side = BuisnessView.SIDE_LEFT;
			if (i > 0 && i%2 > 0){
				side = BuisnessView.SIDE_RIGHT;
			}
			
			BuisnessView buisnessView = new BuisnessView(currentBusinesses.get(i), side, i, this);
			mBuisnesessContainer.addView(buisnessView.getView());
		}
	}
	
	

	
	@Override
	public void onBuisnessClick(View v, Business data,int index) {
		// TODO Auto-generated method stub
		
		BMMApplication.currentCatgoryIcon = data.getCategoryType();
		
		getFragmentManager().beginTransaction()
		.replace(R.id.container, BusinessPagerFragment.newInstance
				(index,currentBusinesses) 
		, BusinessPagerFragment.TAG).addToBackStack(null).commit();
		
	}
}

