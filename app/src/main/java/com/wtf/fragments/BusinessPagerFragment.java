package com.wtf.fragments;

import java.util.ArrayList;
import java.util.List;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.adapters.DepthPageTransformer;
import com.wtf.adapters.ScreenSlidePagerAdapter;
import com.wtf.adapters.ZoomOutPageTransformer;
import com.wtf.businessFragments.BusinessPage2;
import com.wtf.objects.Business;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class BusinessPagerFragment extends Fragment {

	public static final String TAG = "BusinessPageFragment";
	
	private ViewPager mPager;
	
	private PagerAdapter mPagerAdapter;
	
	private int mPosition = 0;
	private ArrayList<Business> mBusinesses;
	
   
	public static BusinessPagerFragment newInstance(int pos,
			ArrayList<Business> businesses){
		BusinessPagerFragment instance = new BusinessPagerFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MainActivity.EXTERA_BUSINESS, businesses);
		bundle.putInt(MainActivity.EXTERA_POSITION, pos);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.mBusinesses = (ArrayList<Business>)getArguments().getSerializable(MainActivity.EXTERA_BUSINESS);
		this.mPosition = getArguments().getInt(MainActivity.EXTERA_POSITION);
		return inflater.inflate(R.layout.fragment_business_pager, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// Instantiate a ViewPager and a PagerAdapter
        mPager = (ViewPager) view.findViewById(R.id.business_page_pager);
        mPagerAdapter = new ScreenSlidePagerAdapter( getFragmentManager(),mBusinesses);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				
				Log.d(TAG, "onPageSelected " + arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
				Log.d(TAG, "onPageScrolled " + arg0 + " " + arg1 + " " + arg2);
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				
				Log.d(TAG, "onPageScrollStateChanged " + arg0); 
			}
		});
        
        mPager.setCurrentItem(mPosition);
       
	}
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		BMMApplication.setBuisnessGiftsAndCoupons(null);
	}

}
