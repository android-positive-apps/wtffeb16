package com.wtf.fragments;

import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.serverTask.UpdateUserEnabledLocation;
import com.wtf.serverTask.UpdateUserRadius;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingFragment extends Fragment implements OnClickListener {

	public static final String TAG = "SettingFragment";

	private Button mMinusBtn, mPluseBtn, mLocationBtn,mFacebookLogOutBtn;
	
	private FrameLayout mAboutBtn,mExplenationBtn,mPrivacyPolicy; 

	private EditText mEditText;
	private TextView mYesTv, mNoTv;
	private ImageView mIndicator;
	private RelativeLayout.LayoutParams params;
	
	private Preferences mPref;
	private int mRadius;
	private boolean mShowLoaction;

	

	
	public SettingFragment (){
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_settings, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mPref = Preferences.getInstans(getActivity());

		mIndicator = (ImageView) view
				.findViewById(R.id.settings_location_btn_indicator);
		mAboutBtn = (FrameLayout) view.findViewById(R.id.about_btn);
		mExplenationBtn = (FrameLayout) view
				.findViewById(R.id.expletion_btn);
		mPrivacyPolicy = (FrameLayout)view.findViewById(R.id.policy_btn);
		//mPrivacyPolicy.setVisibility(View.GONE);
		mLocationBtn = (Button) view.findViewById(R.id.settings_location_btn);
		mMinusBtn = (Button) view.findViewById(R.id.settings_minus_btn);
		mPluseBtn = (Button) view.findViewById(R.id.settings_pluse_btn);

		mEditText = (EditText) view.findViewById(R.id.settings_edit_txt);
		mYesTv = (TextView) view.findViewById(R.id.settings_yes_tv);
		mNoTv = (TextView) view.findViewById(R.id.settings_no_tv);
		mFacebookLogOutBtn = (Button) view.findViewById(R.id.setting_facebook_logout_btn);
		
		if (Preferences.getInstans(getActivity()).getFacebookId().equals("0")){
			mFacebookLogOutBtn.setVisibility(View.GONE);
		}

		mAboutBtn.setOnClickListener(this);
		mExplenationBtn.setOnClickListener(this);
		mLocationBtn.setOnClickListener(this);
		mMinusBtn.setOnClickListener(this);
		mPluseBtn.setOnClickListener(this);
		mFacebookLogOutBtn.setOnClickListener(this);
		mPrivacyPolicy.setOnClickListener(this);

		MainActivity.showTopButtonsLocation();

		mEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				try {
					mRadius = Integer.parseInt(s.toString());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mPref.setRadius(mRadius);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		loadData();
	}

	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MainActivity.hideTopButtons();
		int radius = this.mPref.getRadius();
		new UpdateUserRadius(getActivity(), radius).execute();
		new UpdateUserEnabledLocation(getActivity()).execute(); 
	}
	private void loadData() {

		mRadius = mPref.getRadius();
		mShowLoaction = mPref.isShowLocation();

		params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);

		if (mShowLoaction) {
			mYesTv.setTextColor(Color.BLUE);
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			mIndicator.setLayoutParams(params);
		} else {
			mNoTv.setTextColor(Color.BLUE);
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			mIndicator.setLayoutParams(params);
		}

		mEditText.setText("" + mRadius);
	}

	@Override
	public void onClick(View v) {
		
		String link = "";
		params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);

		switch (v.getId()) {
		case R.id.about_btn:
			link = getString(R.string.terms_of_use_link);
			getFragmentManager().beginTransaction().replace
			(R.id.container, new NewAboutFragmetn(NewAboutFragmetn.TYPE_ABOUT,link)).addToBackStack(null)
			.commit();
			/*getFragmentManager().beginTransaction().replace
					(R.id.container, new AboutFragmetn(AboutFragmetn.TYPE_ABOUT)).addToBackStack(null)
					.commit();*/
			break;
		case R.id.expletion_btn:
			link = getString(R.string.about_app_link);
			getFragmentManager().beginTransaction().replace
			(R.id.container, new NewAboutFragmetn(NewAboutFragmetn.TYPE_EXPLATION,link)).addToBackStack(null)
			.commit();
			/*getFragmentManager().beginTransaction().replace
			(R.id.container, new AboutFragmetn(AboutFragmetn.TYPE_EXPLATION)).addToBackStack(null)
			.commit();*/
			break;
		case R.id.policy_btn:
			link = getString(R.string.privacy_policy_link);
			getFragmentManager().beginTransaction().replace
			(R.id.container, new NewAboutFragmetn(NewAboutFragmetn.TYPE_PRIVACY_POLICY,link)).addToBackStack(null)
			.commit();
			break;
		case R.id.settings_location_btn:
			if (mShowLoaction) {
				mShowLoaction = false;
				mNoTv.setTextColor(Color.BLUE);
				mYesTv.setTextColor(Color.BLACK);
				params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				mIndicator.setLayoutParams(params);
				mPref.setShowLocation(mShowLoaction);
			} else {
				mShowLoaction = true;
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				mIndicator.setLayoutParams(params);
				mNoTv.setTextColor(Color.BLACK);
				mYesTv.setTextColor(Color.BLUE);
				mPref.setShowLocation(mShowLoaction);
			}
			break;
		case R.id.settings_minus_btn:
			if(mRadius == 1){
				return;
			}
			mRadius--;
			mPref.setRadius(mRadius);
			mEditText.setText("" + mRadius);
			break;
		case R.id.settings_pluse_btn:
			mRadius++;
			mPref.setRadius(mRadius);
			mEditText.setText("" + mRadius);
			break;
		case R.id.setting_facebook_logout_btn:
			BMMApplication.mSimpleFacebook.logout(new OnLogoutListener() {
				@Override
				public void onLogout() {
					Preferences.getInstans(getActivity()).setFacebookId(null);
					getActivity().finish();
					getActivity().startActivity(getActivity().getIntent());
				}
				
				/*@Override
				public void onFail(String arg0) {}
				
				@Override
				public void onException(Throwable arg0) {}
				
				@Override
				public void onThinking() {}
				
				@Override
				public void onLogout() {
					Preferences.getInstans(getActivity()).setFacebookId(null);
					getActivity().finish();
					getActivity().startActivity(getActivity().getIntent());
				}*/
			});
			break;
		}
	}
}
