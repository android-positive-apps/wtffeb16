package com.wtf.fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.objects.Business;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetBezeqTask;
import com.wtf.serverTask.SearchBuisnessTask;

public class SearchFragment extends Fragment implements OnClickListener {

	public static final String TAG = "SearchFragment";
	
	public static final int CATEGORY_FODD = 0;
	public static final int CATEGORY_SPORT = 1;
	public static final int CATEGORY_FATION = 2;
	public static final int CATEGORY_NIGHT = 3;
	public static final int CATEGORY_TIME = 4;
	
	public static int[] categoryIcons = {R.drawable.food_icon,R.drawable.sport_icon,
		R.drawable.fashion_icon,R.drawable.night_icon,R.drawable.camera_icon};
	

	private ImageView mBackgroundImg;

	private ImageView mFoodBtn, mSportBtn, mFationBtn, mNightBtn, mSpareTimeBtn,
			mSearchBtn;

	private CallBack<JSONObject> mCallBack;

	private GetBezeqTask mTask;

	
	public SearchFragment (){
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.main_serch_fragment, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mBackgroundImg = (ImageView) view
				.findViewById(R.id.search_background_img);
		mFationBtn = (ImageView) view.findViewById(R.id.search_fation);
		mFoodBtn = (ImageView) view.findViewById(R.id.search_food);
		mNightBtn = (ImageView) view.findViewById(R.id.search_night);
		mSearchBtn = (ImageView) view.findViewById(R.id.search_search);
		mSpareTimeBtn = (ImageView) view.findViewById(R.id.search_spare_time);
		mSportBtn = (ImageView) view.findViewById(R.id.search_sport);

		mFationBtn.setOnClickListener(this);
		mFoodBtn.setOnClickListener(this);
		mNightBtn.setOnClickListener(this);
		mSearchBtn.setOnClickListener(this);
		mSpareTimeBtn.setOnClickListener(this);
		mSportBtn.setOnClickListener(this);

		mBackgroundImg.setBackgroundDrawable(MainMenuFragment.currentBG);
		MainActivity.showTopButtonsLocationAndSettings();

		initCallback();

	}
 
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MainActivity.hideTopButtons();
	}
	private void initCallback() {

		mCallBack = new CallBack<JSONObject>() {

			@Override
			public void callBack(JSONObject object) {
				ArrayList<Business> businesses = new ArrayList<Business>();
				try {
					JSONArray data = object.getJSONArray("data");
					for (int i = data.length()-1; i > 0; i--) {
						businesses.add(new Business(data.getJSONObject(i)));
						if (i == data.length() - 50){
							break;
						}
					}
					
					getFragmentManager()
					.beginTransaction()
					.replace(R.id.container, businessResultListFragment.newInstance(businesses),
							businessResultListFragment.TAG).addToBackStack(null)
					.commit();

				} catch (JSONException e) {
					e.printStackTrace();
				}
				
			}
		};
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.search_food:
			BMMApplication.currentCatgoryIcon = CATEGORY_FODD;
			mTask = new GetBezeqTask(getActivity(), mCallBack,
					GetBezeqTask.TypeFood);
			mTask.execute();
			
			break;
		case R.id.search_fation:
			BMMApplication.currentCatgoryIcon = CATEGORY_FATION;
			mTask = new GetBezeqTask(getActivity(), mCallBack,
					GetBezeqTask.TypeFation);
			mTask.execute();
			break;
		case R.id.search_night:
			BMMApplication.currentCatgoryIcon = CATEGORY_NIGHT;
			mTask = new GetBezeqTask(getActivity(), mCallBack,
					GetBezeqTask.TypeNight);
			mTask.execute();
			break;
		case R.id.search_spare_time:
			BMMApplication.currentCatgoryIcon = CATEGORY_TIME;
			mTask = new GetBezeqTask(getActivity(), mCallBack,
					GetBezeqTask.TypeFreeTime);
			mTask.execute();
			break;
		case R.id.search_sport:
			BMMApplication.currentCatgoryIcon = CATEGORY_SPORT;
			mTask = new GetBezeqTask(getActivity(), mCallBack,
					GetBezeqTask.TypeSport);
			mTask.execute();
			break;
		case R.id.search_search:
			/*getFragmentManager()
					.beginTransaction()
					.replace(R.id.container, new MainMenuFragment(),
							MainMenuFragment.TAG).addToBackStack(null).commit();*/
			showSearchDialog ();
			break;
		}

	}
	
	private EditText searchEt;
	
	/**
	 * 
	 */
	private void showSearchDialog() {
		View view = getActivity().getLayoutInflater().inflate(R.layout.search_dialog, null);
		searchEt = (EditText)view.findViewById(R.id.search_et);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(view);
		builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
		});
		builder.setPositiveButton(getString(R.string.search), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String keyWord = searchEt.getText().toString();
				SearchBuisnessTask task = new SearchBuisnessTask(getActivity(), keyWord, new CallBack<JSONObject>() {
					@Override
					public void callBack(JSONObject object) {
						
						ArrayList<Business> businesses = new ArrayList<Business>();
						try {
							JSONObject jsonObject =object.optJSONObject("data");
							JSONArray data = jsonObject.getJSONArray("Businesses");
							for (int i = 0; i < data.length(); i++) {
								businesses.add(new Business(data.getJSONObject(i)));
								if (i == 50){
									break;
								}
							}
							getFragmentManager()
							.beginTransaction()
							.replace(R.id.container, businessResultListFragment.newInstance(businesses),
									businessResultListFragment.TAG).addToBackStack(null)
							.commit();

						} catch (JSONException e) {
							e.printStackTrace();
						}
						
						
						if (businesses.size() == 0){
							Toast.makeText(getActivity(), "No result found !", Toast.LENGTH_SHORT).show();
							return;
						}
						
					}
				});
				task.execute();
			}
		});
		builder.create().show();
		
	}
	@Override
	public void onPause() {
		super.onPause();
		if (mTask != null) {
			mTask.cancel(true);
		}
	}
	
	

}
