/**
 * 
 */
package com.wtf.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wtf.activity.R;

/**
 * @author natiapplications
 *
 */
public class NewAboutFragmetn extends Fragment{

	public static final String TAG = "BusinessPage1";
	
	public static final int TYPE_EXPLATION = 1;
	public static final int TYPE_ABOUT = 2;
	public static final int TYPE_PRIVACY_POLICY =3;
	
	private int type ;
	private String link;
	
	private TextView title;
	private WebView contentWeb;
	private ProgressBar loadPagePb;
	
	
	public NewAboutFragmetn (){
		
	}
	public NewAboutFragmetn(int type,String link) {
		this.link = link;
		this.type = type;
		Log.e("linklog", "link : " + link);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.new_about_fragment, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		title = (TextView)view.findViewById(R.id.about_title);
		loadPagePb = (ProgressBar)view.findViewById(R.id.load_page_pb);
		loadPagePb.setVisibility(View.VISIBLE);
		
		contentWeb = (WebView)view.findViewById(R.id.content_web);
		switch (type) {
		case TYPE_ABOUT:
			title.setText(getString(R.string.about_the_app));
			break;
		case TYPE_EXPLATION:
			title.setText(getString(R.string.explenation_about_the_app));
			break;
		case TYPE_PRIVACY_POLICY:
			title.setText(getString(R.string.privacy_policy));
			break;
		}
		
		
		String docsLink = "https://docs.google.com/gview?embedded=true&url=" + link;
		link = docsLink;
		setupWebView();
		//openBrowser(this, link);
	}
	
	
	
	private void setupWebView() {
		
		//contentWeb.getSettings().setBuiltInZoomControls(true);
		//contentWeb.requestFocus();
		contentWeb.getSettings().setJavaScriptEnabled(true);
		//contentWeb.getSettings().setUseWideViewPort(true);
		//contentWeb.getSettings().setLoadsImagesAutomatically(true);
		
		contentWeb.loadUrl(link);
		
		contentWeb.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {
			        loadPagePb.setVisibility(View.GONE);
			    }
			   
			   @Override
			      public boolean shouldOverrideUrlLoading(WebView view, String url) {
			         view.loadUrl(url);
			         return true;
			      }
		});
		
	}
	
	
	public static void openBrowser (Fragment fragment,String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		fragment.getActivity().startActivity(browserIntent);
	}
	
}