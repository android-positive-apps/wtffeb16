package com.wtf.fragments;

import java.util.ArrayList;
import java.util.Calendar;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.GiftsFragment;
import com.wtf.activity.MainActivity;
import com.wtf.activity.MapActivity;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.location.LocationUtilis;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetAppuserReceivedGifts;
import com.wtf.serverTask.GetUserGifts;
import com.wtf.serverTask.LoginTask;
import com.wtf.utils.ServerTaskUtills;

public class MainMenuFragment extends Fragment implements View.OnClickListener {

	public static final String TAG = "MainMenuFragment";

	private ImageView mBackgroundImg;

	private Button mSearchBtn, mScanBtn, mGiftBtn, mSettingBtn, mProfileBtn;
	
	public static  TypedArray nigteBgImages;
	public static TypedArray dayBgImages;
	public static Drawable currentBG;
	 private AdView adView1;
	 private AdView adView2;
	
    public MainMenuFragment (){
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.main_menu_fragment, container, false);
	}

	@SuppressLint("Recycle")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		MainActivity.showTopButtonsLocationAndSettings();
		nigteBgImages = getResources().obtainTypedArray(R.array.nigte_bg);
		dayBgImages = getResources().obtainTypedArray(R.array.day_bg);
		
		mBackgroundImg = (ImageView) view
				.findViewById(R.id.main_menu_background_img);
		mGiftBtn = (Button) view.findViewById(R.id.main_menu_gifts);
		mScanBtn = (Button) view.findViewById(R.id.main_menu_scan);
		mSearchBtn = (Button) view.findViewById(R.id.main_menu_search);
		mSettingBtn = (Button) view.findViewById(R.id.main_menu_settings);
		mProfileBtn = (Button) view.findViewById(R.id.main_menu_profile);

		mGiftBtn.setOnClickListener(this);
		mScanBtn.setOnClickListener(this);
		mSearchBtn.setOnClickListener(this);
		mSettingBtn.setOnClickListener(this);
		mProfileBtn.setOnClickListener(this);
		
		//configureAddViews(view);

		if (BMMApplication.getCurrentUserProfile() == null){
			
			final ProgressDialog dialog = new ProgressDialog(getActivity());
			dialog.setMessage(getString(R.string.loading));
			dialog.show();
			getAppUserProfile(dialog);
			getAppuserFriandsList();
		}
		Log.e("GoogleApiLocationLog", "check for location before");
		LocationUtilis.startLocationService(BMMApplication
				.getAppContext());
		ServerTaskUtills.startLocationTransmiterService();
		Log.e("GoogleApiLocationLog", "check for location after");
		
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		configureAddViews(getView());
		
	}
	
	private void configureAddViews(View view) {
		adView1 = (AdView) getView().findViewById(R.id.adView1);
		AdRequest request1 = new AdRequest.Builder().addTestDevice
        ("F19026D69207709A1E81A381630E697E").build();
	    adView1.loadAd(request1);
	    
		adView2 = (AdView) view.findViewById(R.id.adView2);
		AdRequest adRequest2 = new AdRequest.Builder().addTestDevice(
				"F19026D69207709A1E81A381630E697E").build();
		adView2.loadAd(adRequest2);
	}
	
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		adView1.destroy();
		adView2.destroy();
		MainActivity.hideTopButtons();
	}
	
	private void getAppUserProfile (final ProgressDialog dialog) {
		new LoginTask(getActivity(), new CallBack<UserProfile>() {
			
			@Override
			public void callBack(UserProfile object) {
				if (object != null){
					BMMApplication.setCurrentUserProfile(object);
					Preferences.getInstans(getActivity()).setUserId(object.getId());
				}
				
				dialog.dismiss();
			}
		}).execute();
		new GetUserGifts(getActivity(), 1, null).execute();
		new GetAppuserReceivedGifts(getActivity(), 1, null).execute();
	}
	
	private void getAppuserFriandsList (){
		PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
		pictureAttributes.setHeight(250);
		pictureAttributes.setWidth(250);
		pictureAttributes.setType(PictureType.SQUARE);
		
    	Profile.Properties properties = new Profile.Properties.Builder()
	    .add(Properties.ID)
	    .add(Properties.FIRST_NAME)
	    .add(Properties.LAST_NAME)
	    .add(Properties.EMAIL)
	    .add(Properties.PICTURE,pictureAttributes)
	    .build();
        BMMApplication.mSimpleFacebook.getFriends(properties, new OnFriendsListener() {
        	
        	public void onComplete(java.util.List<com.sromku.simple.fb.entities.Profile> response) {
    			
    			ArrayList<FacebookFriand> friends = new ArrayList<FacebookFriand>();
    			for (int i = 0; i < response.size(); i++) {
					FacebookFriand temp = new FacebookFriand(response.get(i));
					friends.add(temp);
				}
    			BMMApplication.setUserFacebookFriends(friends);
    		};
		});
	}

	

	@Override
	public void onResume() {
		super.onResume();
		// setLocationManeger();
		setBackgroundImg(mBackgroundImg);
		adView1.resume();
		adView2.resume();
		
	}

	@Override
	public void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();
		adView1.pause();
		adView2.pause();
	}

	/**
	 * set the image according to the current time day or night
	 */

	@SuppressWarnings("deprecation")
	public static void setBackgroundImg(ImageView bgImage) {

		// TODO set background reassures to the picture
		int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		int index = (int) (Math.random() * 9);
		if (index > 9){
			index = 9;
		}
		if (hour >= 7 && hour <= 18) {
			// its day time
			currentBG = dayBgImages.getDrawable(index);
		} else {
			// its night time
			currentBG = nigteBgImages.getDrawable(index);
		}
		bgImage.setBackgroundDrawable(currentBG);
	}

	@Override
	public void onClick(View v) {

	//	MainActivity.cleanAllFragments();
		
		switch (v.getId()) {

		case R.id.main_menu_gifts:

			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container, new GiftsFragment(),
							GiftsFragment.TAG).addToBackStack(null).commit();
			
			/*Intent intent = new Intent(getActivity(),UserGiftsActivity.class);
			getActivity().startActivity(intent);*/
			break;
		case R.id.main_menu_profile:
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container, new ProfileFragment(),
							ProfileFragment.TAG).addToBackStack(null).commit();
			break;
		case R.id.main_menu_scan:
			//startAugmentedRealty();
			//PackageManager packageManager = getActivity().getPackageManager();
			//if (packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS)){
			//	startActivity(new Intent(getActivity(),ScanningActivity.class));
			//}else{
				startActivity(new Intent(getActivity(),MapActivity.class));
			//}
			
			break;
		case R.id.main_menu_settings:
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container, new SettingFragment(),
							SettingFragment.TAG).addToBackStack(null).commit();
			break;
		case R.id.main_menu_search:
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container, new SearchFragment(),
							SearchFragment.TAG).addToBackStack(null).commit();
			break;
		}
	}

	private void startAugmentedRealty() {

		//ArActivity.startWithSetup(getActivity(), new ArSetup());
	}
	
	
	
	

}
