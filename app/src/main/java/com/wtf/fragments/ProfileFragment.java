package com.wtf.fragments;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager.OnActivityResultListener;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.meg7.widget.CircleImageView;
import com.meg7.widget.SvgImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.UserProfile;
import com.wtf.profileFragments.ProfileCreditCardsFragment;
import com.wtf.profileFragments.ProfileDetailPage;
import com.wtf.profileFragments.ProfileFriendsFragment;
import com.wtf.profileFragments.ProfilePlacessFragment;
import com.wtf.profileFragments.ProfileRatingFragment;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.CreateUpdateUserTask;
import com.wtf.utils.DrawableUtills;
import com.wtf.utils.GeneralUtills;
import com.wtf.utils.SimpleFacebookUtils;

import wheel.Wheel;
import wheel.WheelAdapter;
import wheel.WheelAdapter.OnItemSelectionUpdatedListener;

public class ProfileFragment extends Fragment implements OnClickListener {

	public static final String TAG = "ProfileFragment";

	private static final int REQUEST_CODE = 100;

	private FrameLayout noFacebookFrame;
	private Button facebookLogin;
	private boolean isRejisteredUser;
	//private TextView mTitle;

	private static CircleImageView  mProfileImg;

	private MediaPlayer mMediaPlayer;

	private ImageView mWheelImg;
	private Button mRightArrow, mLeftArrow;
	private static SvgImageView mBluePic;
	
	private boolean isOn;
	

	private Wheel wheel;
	TypedArray iconsArray; 

	private int mWheelIndex = 0;
	private FrameLayout weelContaner;

	
	public ProfileFragment (){
		
	} 
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		isOn = true;
		return inflater.inflate(R.layout.new_prfofile_fragment, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		
		weelContaner = (FrameLayout) view.findViewById(R.id.ll);
		mRightArrow = (Button) view.findViewById(R.id.profile_right_arrow);
		mLeftArrow = (Button) view.findViewById(R.id.profile_left_arrow);
		mBluePic = (SvgImageView) view.findViewById(R.id.profile_blue_pic);
		//mTitle = (TextView) view.findViewById(R.id.profile_title);
		mWheelImg = (ImageView) view.findViewById(R.id.profile_wheel_img);
		mProfileImg = (CircleImageView) view
				.findViewById(R.id.profile_circularImageView);
		/*mProfileImg.setStrokeColor(Color.DKGRAY);
		mProfileImg.setStrokeWidth(4);
		mProfileImg.setStrokeEnabled(true);*/

		MainActivity.showTopButtons();

		setPorfilePic(getActivity());

		mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.wheel_click);

		mProfileImg.setOnClickListener(this);
		mRightArrow.setOnClickListener(this);
		mLeftArrow.setOnClickListener(this);

		//mWheelImg.setY(810);
		/*int screenHight = Preferences.getInstans(getActivity()).getScreenHeight();
		weelContaner.setY(screenHight - screenHight/3);
		wheel.setItems(icons);
		wheel.setWheelDiameter(500);
		wheel.setY(-100);785f*/
		
		wheel = (Wheel) view.findViewById(R.id.profile_wheel);
		iconsArray = getResources().obtainTypedArray(R.array.icons);
		Drawable[] icons = new Drawable[iconsArray.length()];
		for (int i = 0; i < iconsArray.length(); i++) {
			icons[i] = iconsArray.getDrawable(i);
		}
		int screenHight = Preferences.getInstans(getActivity()).getScreenHeight();
		Log.e(TAG,"screen hight"+ screenHight + "");
		weelContaner.setY(screenHight - screenHight/3);
		wheel.setItems(icons);
		wheel.setWheelDiameter(500);
		
		
		wheel.setOnItemSelectionUpdatedListener(new OnItemSelectionUpdatedListener() {

			@Override
			public void onItemSelectionUpdated(View view, int index) {

				
				if (isOn){
					mWheelIndex = index;
					mMediaPlayer.start();
					switchFragment(index);
				}
				
				
			}
		});

		noFacebookFrame = (FrameLayout)view.findViewById(R.id.no_facebook_frame);
		facebookLogin = (Button)view.findViewById(R.id.facebook_btn);
		facebookLogin.setOnClickListener(this);
		if (Preferences.getInstans(getActivity()).getFacebookId().equals("0")){
			isRejisteredUser = false;
			noFacebookFrame.setVisibility(View.VISIBLE);
			hideProfilePicture();
		} else {
			Log.e("appuserid", "id = "
					+ Preferences.getInstans(getActivity()).getFacebookId());
			isRejisteredUser = true;
			noFacebookFrame.setVisibility(View.GONE);
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.profile_detaile_holder,
							new ProfileDetailPage(), ProfileDetailPage.TAG)
					.commit();
		}

	}
	
	
	private void switchFragment(int index) {
		if (isRejisteredUser) {
			switch (index) {
			case 0:
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.profile_detaile_holder,
								new ProfileDetailPage(), ProfileDetailPage.TAG)
						.commit();
				//mTitle.setText(getString(R.string.personal_profile));
				break;
			case 1:
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.profile_detaile_holder,
								new ProfileCreditCardsFragment(),
								ProfileCreditCardsFragment.TAG).commit();
				//mTitle.setText(getString(R.string.my_credit_cards));
				break;
			case 2:
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.profile_detaile_holder,
								new ProfileRatingFragment(),
								ProfileRatingFragment.TAG).commit();
				//mTitle.setText(getString(R.string.my_raiting));
				break;
			case 3:
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.profile_detaile_holder,
								new ProfilePlacessFragment(),
								ProfilePlacessFragment.TAG).commit();
				//mTitle.setText(getString(R.string.my_placess));
				break;
			case 4:
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.profile_detaile_holder,
								new ProfileFriendsFragment(),
								ProfileFriendsFragment.TAG).commit();
				//mTitle.setText(getString(R.string.my_friends_using_the_app));
				break;
			}
		}else{
			hideProfilePicture();
		}
		
	}

	public static void setPorfilePic(final Activity activity) {
		

		String path = Preferences.getInstans(activity).getProfileImgPath();
		if (path == null ||path.equals("")){
			final String facebookImageUrl = Preferences.getInstans(activity).getFacebookImageProfile();
			mProfileImg.setImageBitmap(null); 
			if (facebookImageUrl == null || facebookImageUrl.equals("")){
				mProfileImg.setImageResource(R.drawable.com_facebook_profile_default_icon);
				return;
			}
			Picasso.with(activity)
			   .load(facebookImageUrl)
			   .fit()
			   .into(mProfileImg, new Callback() {

				@Override
				public void onError() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess() {
					// TODO Auto-generated method stub
					mProfileImg.setImageBitmap(null); 
					Picasso.with(activity)
					   .load(facebookImageUrl)
					   .fit()
					   .into(mProfileImg);
				}
				   
			   });
		}else{
			Uri uri = Uri.parse(path);
			InputStream input;
			try {
				input = activity.getContentResolver().openInputStream(uri);
				Bitmap bitmap = BitmapFactory.decodeStream(input);
				input.close();
				mProfileImg.setImageBitmap(null);
				mProfileImg.setImageBitmap(bitmap);
			} catch (Exception e) {} 
		}
		
	}

	public void pickImage() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		startActivityForResult(intent, REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK)
			try {
				InputStream stream = getActivity().getContentResolver()
						.openInputStream(data.getData());
				Bitmap bitmap = BitmapFactory.decodeStream(stream);
				stream.close();
				mProfileImg.setImageBitmap(bitmap);
				Uri path = data.getData();
				Preferences.getInstans(getActivity()).setProfileImgPath(
						path.toString());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	public void onDestroy() {
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
		}
		isOn = false;
		MainActivity.hideTopButtons();
		super.onDestroy();
	}

	public static void hideProfilePicture() {
		//mProfileImg.setVisibility(View.GONE);
		//mBluePic.setVisibility(View.GONE);
	}

	public static void showProfilePicture() {
		mProfileImg.setVisibility(View.VISIBLE);
		mBluePic.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.profile_circularImageView:
			pickImage();
			break;
		case R.id.profile_right_arrow:
			wheelIndexDown();
			wheel.setSelectedItem(mWheelIndex);
			break;
		case R.id.profile_left_arrow:
			wheelIndexUp();
			wheel.setSelectedItem(mWheelIndex);
			break;
		case R.id.facebook_btn:
			loginWithFacebook();
			break;

		}
	}
	
	private void wheelIndexUp() {

		mWheelIndex++;
		if (mWheelIndex > iconsArray.length()) {
			mWheelIndex = 0;
		}
		switchFragment(mWheelIndex);
	}

	private void wheelIndexDown() {

		mWheelIndex--;
		if (mWheelIndex < 0) {
			mWheelIndex = iconsArray.length();
		}
		switchFragment(mWheelIndex);
	}
	
	
	
	
	
	private void loginWithFacebook() {

		SimpleFacebookUtils.facebookLogin(getActivity(),
				new CallBack<UserProfile>() {
					@Override
					public void callBack(UserProfile object) {
						if (object == null){
							return;
						}
						BMMApplication.setCurrentUserProfile(object);
						Preferences.getInstans(getActivity()).setUserId(
								object.getId());
						getFragmentManager().beginTransaction()
								.detach(ProfileFragment.this)
								.attach(ProfileFragment.this).commit();
					}
				});
	}

}
