/**
 * 
 */
package com.wtf.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wtf.activity.R;

/**
 * @author natiapplications
 *
 */
public class AboutFragmetn extends Fragment{

	public static final String TAG = "BusinessPage1";
	public static final int TYPE_EXPLATION = 1;
	public static final int TYPE_ABOUT = 2;
	
	private int type ;
	
	private TextView title;
	private TextView content;
    private LinearLayout aboutContentLL;
	public AboutFragmetn (){
		
	}
	public AboutFragmetn(int type) {
		
		this.type = type;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.about_fragment, container , false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		title = (TextView)view.findViewById(R.id.about_title);
		content = (TextView)view.findViewById(R.id.content);
		aboutContentLL = (LinearLayout)view.findViewById(R.id.about_content_text_ll);
		
		switch (type) {
		case TYPE_ABOUT:
			title.setText(getString(R.string.about_the_app));
			content.setText(getString(R.string.first_time_terms));
			LinearLayout.LayoutParams textParams = (LinearLayout.LayoutParams)content.getLayoutParams();
			textParams.gravity = Gravity.RIGHT;
			content.setLayoutParams(textParams);
			content.setGravity(Gravity.RIGHT);
			content.setTextColor(Color.BLACK);
			aboutContentLL.setVisibility(View.GONE);
			break;

		case TYPE_EXPLATION:
			title.setText(getString(R.string.explenation_about_the_app));
			content.setText(getString(R.string.app_name));
			content.setTextColor(Color.RED);
			aboutContentLL.setVisibility(View.VISIBLE);
			
			break;
		}
	}
}