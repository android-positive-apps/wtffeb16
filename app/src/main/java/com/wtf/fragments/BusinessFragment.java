package com.wtf.fragments;


import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.test.PerformanceTestCase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.meg7.widget.CircleImageView;
import com.meg7.widget.SvgImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.businessFragments.BusinessPage1;
import com.wtf.businessFragments.BusinessPage2;
import com.wtf.businessFragments.BusinessPage3;
import com.wtf.businessFragments.BusinessPage4;
import com.wtf.businessFragments.BusinessPage5;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.Business;
import com.wtf.objects.FacebookFriand;
import com.wtf.objects.UserProfile;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.CreateUpdateUserTask;
import com.wtf.utils.DrawableUtills;
import com.wtf.utils.GeneralUtills;
import com.wtf.utils.SimpleFacebookUtils;

import wheel.Wheel;
import wheel.WheelAdapter;
import wheel.WheelAdapter.OnItemSelectionUpdatedListener;

@SuppressLint({ "ValidFragment", "Recycle" })
public class BusinessFragment extends Fragment implements OnClickListener {

	public static final String TAG = "BusinessFragment";
	public static final String EXTRA_FROM_MAP = "FromMap";
	
	private FrameLayout noFacebookFrame;
	private Button facebookLogin;
	private boolean isRejisteredUser;
	private boolean isFromMap;
	
	private TextView mTitle;
	private ImageView mWeelImage;
	private SvgImageView mCategoryBG;

	private  CircleImageView mProfileImg;

	private MediaPlayer mMediaPlayer;
	
	private Business mBusiness;

	private int mPosition;
	private int mId;

	private Fragment currentFragment;
	private int wheelDiameter = 400;

	private Wheel wheel;
	private int[] icons = { R.drawable.roller_icon3, R.drawable.roller_icon8,
			R.drawable.roller_icon5, R.drawable.roller_icon9 , R.drawable.roller_icon1 };
	
	TypedArray categoryBgImages; 
	TypedArray categoryDeafultIcons;
	
	private FrameLayout weelContaner;
	private FrameLayout holder;
	
	public static BusinessFragment newInstance(int pos,Business business,boolean fromMap){
		BusinessFragment instance = new BusinessFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MainActivity.EXTERA_BUSINESS, business);
		bundle.putInt(MainActivity.EXTERA_POSITION, pos);
		bundle.putBoolean(EXTRA_FROM_MAP, fromMap);
		instance.setArguments(bundle);
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.mBusiness = (Business)getArguments().getSerializable(MainActivity.EXTERA_BUSINESS);
		this.mPosition = getArguments().getInt(MainActivity.EXTERA_POSITION);
		try {
			this.isFromMap = getArguments().getBoolean(EXTRA_FROM_MAP);
		} catch (Exception e) {}
		
		return inflater.inflate(R.layout.new_buisness_fragment, container, false);
	}

	public  void hideProfilePicture() {
		//mProfileImg.setVisibility(View.GONE);
		//mCategoryBG.setVisibility(View.GONE);
	}

	public  void showProfilePicture() {
		mProfileImg.setVisibility(View.VISIBLE); 
		mCategoryBG.setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		noFacebookFrame = (FrameLayout)view.findViewById(R.id.no_facebook_frame);
		facebookLogin = (Button)view.findViewById(R.id.facebook_btn);
		facebookLogin.setOnClickListener(this);
		if (Preferences.getInstans(getActivity()).getFacebookId().equals("0")){
			isRejisteredUser = false;
			noFacebookFrame.setVisibility(View.VISIBLE);
		}else{
			Log.e("appuserid" , "id = " +Preferences.getInstans(getActivity()).getUserId() );
			isRejisteredUser = true;
			noFacebookFrame.setVisibility(View.GONE);
		}
		
		holder = (FrameLayout)view.findViewWithTag(getString(R.string.yes));
		holder.setId(mPosition+1);
		mId = mPosition+1;
		Fragment fragment = BusinessPage1.newInstance(mBusiness);
		getFragmentManager().beginTransaction()
				.replace(mId, fragment, BusinessPage1.TAG).commit();
		
		currentFragment = fragment;
		weelContaner = (FrameLayout)view.findViewById(R.id.ll);
		categoryBgImages = getResources().obtainTypedArray(R.array.categorys_bg);
		categoryDeafultIcons = getResources().obtainTypedArray(R.array.deafults_icons_b);

		mProfileImg = (CircleImageView) view.findViewById(R.id.business_circularImageView);
		
		
		mCategoryBG = (SvgImageView)view.findViewById(R.id.category_bg);
		
		int categoryBackground = BMMApplication.currentCatgoryIcon;
		if (isFromMap){
			categoryBackground = mBusiness.getCategoryType();
			BMMApplication.currentCatgoryIcon = categoryBackground;
			Log.i("CategoryTypeLog", "categoryType = " + categoryBackground);
		}else{
			Log.e("CategoryTypeLog", "categoryType = " + categoryBackground);
		}
		
		mCategoryBG.setBackgroundDrawable(categoryBgImages.getDrawable
				(categoryBackground));
		
		
		MainActivity.showTopButtonsLocationAndSettings();
		hideProfilePicture();
		showProfilePicture();
		setPorfilePic();
		
		mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.wheel_click);

		mWeelImage = (ImageView) view.findViewById(R.id.business_circle_img);
		wheel = (Wheel) view.findViewById(R.id.profile_wheel);

		setUpWheel();
		wheel.setSelectedItem(0);
		
	}

	private void setUpWheel() {
		
		int screenHight = Preferences.getInstans(getActivity()).getScreenHeight();
		Log.e(TAG,"screen hight"+ screenHight + "");
		weelContaner.setY(screenHight - screenHight/3);
		wheel.setItems(DrawableUtills.getDrawableFromData(getActivity(), icons));
		wheel.setWheelDiameter(wheelDiameter);
		//wheel.setY(-70);
		wheel.setOnItemSelectionUpdatedListener(new OnItemSelectionUpdatedListener() {

			@Override
			public void onItemSelectionUpdated(View view, int index) {

				mMediaPlayer.start();
				Fragment temp = null;
				String fragmentTag = "" ;
				boolean show = true;
				switch (index) {
				case 0:
					temp = BusinessPage1.newInstance(mBusiness);
					fragmentTag = BusinessPage1.TAG;
					showProfilePicture();
					break;
				case 1:
					temp = BusinessPage2.newInstance(mBusiness);
					fragmentTag = BusinessPage2.TAG;
					showProfilePicture();
					break;
				case 2:
					temp = BusinessPage3.newInstance(mBusiness);
					fragmentTag = BusinessPage3.TAG;
					showProfilePicture();
					show = isRejisteredUser;
					break;
				case 3:
					temp = BusinessPage4.newInstance(mBusiness);
					fragmentTag = BusinessPage4.TAG;
					showProfilePicture();
					break;
				case 4:
					temp = BusinessPage5.newInstance(mBusiness);
					fragmentTag = BusinessPage5.TAG;
					hideProfilePicture();
					show = isRejisteredUser;
					break;
				}
				
				if (show){
					currentFragment = temp;
					getFragmentManager()
					.beginTransaction()
					.replace(mId, temp,fragmentTag).commit();
				}else{
					hideProfilePicture();
					getFragmentManager()
					.beginTransaction()
					.remove(currentFragment).commit();
				}
			}

		});
	}

	private void setPorfilePic() {
		try {
			if (mBusiness.getImage() != null && !mBusiness.getImage().equals("")){
				Picasso.with(getActivity())
				   .load(mBusiness.getImage())
				   .fit()
				   .into(mProfileImg, new Callback(){
					@Override
					public void onError() {}
					@Override
					public void onSuccess() {
						// TODO Auto-generated method stub
						Picasso.with(getActivity())
						   .load(mBusiness.getImage())
						   .fit()
						   .into(mProfileImg);
					}
					   
				   });
			}else{
				if (mProfileImg != null){
					mProfileImg.setImageDrawable(categoryDeafultIcons.getDrawable
							(BMMApplication.currentCatgoryIcon));
				}
			}
		} catch (Exception e) {}
		catch (Error e) {}
		
	}

	@Override
	public void onDestroy() {
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
		}
		MainActivity.hideTopButtons();
		super.onDestroy();
	}

	
	@Override
	public void onClick(View v) {
		loginWithFacebook();
	}
	
	
	
	private void loginWithFacebook() {

		SimpleFacebookUtils.facebookLogin(getActivity(),
				new CallBack<UserProfile>() {
					@Override
					public void callBack(UserProfile object) {
						if (object != null){
							BMMApplication.setCurrentUserProfile(object);
							Preferences.getInstans(getActivity()).setUserId(
									object.getId());
							getFragmentManager().beginTransaction()
									.detach(BusinessFragment.this)
									.attach(BusinessFragment.this).commit();
						}
					}
				});
	}

}
