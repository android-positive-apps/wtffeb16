package com.wtf.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.wtf.activity.R;
import com.wtf.objects.CreditCard;
import com.wtf.objects.Place;
import com.wtf.profileFragments.ProfileCreditCardsFragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CreditCardsAdapter extends BaseAdapter {

	Context mContext;
	
	List<CreditCard> mCreditCards = new ArrayList<CreditCard>();
	
	
	public CreditCardsAdapter(Context context , List<CreditCard> creditCards){
		mCreditCards.addAll(creditCards);
		if (mCreditCards != null){
			Collections.reverse(mCreditCards);
		}
		mContext = context;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) mContext
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View view = convertView;
		if(view == null){
			view = inflater.inflate(R.layout.list_item_credit_cards, parent , false);
		}
		
		TextView cardName = (TextView) view.findViewById(R.id.list_item_credit_card_text);
		ImageView cardImage = (ImageView) view.findViewById(R.id.list_item_credit_card_img);
		
		
		
		cardImage.setImageDrawable(ProfileCreditCardsFragment.cardsIcons.getDrawable(mCreditCards.get(position).getImgResource()));
		cardName.setText(mCreditCards.get(position).getName());
		
		return view;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mCreditCards.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mCreditCards.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
