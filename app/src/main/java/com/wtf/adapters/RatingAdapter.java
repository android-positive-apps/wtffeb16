package com.wtf.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.fragments.BusinessFragment;
import com.wtf.fragments.SearchFragment;
import com.wtf.objects.Business;
import com.wtf.objects.Place;
import com.wtf.objects.Rating;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetBusinnesById;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RatingAdapter extends BaseAdapter {

	private Context mContext;

	private List<Rating> mRatings = new ArrayList<Rating>();

	public RatingAdapter(Context context, List<Rating> ratings) {
		
		mContext = context;
		mRatings.addAll(ratings);
		if (mRatings != null){
			Collections.reverse(mRatings);
		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
		
		View view = convertView;
		
		if(view == null){
			view = inflater.inflate(R.layout.list_item_ratings, parent , false);
		}
		
		TextView name = (TextView) view.findViewById(R.id.list_item_rating_tv_name);
		TextView date = (TextView) view.findViewById(R.id.list_item_rating_tv_date);
		ImageView img = (ImageView) view.findViewById(R.id.list_item_rating_img);
		RatingBar ratingBar = (RatingBar) view.findViewById(R.id.list_item_rating_ratingBar);
		
		name.setText(mRatings.get(position).getName());
		date.setText(mRatings.get(position).getDate());
		img.setBackgroundResource(SearchFragment.categoryIcons[mRatings.get(position).getImgResource()]);
		ratingBar.setRating(mRatings.get(position).getRate());
		
		LinearLayout frame = (LinearLayout)view.findViewById(R.id.rating_frame);
		frame.setTag(new Integer(position));
		frame.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				final Rating selected = (Rating) getItem(position);
				
				GetBusinnesById getBusinnesById = new GetBusinnesById(mContext,
						selected.getBusinessID(), selected.getBusinessExternalId(),
						new CallBack<Business>() {

							@Override
							public void callBack(Business object) {
								
								BMMApplication.currentCatgoryIcon = object.getCategoryType();
								
								
								FragmentActivity fragmentActivity = (FragmentActivity)mContext;
								fragmentActivity.getSupportFragmentManager().beginTransaction()
								
								.replace(R.id.container, BusinessFragment.newInstance(0, object,false) ,
										BusinessFragment.TAG).addToBackStack(null).commit();
								
							}
						});
				getBusinnesById.execute();
				
			}
		});
		
		return view ;
	}

	@Override
	public int getCount() {
		return mRatings.size();
	}

	@Override
	public Object getItem(int position) {
		return mRatings.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}
