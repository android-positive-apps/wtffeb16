/**
 * 
 */
package com.wtf.adapters;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.wtf.activity.R;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class WTFInfoWindowAdapter implements InfoWindowAdapter {
	  LayoutInflater inflater=null;

	  public WTFInfoWindowAdapter(LayoutInflater inflater) {
	    this.inflater=inflater;
	  }

	  @Override
	  public View getInfoWindow(Marker marker) {
	    return(null);
	  }

	  @Override
	  public View getInfoContents(Marker marker) {
	    View popup=inflater.inflate(R.layout.custom_info_window_view, null);
	    TextView tv=(TextView)popup.findViewById(R.id.name);
	    tv.setText(marker.getTitle());
	    return(popup);
	  }
}
