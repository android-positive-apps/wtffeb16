package com.wtf.adapters;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wtf.activity.MainActivity;
import com.wtf.fragments.BusinessFragment;
import com.wtf.objects.Business;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

	private static final int MAX_PAGES = 5;

	List<Business> mBusinesses;
	public ScreenSlidePagerAdapter(FragmentManager fm,List<Business> mBusinesses) {
		super(fm);
		this.mBusinesses = mBusinesses;
	}

	@Override
	public Fragment getItem(int position) {
			return BusinessFragment.newInstance(position, mBusinesses.get(position),false);
	}

	@Override
	public int getCount() {
		return mBusinesses.size();
	}

}
