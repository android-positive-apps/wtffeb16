/**
 * 
 */
package com.wtf.adapters;

import java.util.ArrayList;

import com.wtf.activity.R;
import com.wtf.objects.BuisnessGiftsAndCoupons.GiftSummery;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class BusinessGiftsAndCouponsListAdapter extends BaseAdapter {
	
	private Activity activity;
	private ArrayList<GiftSummery> data;
	
	

	public BusinessGiftsAndCouponsListAdapter(Activity activity,
			ArrayList<GiftSummery> data) {
		super();
		this.activity = activity;
		this.data = data;
	}

	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		convertView = activity.getLayoutInflater().inflate(R.layout.gifts_and_coupons_list_item, null);
		GiftSummery temp = data.get(position);
		
		TextView title = (TextView)convertView.findViewById(R.id.title);
		TextView desc = (TextView)convertView.findViewById(R.id.description);
		
		title.setText(temp.title);
		desc.setText(temp.desc);
		
		return convertView;
	}

}
