/**
 * 
 */
package com.wtf.adapters;

import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.dataBase.Preferences;
import com.wtf.objects.FacebookFriand;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class FriendsListAdapter extends BaseAdapter {

	private Activity activity;
	
	
	public FriendsListAdapter (Activity activity) {
		this.activity = activity;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return BMMApplication.getUserFacebookFriends().size();
	}

	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return BMMApplication.getUserFacebookFriends().get(position);
	}

	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		convertView = activity.getLayoutInflater().inflate(R.layout.freind_list_item, null);
		FacebookFriand temp = BMMApplication.getUserFacebookFriends().get(position);
		
		final NetworkImageView friendImage = (NetworkImageView)convertView.findViewById(R.id.friend_image);
		final TextView friendName = (TextView)convertView.findViewById(R.id.freind_name);
		
		friendName.setText(temp.getFirstName() + " " + temp.getLastName());
		
		friendImage.setImageUrl(temp.getImage());
		
		return convertView;
	}

}
