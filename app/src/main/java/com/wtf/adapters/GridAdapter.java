package com.wtf.adapters;

import java.util.List;

import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Picasso;
import com.wtf.activity.R;
import com.wtf.objects.FacebookFriand;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class GridAdapter extends BaseAdapter{
	
	private List<FacebookFriand> mPicturePaths;
	private Context mContext;

	public GridAdapter(Context context , List<FacebookFriand> picturePaths) {
		super();
		mContext = context;
		mPicturePaths = picturePaths;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		int size = mPicturePaths.size();
		if (size < 12) {
			size = 12;
		}
		return size;
	}

	@Override
	public FacebookFriand getItem(int position) {
		// TODO Auto-generated method stub
		if (position < mPicturePaths.size() ){
			return mPicturePaths.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) mContext
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View view = convertView;
		
		
		view = inflater.inflate(R.layout.list_item_facebook_picture, parent , false);
		
		
		ImageView imageView = (ImageView) view.findViewById(R.id.list_item_facebook_pic);
		//imageView.setImageUrl(mPicturePaths.get(position));
		
		if (getItem(position) != null){
			if (getItem(position).getImage() != null && 
					!getItem(position).getImage().isEmpty())
			Picasso.with(mContext)
			   .load(mPicturePaths.get(position).getImage())
			   .fit()
			   .into(imageView);
		}
		
		return view;
	}

}
