/**
 * 
 */
package com.wtf.adapters;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.wtf.activity.BMMApplication;
import com.wtf.activity.MainActivity;
import com.wtf.activity.R;
import com.wtf.fragments.businessResultListFragment;
import com.wtf.objects.Business;


/**
 * @author natiapplications
 *
 */
public class BuisnessView {
	
	public static final int SIDE_RIGHT = 1; 
	public static final int SIDE_LEFT = 2;
	
	private static final int STAR = 1;
	private static final int MAN = 2;
	
	private Business data;
	private OnBuisnessClickListener listener;
	private View view;
	private int side;
	private int index;
	
	
	public BuisnessView (Business data,int side,int index,OnBuisnessClickListener listener){
		this.data = data;
		this.side = side;
		this.listener = listener;
		this.index = index;
		
		buildView();
	}
	
	
	/**
	 * 
	 */
	private void buildView() {
		if (side == SIDE_RIGHT){
			view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.buisness_item_view, null);
		}else if (side == SIDE_LEFT){
			view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.buisness_item_view_left, null);
		}
		
		Button buisnessName = (Button)view.findViewById(R.id.name);
		ImageView ratingTop = (ImageView)view.findViewById(R.id.rating_top);
		ImageView ratingBottom = (ImageView)view.findViewById(R.id.rating_bottom);
		ImageView buisnessLogo = (ImageView)view.findViewById(R.id.business_logo);
		
		if (data.getImage().equals("")||data.getImage()== null){
			buisnessLogo.setImageDrawable(businessResultListFragment.categoryDeafultIcons.getDrawable
					(BMMApplication.currentCatgoryIcon));
		}else{
			Picasso.with(MainActivity.mainInstance)
			.load(data.getImage())
			.fit()
			.into(buisnessLogo,new picsoCallBac(buisnessLogo));
		}
		buisnessName.setTextColor(MainActivity.mainInstance.getResources().getColor(R.color.Blue));
		buisnessName.setText(data.getName());
		ratingTop.setImageDrawable(getRatingImage(STAR,data.getRank()));
		ratingBottom.setImageDrawable(getRatingImage(MAN,data.getManRank()));
		view.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (listener != null){
					listener.onBuisnessClick(arg0, data,index);
				}
			}
		});
	}

	
	public View getView(){
		return view;
	}
	
	private Drawable getRatingImage (int type,int rating){
		
		TypedArray ratingImages = null;
		int result = 0;
		Drawable toSet = null;
		if (type == STAR){
			ratingImages = MainActivity.mainInstance.getResources().obtainTypedArray(R.array.star_rating);
		}else if (type == MAN){
			ratingImages = MainActivity.mainInstance.getResources().obtainTypedArray(R.array.man_rating);
		}
		if (rating >0){
			result = rating/10;
		}
		if (result >= ratingImages.length()){
			result = ratingImages.length()-1;
		}
		toSet = ratingImages.getDrawable(result);
		
		return toSet;
		
	}

	public interface OnBuisnessClickListener {
		public void onBuisnessClick(View v,Business data,int index);
	}
	
	public class picsoCallBac implements com.squareup.picasso.Callback{

		
		private ImageView image;
		
		public picsoCallBac(ImageView image){
			this.image = image;
		}
		@Override
		public void onError() {
			image.setImageDrawable(businessResultListFragment.categoryDeafultIcons.getDrawable
					(BMMApplication.currentCatgoryIcon));
		}

		@Override
		public void onSuccess() {
			// TODO Auto-generated method stub
			Picasso.with(MainActivity.mainInstance)
			   .load(data.getImage())
			   .fit()
			   .into(image);
		}
		
	}

}
