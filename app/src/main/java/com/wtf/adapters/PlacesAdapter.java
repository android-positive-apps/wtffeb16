package com.wtf.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wtf.activity.BMMApplication;
import com.wtf.activity.R;
import com.wtf.fragments.BusinessFragment;
import com.wtf.objects.Business;
import com.wtf.objects.Place;
import com.wtf.serverTask.CallBack;
import com.wtf.serverTask.GetBusinnesById;

public class PlacesAdapter extends BaseAdapter {

	Activity mContext;
	
	List<Place> mPlaces = new ArrayList<Place>();
	
	
	public PlacesAdapter(Activity context , List<Place> places){
		mPlaces.addAll(places);
		if (mPlaces != null){
			Collections.reverse(mPlaces);
		}
		mContext = context;
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) mContext
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View view = convertView;
		if(view == null){
			view = inflater.inflate(R.layout.list_item_plasess, parent , false);
		}
		
		TextView placeName = (TextView) view.findViewById(R.id.list_item_places_name_tv);
		TextView placeDate = (TextView) view.findViewById(R.id.list_item_places_date_tv);
		
		Button btn = (Button) view.findViewById(R.id.list_item_places_btn);
		RelativeLayout frame = (RelativeLayout)view.findViewById(R.id.plase_frame);
		frame.setTag(new Integer(position));
		frame.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				final Place selected = (Place) getItem(position);

				GetBusinnesById getBusinnesById = new GetBusinnesById(mContext,
						selected.getBusinessID(), selected.getBusinessExternalID(),
						new CallBack<Business>() {

							@Override
							public void callBack(Business object) {
								//object.setCategoryType(selected.getCategoryType());
								BMMApplication.currentCatgoryIcon = object.getCategoryType();
								FragmentActivity fragmentActivity = (FragmentActivity)mContext;
								fragmentActivity.getSupportFragmentManager().beginTransaction()
								.replace(R.id.container,BusinessFragment.newInstance(0, object,false) ,
										BusinessFragment.TAG).addToBackStack(null).commit();
								
							}
						});
				getBusinnesById.execute();
				
			}
		});
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				openWaze(mPlaces.get(position));
			}
		});
		
		placeName.setText(mPlaces.get(position).getName());
		placeDate.setText(mPlaces.get(position).getDate());
		
		return view;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mPlaces.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mPlaces.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private void openWaze (Place place) {
		if (place == null || mContext == null){
			return;
		}
		
		
		Intent intent = new Intent(Intent.ACTION_VIEW);
		Uri location = Uri.parse("geo:"+place.getLat()+","+place.getLon()+"?z=11");
		
		Log.e("showmaplog", "location:  = " + place.getLat() + ", " + place.getLon());
		intent.setData(location);
		if (intent.resolveActivity(mContext.getPackageManager()) != null) {
			mContext.startActivity(intent);
		}
		
		/*try
		{
			String locationCordi = place.getLat() + "," + place.getLon();
			Uri location = Uri.parse("geo:"+place.getLat()+","+place.getLon()+"?z=11");
			Intent intent = new Intent(Intent.ACTION_VIEW,location);
	        mContext.startActivity(intent);
		}
		catch ( Exception ex  )
		{
		   Intent intent =
		    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
		   mContext.startActivity(intent);
		}*/
	}
	
	
	
	
}