/**
 * 
 */
package com.wtf.objects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;

import com.sromku.simple.fb.entities.Profile;
import com.wtf.storage.StorageListener;

/**
 * @author Nati Gabay
 *
 */
public class FacebookFriand implements Serializable {
	
	
	protected static final String KEY_FREINDS = "KEY_FRIENDS";
	private String firstName;
	private String lastName;
	private String FBID;
	private String image;
	private double lat;
	private double lon;
	private String locationDesc;
	
	
	public FacebookFriand (){
		
	}
	
	public FacebookFriand (Profile profile){
		this.firstName = profile.getFirstName();
		this.lastName = profile.getLastName();
		this.FBID = profile.getId();
		//this.locationDesc = profile.getLocation().getName();
		this.image = profile.getPicture();
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the fBID
	 */
	public String getFBID() {
		return FBID;
	}

	/**
	 * @param fBID the fBID to set
	 */
	public void setFBID(String fBID) {
		FBID = fBID;
	}

	/**
	 * @return the lat
	 */
	public double getLat() {
		return lat;
	}

	/**
	 * @param lat the lat to set
	 */
	public void setLat(double lat) {
		this.lat = lat;
	}

	/**
	 * @return the lon
	 */
	public double getLon() {
		return lon;
	}

	/**
	 * @param lon the lon to set
	 */
	public void setLon(double lon) {
		this.lon = lon;
	}

	/**
	 * @return the locationDesc
	 */
	public String getLocationDesc() {
		return locationDesc;
	}

	/**
	 * @param locationDesc the locationDesc to set
	 */
	public void setLocationDesc(String locationDesc) {
		this.locationDesc = locationDesc;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
	
	
	
	
   /* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((FBID == null) ? 0 : FBID.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FacebookFriand other = (FacebookFriand) obj;
		if (FBID == null) {
			if (other.FBID != null)
				return false;
		} else if (!FBID.equals(other.FBID))
			return false;
		return true;
	}

	public static void saveFriendsToStorage (final Context context, final ArrayList<FacebookFriand> toSave) {
		
		new Thread(new Runnable() {
			private FileOutputStream fos;
			private ObjectOutputStream oos;

			@Override
			public void run() {

				try {
					fos = context.openFileOutput(KEY_FREINDS,
							Context.MODE_PRIVATE);
					oos = new ObjectOutputStream(fos);
					oos.writeObject(toSave);
					oos.flush();
				} catch (Exception e) {
				} finally {
					try {
						if (fos != null) {
							fos.close();
						}
						if (oos != null) {
							oos.close();
						}
					} catch (IOException e) {
					}
				}

			}
		}).start();

	}
	
	public static void readFriendsFromStorage (final Context context,final StorageListener listener) {
		
		new Thread(new Runnable() {
			private FileInputStream fis;
			private ObjectInputStream ois;

			@Override
			public void run() {
			    ArrayList<FacebookFriand> friends = new ArrayList<FacebookFriand>();
				try {
					fis = context.openFileInput(KEY_FREINDS);
					ois = new ObjectInputStream(fis);
					
					friends = (ArrayList<FacebookFriand>) ois
							.readObject();
					
				} catch (Exception e) {
				} finally {
					listener.onStorageDataReceived(friends);
					try {
						if (fis != null) {
							fis.close();
						}
						if (ois != null) {
							ois.close();
						}

					} catch (IOException e) {
					}

				}
			}
		}).start();
		
	}
	
	
	
	

}
