/**
 * 
 */
package com.wtf.objects;

import org.json.JSONException;
import org.json.JSONObject;

import com.wtf.utils.Const;

/**
 * @author natiapplications
 *
 */
public class Gift {
	
	
	
	private String businessId;
	private String title;
	private String description;
	private String imageURl;
	private String password;
	private String status;
	private String expriation;
	private String id;
	private String createdAt;
	private int giftType;
	private String smallLetters;
	private String businessName;
	
	
	
	public Gift(JSONObject toParse) {

		parse(toParse);

	}
	
	
	private void parse (JSONObject toParse)  {
		
		businessId = toParse.optString("BusinessID");
		title = toParse.optString("Title");
		description = toParse.optString("Description");
		imageURl = toParse.optString("Image");
		password = toParse.optString("Password");
		status = toParse.optString("Status");
		expriation = toParse.optString("Expiration");
		id = toParse.optString("ID");
		createdAt = toParse.optString("Created");
		giftType = toParse.optInt("Type");
		smallLetters = toParse.optString("SmallLetters"); 
		businessName = toParse.optString("BusinessName");

		
		
	}


	/**
	 * @return the businessId
	 */
	public String getBusinessId() {
		return businessId;
	}


	/**
	 * @param businessId the businessId to set
	 */
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}


	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the imageURl
	 */
	public String getImageURl() {
		return imageURl;
	}


	/**
	 * @param imageURl the imageURl to set
	 */
	public void setImageURl(String imageURl) {
		this.imageURl = imageURl;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	/**
	 * @return the expriation
	 */
	public String getExpriation() {
		return expriation;
	}


	/**
	 * @param expriation the expriation to set
	 */
	public void setExpriation(String expriation) {
		this.expriation = expriation;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}


	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


	/**
	 * @return the giftType
	 */
	public int getGiftType() {
		return giftType;
	}


	/**
	 * @param giftType the giftType to set
	 */
	public void setGiftType(int giftType) {
		this.giftType = giftType;
	}


	/**
	 * @return the smallLetters
	 */
	public String getSmallLetters() {
		return smallLetters;
	}


	/**
	 * @param smallLetters the smallLetters to set
	 */
	public void setSmallLetters(String smallLetters) {
		this.smallLetters = smallLetters;
	}


	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}


	/**
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	} 
	
	
	
	
	
	
	

}
