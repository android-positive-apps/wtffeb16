/**
 * 
 */
package com.wtf.objects;

import java.util.ArrayList;

/**
 * @author natiapplications
 *
 */
public class BuisnessGiftsAndCoupons {
	
	
	public static final int TYPE_COUPON = 1;
	public static final int TYPE_GIFT = 2;
	
	private ArrayList<Gift> gifts;
	private ArrayList<Coupon> copupons;
	
	
	public BuisnessGiftsAndCoupons() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the gifts
	 */
	public ArrayList<Gift> getGifts() {
		return gifts;
	}


	/**
	 * @param gifts the gifts to set
	 */
	public void setGifts(ArrayList<Gift> gifts) {
		this.gifts = gifts;
	}


	/**
	 * @return the copupons
	 */
	public ArrayList<Coupon> getCopupons() {
		return copupons;
	}


	/**
	 * @param copupons the copupons to set
	 */
	public void setCopupons(ArrayList<Coupon> copupons) {
		this.copupons = copupons;
	}
	
	public ArrayList<GiftSummery> getAllNames () {
		ArrayList<GiftSummery> names = new ArrayList<GiftSummery>();
		
		if (gifts != null){
			for (int i = 0; i < gifts.size(); i++) {
				GiftSummery temp = new GiftSummery(TYPE_GIFT,i,
						gifts.get(i).getTitle(), gifts.get(i).getDescription());
				names.add(temp);
			}
		}
		if (copupons != null){
			for (int i = 0; i < copupons.size(); i++) {
				GiftSummery temp = new GiftSummery(TYPE_COUPON,i,
						copupons.get(i).getTitle(), copupons.get(i).getDescription());
				names.add(temp);
			}
		}
		
		return names;
	}
	
	public class GiftSummery {
		public String title;
		public String desc;
		public int type;
		public int indexInParentList;
		
		public GiftSummery (int type,int index,String title,String desc){
			this.type = type;
			this.indexInParentList = index;
			this.title = title;
			this.desc = desc;
		}
	}
	
	
	
	

}
