package com.wtf.objects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.wtf.storage.StorageListener;

import android.content.Context;


public class Place implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String KEY_PLACES = "KeyPlacess";
	
	private String name;
	private double lat;
	private double lon;
	private String date;
	private String businessID;
	private String businessExternalID;
	private int categoryType;
	
	/**
	 * @return the categoryType
	 */
	public int getCategoryType() {
		return categoryType;
	}

	/**
	 * @param categoryType the categoryType to set
	 */
	public void setCategoryType(int categoryType) {
		this.categoryType = categoryType;
	}

	/***
	 * used on the profile fragment to create the places List
	 * 
	 * @param name
	 * @param date
	 */

	public Place(String name,double lat,double lon,String businessID,String businessExternakID ) {
		super();
		this.name = name;
		this.businessID = businessID;
		this.businessExternalID = businessExternakID;
		this.date = getCurrentDate();
		this.lat = lat;
		this.lon = lon;
	}

	/**
	 * @return
	 */
	private String getCurrentDate() {
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formter = new SimpleDateFormat("dd/MM/yyyy");
		String date = formter.format(calendar.getTime());
		return date;
	}

	public String getName() {
		return name;
	}

	public String getDate() {
		return date;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the lat
	 */
	public double getLat() {
		return lat;
	}

	/**
	 * @param lat the lat to set
	 */
	public void setLat(double lat) {
		this.lat = lat;
	}

	/**
	 * @return the lon
	 */
	public double getLon() {
		return lon;
	}

	/**
	 * @param lon the lon to set
	 */
	public void setLon(double lon) {
		this.lon = lon;
	}
	
	
	
	/**
	 * @return the businessID
	 */
	public String getBusinessID() {
		return businessID;
	}

	/**
	 * @param businessID the businessID to set
	 */
	public void setBusinessID(String businessID) {
		this.businessID = businessID;
	}

	/**
	 * @return the businessExternalID
	 */
	public String getBusinessExternalID() {
		return businessExternalID;
	}

	/**
	 * @param businessExternalID the businessExternalID to set
	 */
	public void setBusinessExternalID(String businessExternalID) {
		this.businessExternalID = businessExternalID;
	}

	public static void savePlacesToStorage (final Context context, final ArrayList<Place> toSave) {
		
		new Thread(new Runnable() {
			private FileOutputStream fos;
			private ObjectOutputStream oos;

			@Override
			public void run() {

				try {
					fos = context.openFileOutput(KEY_PLACES,
							Context.MODE_PRIVATE);
					oos = new ObjectOutputStream(fos);
					oos.writeObject(toSave);
					oos.flush();
				} catch (Exception e) {
				} finally {
					try {
						if (fos != null) {
							fos.close();
						}
						if (oos != null) {
							oos.close();
						}
					} catch (IOException e) {
					}
				}

			}
		}).start();

	}
	
	public static void readPlacesFromStorage (final Context context,final StorageListener listener) {
		
		new Thread(new Runnable() {
			private FileInputStream fis;
			private ObjectInputStream ois;

			@Override
			public void run() {
			    ArrayList<Place> places = new ArrayList<Place>();
				try {
					fis = context.openFileInput(KEY_PLACES);
					ois = new ObjectInputStream(fis);
					
					places = (ArrayList<Place>) ois
							.readObject();
					
				} catch (Exception e) {
				} finally {
					listener.onStorageDataReceived(places);
					try {
						if (fis != null) {
							fis.close();
						}
						if (ois != null) {
							ois.close();
						}

					} catch (IOException e) {
					}

				}
			}
		}).start();
		
	}
	

	

	
	
	
	
	

}
