/**
 * 
 */
package com.wtf.objects;

import org.json.JSONObject;

/**
 * @author Nati Gabay
 *
 */
public class FriandLocation {
	
	
	private String id;
	private String FBID;
	private String lat,lon;
	
	
	public FriandLocation (){
		
	}


	public FriandLocation(String fbid,String id,String lat, String lon) {
		super();
		this.FBID = fbid;
		this.id = id;
		this.lat = lat;
		this.lon = lon;
	}
	
	public FriandLocation(JSONObject toParse){
		parse(toParse);
	}
	
	private void parse (JSONObject toParse){
		this.FBID = toParse.optString("FBID");
		this.lat = toParse.optString("Lat");
		this.lon = toParse.optString("Lng");
		this.id = toParse.optString("ID");
	}


	/**
	 * @return the lat
	 */
	public double getLat() {
		return Double.parseDouble(lat);
	}


	/**
	 * @param lat the lat to set
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}


	/**
	 * @return the lon
	 */
	public double getLon() {
		return Double.parseDouble(lon);
	}


	/**
	 * @param lon the lon to set
	 */
	public void setLon(String lon) {
		this.lon = lon;
	}


	/**
	 * @return the fBID
	 */
	public String getFBID() {
		return FBID;
	}


	/**
	 * @param fBID the fBID to set
	 */
	public void setFBID(String fBID) {
		FBID = fBID;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	
	

}
