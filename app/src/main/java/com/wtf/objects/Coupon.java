package com.wtf.objects;

import org.json.JSONException;
import org.json.JSONObject;

import com.wtf.utils.Const;

public class Coupon {
	
	
	private String imgUrl = "";
	private String id = "";
	private String businessID = "";
	private String title = "";
	private String description = "";
	private String password = "";
	private String numItems = "";
	private String itemsLeft = "";
	private String status = "";
	private String expiration = "";
	private String created = "";
	private String smallLetters = "";
	private String businessName = "";
	
	
	public Coupon(String id){
		this.id = id;
	}
	
	
	public Coupon(JSONObject jsonObject) throws JSONException {
		imgUrl = jsonObject.optString("Image");
		id = jsonObject.optString("ID");
		businessID = jsonObject.optString("BusinessID");
		created = jsonObject.optString("Created");
		description = jsonObject.optString("Description");
		expiration = jsonObject.optString("Expiration");
		itemsLeft = jsonObject.optString("ItemsLeft");
		numItems = jsonObject.optString("NumItems");
		password = jsonObject.optString("Password");
		status = jsonObject.optString("Password");
		title = jsonObject.optString("Title");
		smallLetters = jsonObject.optString("SmallLetters");
		businessName = jsonObject.optString("BusinessName");
	}

	public String getImgUrl() {
		return imgUrl; 
	}

	public String getId() {
		return id;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBusinessID() {
		return businessID;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getPassword() {
		return password;
	}

	public String getNumItems() {
		return numItems;
	}

	public String getItemsLeft() {
		return itemsLeft;
	}

	public String getStatus() {
		return status;
	}

	public String getExpiration() {
		return expiration;
	}

	public String getCreated() {
		return created;
	}

	public void setBusinessID(String businessID) {
		this.businessID = businessID;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setNumItems(String numItems) {
		this.numItems = numItems;
	}

	public void setItemsLeft(String itemsLeft) {
		this.itemsLeft = itemsLeft;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}

	public void setCreated(String created) {
		this.created = created;
	}
	

	/**
	 * @return the smallLetters
	 */
	public String getSmallLetters() {
		return smallLetters;
	}


	/**
	 * @param smallLetters the smallLetters to set
	 */
	public void setSmallLetters(String smallLetters) {
		this.smallLetters = smallLetters;
	}

	

	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}


	/**
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}


	@Override
	public String toString() {
		return "Coupon [imgUrl=" + imgUrl + ", id=" + id + ", businessID="
				+ businessID + ", title=" + title + ", description="
				+ description + ", password=" + password + ", numItems="
				+ numItems + ", itemsLeft=" + itemsLeft + ", status=" + status
				+ ", expiration=" + expiration + ", created=" + created + "]";
	}
	
	
	
	
	
	
	

}
