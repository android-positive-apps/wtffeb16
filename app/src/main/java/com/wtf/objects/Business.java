package com.wtf.objects;

import java.io.Serializable;
import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import android.util.Log;

import com.wtf.activity.ScanningActivity;
import com.wtf.dataBase.Preferences;
import com.wtf.fragments.SearchFragment;

public class Business implements Serializable{

	
	private String[] categories = {"אוכל", "ספורט","פנאי","אופנה","לילה"};	
	
	private String name;
	private String address;
	private String id;
	private String externalID;
	private String phone;
	private double Lat;
	private double Lng;
	private String Website;
	private String FacebookLink;
	private String Opening;
	private String BusinessDesc;
	private int Rank;
	private String Image;
	private int manRank;
	
	private int markerRecourse;
	private int markerRecourseSelected;
	
	private int businessPerfer;
	private int distancePoints;
	private boolean showMarker;
	private int categoryType;
	// constructors

	/***
	 * generate this object from a JSON with it we create the the specific
	 * business fragment
	 * 
	 * @param businessJson
	 */

	public Business(JSONObject businessJson) {

		this.name = businessJson.optString("Name");
		this.address = businessJson.optString("Street") + " "
				+ businessJson.optString("HouseNum") + " " 
				+ businessJson.optString("City");
		this.id = businessJson.optString("ID");
		this.phone = businessJson.optString("Phone");
		this.Lat = businessJson.optDouble("Lat");
		this.Lng = businessJson.optDouble("Lng");
		this.Website = businessJson.optString("Website");
		this.FacebookLink = businessJson.optString("FacebookLink");
		this.Opening = businessJson.optString("Opening");
		this.BusinessDesc = businessJson.optString("BusinessDesc");
		this.Rank = businessJson.optInt("Rank");
		this.manRank = businessJson.optInt("CheckIns");
		this.Image = businessJson.optString("Image");
		
		Log.e("categoryBuLog", "Category = " + businessJson.optString("Category"));
		
		this.categoryType = getCategoryByName(businessJson.optString("Category"));

	}

	
	public int getCategoryByName (String name){
		
		
		if (name.equalsIgnoreCase("אוכל")){
			return SearchFragment.CATEGORY_FODD;
		}else if (name.equalsIgnoreCase("ספורט")){
			return SearchFragment.CATEGORY_SPORT;
		}else if(name.equalsIgnoreCase("פנאי")){
			return SearchFragment.CATEGORY_TIME;
		}else if(name.equalsIgnoreCase("אופנה")){
			return SearchFragment.CATEGORY_FATION;
		}else if(name.equalsIgnoreCase("לילה")){
			return SearchFragment.CATEGORY_NIGHT;
		}
		
		return 0;
		
	}
	
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getId() {
		return id;
	}

	public String getExternalID() {
		return externalID;
	}

	public String getPhone() {
		return phone;
	}

	public double getLat() {
		return Lat;
	}

	public double getLng() {
		return Lng;
	}

	public String getWebsite() {
		return Website;
	}

	public String getFacebookLink() {
		return FacebookLink;
	}

	public String getOpening() {
		return Opening;
	}

	public String getBusinessDesc() {
		return BusinessDesc;
	}

	public int getRank() {
		return Rank;
	}

	public String getImage() {
		return Image;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setExternalID(String externalID) {
		this.externalID = externalID;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setLat(double lat) {
		Lat = lat;
	}

	public void setLng(double lng) {
		Lng = lng;
	}

	public void setWebsite(String website) {
		Website = website;
	}

	public void setFacebookLink(String facebookLink) {
		FacebookLink = facebookLink;
	}

	public void setOpening(String opening) {
		Opening = opening;
	}

	public void setBusinessDesc(String businessDesc) {
		BusinessDesc = businessDesc;
	}

	public void setRank(int rank) {
		Rank = rank;
	}

	public void setImage(String image) {
		Image = image;
	}
	
	

	/**
	 * @return the manRank
	 */
	public int getManRank() {
		return manRank;
	}

	/**
	 * @param manRank the manRank to set
	 */
	public void setManRank(int manRank) {
		this.manRank = manRank;
	}
	
	

	/**
	 * @return the markerRecourse
	 */
	public int getMarkerRecourse() {
		return markerRecourse;
	}

	/**
	 * @param markerRecourse the markerRecourse to set
	 */
	public void setMarkerRecourse(int markerRecourse) {
		this.markerRecourse = markerRecourse;
	}
	
	

	/**
	 * @return the markerRecourseSelected
	 */
	public int getMarkerRecourseSelected() {
		return markerRecourseSelected;
	}

	/**
	 * @param markerRecourseSelected the markerRecourseSelected to set
	 */
	public void setMarkerRecourseSelected(int markerRecourseSelected) {
		this.markerRecourseSelected = markerRecourseSelected;
	}

	@Override
	public String toString() {
		return "Business [name=" + name + ", address=" + address + ", id=" + id
				+ ", externalID=" + externalID + ", phone=" + phone + ", Lat="
				+ Lat + ", Lng=" + Lng + ", Website=" + Website
				+ ", FacebookLink=" + FacebookLink + ", Opening=" + Opening
				+ ", BusinessDesc=" + BusinessDesc + ", Rank=" + Rank
				+ ", Image=" + Image + "]";
	}

	/**
	 * @return the businessPerfer
	 */
	public int getBusinessPerfer() {
		return businessPerfer;
	}

	/**
	 * @param businessPerfer the businessPerfer to set
	 */
	public void setBusinessPerfer(int businessPerfer) {
		this.businessPerfer = businessPerfer;
	}
	
	
	
	/**
	 * @return the showMarker
	 */
	public boolean isShowMarker() {
		return showMarker;
	}

	/**
	 * @param showMarker the showMarker to set
	 */
	public void setShowMarker(boolean showMarker) {
		this.showMarker = showMarker;
	}

	
	
	/**
	 * @return the categoryType
	 */
	public int getCategoryType() {
		return categoryType;
	}

	/**
	 * @param categoryType the categoryType to set
	 */
	public void setCategoryType(int categoryType) {
		this.categoryType = categoryType;
	}

	public void findDistanceFromUser(Context context,double targetLat, double targetLang)
	{
		double userLat = Double.parseDouble(Preferences.getInstans(context).getLatitude());
		double userLon = Double.parseDouble(Preferences.getInstans(context).getLongitude());
		
		double earthRadius = 3958.75;
	    double dLat = Math.toRadians(targetLat-userLat);
	    double dLng = Math.toRadians(targetLang-userLon);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	               Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(targetLat)) *
	               Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;

	    int meterConversion = 1609;
	    int mDistance = (int) (dist * meterConversion);
	    if (mDistance > 0){
	    	mDistance = mDistance/1000;
	    }
		this.distancePoints = Preferences.getInstans(context).getRadius() - mDistance;
		
	}

}
