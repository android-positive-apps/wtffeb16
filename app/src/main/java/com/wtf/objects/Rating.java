package com.wtf.objects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.content.Context;

import com.wtf.storage.StorageListener;

public class Rating implements Serializable {

	
	public static final String KEY_RATING = "KeyUserRatings";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	
	private String date;
	
	private float rate;
	
	private int imgResource;
	private String businessID;
	private String businessExternalId;
	private int categoryType;
	/***
	 * used on the profile fragment to create the rating List
	 * 
	 * @param name
	 * @param date
	 * @param rate
	 * @param imgResource
	 */

	public Rating(String name,  float rate, int imgResource,String businessID, String businessExternalId) {
		super();
		this.name = name;
		this.date = getCurrentDate();
		this.rate = rate;
		this.businessID = businessID;
		this.businessExternalId = businessExternalId;
		this.imgResource = imgResource;
	}

	
	/**
	 * @return
	 */
	private String getCurrentDate() {
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formter = new SimpleDateFormat("dd/MM/yyyy");
		String date = formter.format(calendar.getTime());
		return date;
	}
	
	
	public String getName() {
		return name;
	}

	public String getDate() {
		return date;
	}

	public float getRate() {
		return rate;
	}

	public int getImgResource() {
		return imgResource;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public void setImgResource(int imgResource) {
		this.imgResource = imgResource;
	}
	
	
	
	/**
	 * @return the businessID
	 */
	public String getBusinessID() {
		return businessID;
	}


	/**
	 * @param businessID the businessID to set
	 */
	public void setBusinessID(String businessID) {
		this.businessID = businessID;
	}


	/**
	 * @return the businessExternalId
	 */
	public String getBusinessExternalId() {
		return businessExternalId;
	}


	/**
	 * @param businessExternalId the businessExternalId to set
	 */
	public void setBusinessExternalId(String businessExternalId) {
		this.businessExternalId = businessExternalId;
	}


	
	/**
	 * @return the categoryType
	 */
	public int getCategoryType() {
		return categoryType;
	}


	/**
	 * @param categoryType the categoryType to set
	 */
	public void setCategoryType(int categoryType) {
		this.categoryType = categoryType;
	}


	public static void saveRatingsToStorage(final Context context,
			final ArrayList<Rating> toSave) {

		new Thread(new Runnable() {
			private FileOutputStream fos;
			private ObjectOutputStream oos;

			@Override
			public void run() {

				try {
					fos = context.openFileOutput(KEY_RATING,
							Context.MODE_PRIVATE);
					oos = new ObjectOutputStream(fos);
					oos.writeObject(toSave);
					oos.flush();
				} catch (Exception e) {
				} finally {
					try {
						if (fos != null) {
							fos.close();
						}
						if (oos != null) {
							oos.close();
						}
					} catch (IOException e) {
					}
				}

			}
		}).start();

	}

	public static void readRatingsFromStorage(final Context context,
			final StorageListener listener) {

		new Thread(new Runnable() {
			private FileInputStream fis;
			private ObjectInputStream ois;

			@Override
			public void run() {
				ArrayList<Rating> ratings = new ArrayList<Rating>();
				try {
					fis = context.openFileInput(KEY_RATING);
					ois = new ObjectInputStream(fis);

					ratings = (ArrayList<Rating>) ois.readObject();

				} catch (Exception e) {
				} finally {
					listener.onStorageDataReceived(ratings);
					try {
						if (fis != null) {
							fis.close();
						}
						if (ois != null) {
							ois.close();
						}

					} catch (IOException e) {
					}

				}
			}
		}).start();

	}
	
	
	
}
