package com.wtf.objects;

import org.json.JSONObject;

public class Response {

	private int error = 1;
	
	private String errdesc;
	
	
	public Response(JSONObject jsonObject){
		if (jsonObject != null){
			error = jsonObject.optInt("error");
			errdesc = jsonObject.optString("errdesc");
		}
		
	}
	
	
	public String getErrorMessage(){
		return errdesc;
	}
	
	public boolean isSuccess(){
		
		if(error == 1){
			return false;
		}
		
		return true;
	}
	
	
}
