/**
 * 
 */
package com.wtf.objects;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Nati Gabay
 *
 */
public class UserProfile {
	
	
	private String myPushServerId;
	private String FirstName;
	private String lastName;
	private String password;
	private String udid;
	private String countryCode;
	private String updateInterval;
	private String radius;
	private int correctCounter;
	private String countryName;
	private String city;
	private String state;
	private String about;
	private String gcmToken;
	private String pushToken;
	private String email;
	private String facebookId;
	private String phone;
	private int points;
	private String facebookAvatar;
	private String lastLan;
	private String lasLon;
	private String lastLocationUpdateTime;
	private String id;
	private String createdAt;
	private String ip;
	
	private ArrayList<Gift> userGifts = new ArrayList<Gift>();
	private ArrayList<Gift> receivedGift = new ArrayList<Gift>();
	private ArrayList<Action> actions = new ArrayList<Action>();
	
	
	public UserProfile (JSONObject toParse){
		
		try {
			parse(toParse);
		} catch (JSONException e) {}
	}
	
	
	private void parse (JSONObject toParse) throws JSONException {
		
		JSONObject appUser = toParse.getJSONObject("Appuser");
		
		this.myPushServerId = appUser.getString("MyPushServerID");
		this.FirstName = appUser.getString("FirstName");
		this.lastName = appUser.getString("LastName");
		this.password = appUser.getString("Password");
		this.udid = appUser.getString("UDID");
		this.updateInterval = appUser.getString("UpdateInterval");
		this.radius = appUser.getString("Radius");
		this.countryCode = appUser.getString("CountryCode");
		this.countryName = appUser.getString("CountryName");
		this.correctCounter = Integer.parseInt(appUser.getString("CorrectCounter"));
		this.city = appUser.getString("City");
		this.state = appUser.getString("State");
		this.about = appUser.getString("About");
		this.gcmToken = appUser.getString("GCM");
		this.pushToken = appUser.getString("PushToken");
		this.email = appUser.getString("Email");
		this.facebookId = appUser.getString("FBID");
		this.facebookAvatar = appUser.getString("FBAvatar");
		this.phone = appUser.getString("Phone");
		this.points = Integer.parseInt(appUser.getString("Points"));
		this.lastLan = appUser.getString("LastLat");
		this.lasLon = appUser.getString("LastLng");
		this.lastLocationUpdateTime = appUser.getString("LastLocationUpdate");
		this.id = appUser.getString("ID");
		this.createdAt = appUser.getString("Created");
		this.ip = appUser.getString("IP");
		
		JSONArray userGiftsJA = toParse.getJSONArray("MyGifts");
		JSONArray receivedGiftsJA = toParse.getJSONArray("ReceivedGifts");
		JSONArray actionGiftsJA = toParse.getJSONArray("ActionsList");
		
		for (int i = 0; i < userGiftsJA.length(); i++) {
			Gift temp = new Gift(userGiftsJA.getJSONObject(i));
			this.userGifts.add(temp);
		}
		for (int i = 0; i < receivedGiftsJA.length(); i++) {
			Gift temp = new Gift(receivedGiftsJA.getJSONObject(i));
			this.receivedGift.add(temp);
		}
		for (int i = 0; i < actionGiftsJA.length(); i++) {
			Action temp = new Action(actionGiftsJA.getJSONObject(i));
			this.actions.add(temp);
		}
		
	}


	/**
	 * @return the myPushServerId
	 */
	public String getMyPushServerId() {
		return myPushServerId;
	}


	/**
	 * @param myPushServerId the myPushServerId to set
	 */
	public void setMyPushServerId(String myPushServerId) {
		this.myPushServerId = myPushServerId;
	}


	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return FirstName;
	}


	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the udid
	 */
	public String getUdid() {
		return udid;
	}


	/**
	 * @param udid the udid to set
	 */
	public void setUdid(String udid) {
		this.udid = udid;
	}


	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}


	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	/**
	 * @return the updateInterval
	 */
	public int getUpdateInterval() {
		if (updateInterval.equals("")){
			return 0;
		}
		return Integer.parseInt(updateInterval) ;
	}


	/**
	 * @param updateInterval the updateInterval to set
	 */
	public void setUpdateInterval(String updateInterval) {
		this.updateInterval = updateInterval;
	}


	/**
	 * @return the radius
	 */
	public int getRadius() {
		if (radius.equals("")){
			return 0;
		}
		return Integer.parseInt(radius);
	}


	/**
	 * @param radius the radius to set
	 */
	public void setRadius(String radius) {
		this.radius = radius;
	}


	/**
	 * @return the correctCounter
	 */
	public int getCorrectCounter() {
		return correctCounter;
	}


	/**
	 * @param correctCounter the correctCounter to set
	 */
	public void setCorrectCounter(int correctCounter) {
		this.correctCounter = correctCounter;
	}


	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}


	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}


	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}


	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}


	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}


	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}


	/**
	 * @return the about
	 */
	public String getAbout() {
		return about;
	}


	/**
	 * @param about the about to set
	 */
	public void setAbout(String about) {
		this.about = about;
	}


	/**
	 * @return the gcmToken
	 */
	public String getGcmToken() {
		return gcmToken;
	}


	/**
	 * @param gcmToken the gcmToken to set
	 */
	public void setGcmToken(String gcmToken) {
		this.gcmToken = gcmToken;
	}


	/**
	 * @return the pushToken
	 */
	public String getPushToken() {
		return pushToken;
	}


	/**
	 * @param pushToken the pushToken to set
	 */
	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the facebookId
	 */
	public String getFacebookId() {
		return facebookId;
	}


	/**
	 * @param facebookId the facebookId to set
	 */
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}


	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}


	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}


	/**
	 * @return the points
	 */
	public int getPoints() {
		
		return this.points;
	}


	/**
	 * @param points the points to set
	 */
	public void setPoints(int points) {
		this.points = points;
	}


	/**
	 * @return the facebookAvatar
	 */
	public String getFacebookAvatar() {
		return facebookAvatar;
	}


	/**
	 * @param facebookAvatar the facebookAvatar to set
	 */
	public void setFacebookAvatar(String facebookAvatar) {
		this.facebookAvatar = facebookAvatar;
	}


	/**
	 * @return the lastLan
	 */
	public double getLastLan() {
		if (lastLan.equals("")){
			return 0;
		}
		return Double.parseDouble(lastLan);
	}


	/**
	 * @param lastLan the lastLan to set
	 */
	public void setLastLan(String lastLan) {
		this.lastLan = lastLan;
	}


	/**
	 * @return the lasLon
	 */
	public double getLasLon() {
		if (lasLon.equals("")){
			return 0;
		}
		return Double.parseDouble(lasLon);
	}


	/**
	 * @param lasLon the lasLon to set
	 */
	public void setLasLon(String lasLon) {
		this.lasLon = lasLon;
	}


	/**
	 * @return the lastLocationUpdateTime
	 */
	public String getLastLocationUpdateTime() {
		return lastLocationUpdateTime;
	}


	/**
	 * @param lastLocationUpdateTime the lastLocationUpdateTime to set
	 */
	public void setLastLocationUpdateTime(String lastLocationUpdateTime) {
		this.lastLocationUpdateTime = lastLocationUpdateTime;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}


	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}


	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}


	/**
	 * @return the userGifts
	 */
	public ArrayList<Gift> getUserGifts() {
		return userGifts;
	}


	/**
	 * @param userGifts the userGifts to set
	 */
	public void setUserGifts(ArrayList<Gift> userGifts) {
		this.userGifts = userGifts;
	}


	/**
	 * @return the receivedGift
	 */
	public ArrayList<Gift> getReceivedGift() {
		return receivedGift;
	}


	/**
	 * @param receivedGift the receivedGift to set
	 */
	public void setReceivedGift(ArrayList<Gift> receivedGift) {
		this.receivedGift = receivedGift;
	}


	/**
	 * @return the actions
	 */
	public ArrayList<Action> getActions() {
		return actions;
	}


	/**
	 * @param actions the actions to set
	 */
	public void setActions(ArrayList<Action> actions) {
		this.actions = actions;
	}
	
	
	

}
