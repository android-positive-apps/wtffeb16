package com.wtf.objects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;

import com.wtf.storage.StorageListener;

public class CreditCard implements Serializable {

	
	public static final String KEY_CREDIT_CARDS = "CrditCard";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private int imgResource;
	
	/***
	 * used on the profile fragment to create the credit cards List
	 * 
	 * @param name
	 * @param imgResource
	 */

	public CreditCard(String name, int imgResource) {
		super();
		this.name = name;
		this.imgResource = imgResource;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getImgResource() {
		return imgResource;
	}

	public void setImgResource(int imgResource) {
		this.imgResource = imgResource;
	}
	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public static void saveCrditCardsToStorage(final Context context,
			final ArrayList<CreditCard> toSave) {

		new Thread(new Runnable() {
			private FileOutputStream fos;
			private ObjectOutputStream oos;

			@Override
			public void run() {

				try {
					fos = context.openFileOutput(KEY_CREDIT_CARDS,
							Context.MODE_PRIVATE);
					oos = new ObjectOutputStream(fos);
					oos.writeObject(toSave);
					oos.flush();
				} catch (Exception e) {
				} finally {
					try {
						if (fos != null) {
							fos.close();
						}
						if (oos != null) {
							oos.close();
						}
					} catch (IOException e) {
					}
				}

			}
		}).start();

	}

	public static void readCreditCardsFromStorage(final Context context,
			final StorageListener listener) {

		new Thread(new Runnable() {
			private FileInputStream fis;
			private ObjectInputStream ois;

			@Override
			public void run() {
				ArrayList<CreditCard> cards = new ArrayList<CreditCard>();
				try {
					fis = context.openFileInput(KEY_CREDIT_CARDS);
					ois = new ObjectInputStream(fis);

					cards = (ArrayList<CreditCard>) ois.readObject();

				} catch (Exception e) {
				} finally {
					listener.onStorageDataReceived(cards);
					try {
						if (fis != null) {
							fis.close();
						}
						if (ois != null) {
							ois.close();
						}

					} catch (IOException e) {
					}

				}
			}
		}).start();

	}

	
	
	
}
