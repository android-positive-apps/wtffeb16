/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.wtf.activity;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.wtf.activity";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "wtf";
  public static final int VERSION_CODE = 12;
  public static final String VERSION_NAME = "2.12";
}
